#include "stdafx.h"
#include "MainGame.h"

//Scene Defines
//#define Test
#define PhysX

//Scene includes
#ifdef Test
#include "../Game Classes/TestScene.h"
#endif

#ifdef PhysX
#include "../Game Classes/PhysX Testing/PhysXScene.h"
#endif



MainGame::MainGame(GameSettings* settings): BurnedIceGame(settings) {
#ifdef PhysX
	m_FlexEnabled = true;
#endif
}


MainGame::~MainGame()
{
}

bool MainGame::Initialise()
{
	//Add scenes to the game here with AddScene()
	GameScene * scene  = nullptr;
#ifdef Test
	scene = AddScene(new TestScene());
	SetActiveScene(scene);
#endif
#ifdef PhysX
	scene = AddScene(new PhysXScene());
	SetActiveScene(scene);
#endif

	return true;
}
