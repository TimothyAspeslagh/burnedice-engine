#include "stdafx.h"
#include "MainGame.h"

#include <dxgidebug.h>


int main()
{
	// Enable run-time memory leak check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	typedef HRESULT(__stdcall *fPtr)(const IID&, void**);
	HMODULE hDll = LoadLibrary(L"dxgidebug.dll");
	fPtr DXGIGetDebugInterface = (fPtr)GetProcAddress(hDll, "DXGIGetDebugInterface");
	
	IDXGIDebug* pDXGIDebug;
	DXGIGetDebugInterface(__uuidof(IDXGIDebug), (void**)&pDXGIDebug);
	//_CrtSetBreakAlloc(8115);
	//_CrtSetBreakAlloc(8035);
#endif


	auto gameSettings = new GameSettings(1280, 720, L"BurnedIce Game", L"BurnedIce Game");
	auto game = new MainGame(gameSettings);
	
	bool initResult = game->BaseInitialise();
	
	if (!initResult)
	{
		game->Clean();
		return 0;
	}

	game->Run();
	
	game->Clean();
	
	delete game;
	game = nullptr;
	
	delete gameSettings;
	gameSettings = nullptr;
	return 0;


}
