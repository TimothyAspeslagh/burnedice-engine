#pragma once
#include "../BurnedIceEngine/Main/BurnedIceGame.h"
class MainGame :
	public BurnedIceGame
{
public:
	MainGame(GameSettings* settings);
	~MainGame();

	bool Initialise();

};

