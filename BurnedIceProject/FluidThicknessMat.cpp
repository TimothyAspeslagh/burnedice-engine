#include "stdafx.h"
#include "FluidThicknessMat.h"


FluidThicknessMat::FluidThicknessMat(DesignString assetFile, DesignString materialID) : Material(assetFile, materialID)
{
}


FluidThicknessMat::~FluidThicknessMat()
{
}

void FluidThicknessMat::Initialise(const MainManager& gameManager)
{

#pragma region Shader Compiler
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/FluidThickness.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: FluidThickness.fx" << endl;
	}

	compilationMsgs->Release();
#pragma endregion

#pragma region Bind to shader and shader input layout

	m_pTechnique = m_pEffect->GetTechniqueByName("DefaultTechnique");
	m_pWorldViewProjMat = m_pEffect->GetVariableByName("gWorldViewProj")->AsMatrix();
	m_pWorldMat = m_pEffect->GetVariableByName("gWorld")->AsMatrix();
	m_pProjection = m_pEffect->GetVariableByName("gProjection")->AsMatrix();
	m_pWorldView = m_pEffect->GetVariableByName("gWorldView")->AsMatrix();
	m_pViewInverse = m_pEffect->GetVariableByName("gViewInverse")->AsMatrix();
	m_pEyePos = m_pEffect->GetVariableByName("gEyePos")->AsVector();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",0, DXGI_FORMAT_R32G32B32_FLOAT, 0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: FluidThickness" << endl;
	}

#pragma endregion

#pragma region Depth  RTV & SRV
	auto desc = D3D11_TEXTURE2D_DESC();
	desc.Width = gameManager.GetGameSettings()->WindowWidth;
	desc.Height = gameManager.GetGameSettings()->WindowHeight;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	//desc.Format = DXGI_FORMAT_R32_FLOAT;
	desc.Format = DXGI_FORMAT_R8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	result = gameManager.m_pDevice->CreateTexture2D(&desc, nullptr, &m_pFluidTex2D);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Thickness Texture2D!" << endl;
	}

	result = gameManager.m_pDevice->CreateRenderTargetView(m_pFluidTex2D, nullptr, &m_pFluidRTV);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Thickness RTV!" << endl;
	}

	result = gameManager.m_pDevice->CreateShaderResourceView(m_pFluidTex2D, nullptr, &m_pFluidSRV);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Thickness SRV!" << endl;
	}
#pragma endregion

}

void FluidThicknessMat::UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures)
{
	wcout << L"Warning: Updating fluid shader variables with transformcomponent, fluids do not have one!" << endl;
}

void FluidThicknessMat::UpdateShaderVariables(const MainManager& gameManager)
{
	auto posMat = XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);
	//auto posMat = XMMatrixTranslation(0,0,0);
	auto RotMat = XMMatrixRotationQuaternion(XMQuaternionIdentity());
	// auto scaleMat = XMMatrixScaling(m_Radius / 2.0f, m_Radius / 2.0f, m_Radius / 2.0f);
	auto scaleMat = XMMatrixScaling(1, 1, 1);
	XMMATRIX world = scaleMat * RotMat * posMat;
	XMFLOAT4X4 viewProjMat = gameManager.GetActiveCamera()->GetViewProjection();
	XMFLOAT4X4 viewInv = gameManager.GetActiveCamera()->GetViewInverse();
	XMFLOAT4X4 proj = gameManager.GetActiveCamera()->GetProjection();
	XMFLOAT4X4 view = gameManager.GetActiveCamera()->GetView();
	XMMATRIX projMat = XMLoadFloat4x4(&proj);
	XMMATRIX viewInvMat = XMLoadFloat4x4(&viewInv);
	XMMATRIX viewProj = XMLoadFloat4x4(&viewProjMat);
	XMMATRIX viewMat = XMLoadFloat4x4(&view);
	XMMATRIX worldView = world * viewMat;
	XMFLOAT4X4 worldMat, wvpMat;
	XMStoreFloat4x4(&worldMat, world);
	XMMATRIX wvp = world* viewProj;
	//XMStoreFloat4x4(&wvpMat, wvp);
	
	XMVECTOR eyePos = gameManager.GetActiveCamera()->GetEyePos();
	
	
	if (m_pViewInverse != nullptr)
		m_pViewInverse->SetMatrix(reinterpret_cast<float*>(&viewInvMat));
	
	if (m_pWorldMat != nullptr)
		m_pWorldMat->SetMatrix(reinterpret_cast<float*>(&worldMat));
	
	if (m_pWorldViewProjMat != nullptr)
		m_pWorldViewProjMat->SetMatrix(reinterpret_cast<float*>(&wvp));
	
	if (m_pProjection != nullptr)
		m_pProjection->SetMatrix(reinterpret_cast<float*>(&projMat));
	
	if (m_pWorldView != nullptr)
		m_pWorldView->SetMatrix(reinterpret_cast<float*>(&worldView));
	
	//if (m_pTexDiffuse != nullptr)
	//	m_pTexDiffuse->SetResource(m_pDiffuseTexture->GetSRV());
	
	if (m_pEyePos != nullptr)
		m_pEyePos->SetFloatVector(reinterpret_cast<float*>(&eyePos));

}

void FluidThicknessMat::Clean()
{
	m_pDiffuseTexture = nullptr;
	if (m_pFluidRTV != nullptr)
	{
		m_pFluidRTV->Release();
	}

	if (m_pFluidSRV != nullptr)
	{
		m_pFluidSRV->Release();
	}
	if (m_pFluidTex2D != nullptr)
	{
		m_pFluidTex2D->Release();
	}
}

void FluidThicknessMat::Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp)
{
	wcout << L"Warning: Drawing fluid shader with transformcomponent, fluids do not have one!" << endl;
}

void FluidThicknessMat::Draw(const MainManager& gameManager, const DesignString meshID)
{
	//TODO: REMOVE
	wcout << L"Warning! Method should be removed! Do not call!" << endl;
}

void FluidThicknessMat::Draw(const MainManager& gameManager)
{


	size_t stride = sizeof(iFluidVertex);
	size_t offset = 0;
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	D3DX11_TECHNIQUE_DESC tD;
	m_pTechnique->GetDesc(&tD);
	for (size_t i = 0; i < tD.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->Draw(1, 0);
	}

}

void FluidThicknessMat::CreateVertexBuffer(const MainManager& gameManager, const int size)
{
	//VERTEX BUFFER
	vector<iFluidVertex> vertexList = { iFluidVertex(0,0,0) };
	//vertexList.resize(size);
	D3D11_BUFFER_DESC vbd;
	vbd.ByteWidth = sizeof(iFluidVertex) *vertexList.size();
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	//vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	//vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vsrd = { 0 };
	vsrd.pSysMem = vertexList.data();

	auto result = gameManager.m_pDevice->CreateBuffer(&vbd, &vsrd, &m_pVertexBuffer);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create vertex buffer in FluidMaterial" << endl;
	}
}

void FluidThicknessMat::SetRenderTarget(const MainManager& gameManager)
{
	gameManager.m_pDeviceContext->OMSetRenderTargets(1, &m_pFluidRTV, nullptr);
}

void FluidThicknessMat::ClearRTV(const MainManager& gameManager)
{
	float color[1];
	color[0] = 0;

	gameManager.m_pDeviceContext->ClearRenderTargetView(m_pFluidRTV, color);
}
