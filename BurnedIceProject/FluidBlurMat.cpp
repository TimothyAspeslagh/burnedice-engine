#include "stdafx.h"
#include "FluidBlurMat.h"


FluidBlurMat::FluidBlurMat(DesignString assetFile, DesignString materialID) : Material(assetFile, materialID)
{
}

FluidBlurMat::~FluidBlurMat()
{
}


void FluidBlurMat::Initialise(const MainManager& gameManager)
{
#pragma region Shader Compiler
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/FluidBlur.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: FluidBlur.fx" << endl;
	}

	compilationMsgs->Release();
#pragma endregion

	m_pTechniqueX = m_pEffect->GetTechniqueByName("BlurXTechnique");
	//m_pTechniqueY = m_pEffect->GetTechniqueByName("BlurYTechnique");
	m_pDepthInfo = m_pEffect->GetVariableByName("gDepthTexture")->AsShaderResource();
	m_pThicknessInfo = m_pEffect->GetVariableByName("gThicknessTexture")->AsShaderResource();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0 ,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pTechniqueX->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: FluidDepth" << endl;
	}

	CreateVertexBuffer(gameManager);

#pragma region Depth  RTV & SRV
	auto desc = D3D11_TEXTURE2D_DESC();
	desc.Width = gameManager.GetGameSettings()->WindowWidth;
	desc.Height = gameManager.GetGameSettings()->WindowHeight;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	//desc.Format = DXGI_FORMAT_R32G32_FLOAT;
	desc.Format = DXGI_FORMAT_R8G8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	//desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

	result = gameManager.m_pDevice->CreateTexture2D(&desc, nullptr, &m_pFluidTex2D);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Depth Texture2D!" << endl;
	}

	result = gameManager.m_pDevice->CreateRenderTargetView(m_pFluidTex2D, nullptr, &m_pFluidRTV);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Depth RTV!" << endl;
	}

	result = gameManager.m_pDevice->CreateShaderResourceView(m_pFluidTex2D, nullptr, &m_pFluidSRV);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Fluid Depth SRV!" << endl;
	}
#pragma endregion

}

void FluidBlurMat::UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures)
{
	//nothing to do 
}

void FluidBlurMat::UpdateShaderVariables(const MainManager& gameManager)
{
	//nothing to do
}

void FluidBlurMat::Clean()
{
	if (m_pFluidRTV != nullptr)
	{
		m_pFluidRTV->Release();
	}

	if (m_pFluidSRV != nullptr)
	{
		m_pFluidSRV->Release();
	}
	if (m_pFluidTex2D != nullptr)
	{
		m_pFluidTex2D->Release();
	}
}

void FluidBlurMat::Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp)
{
	wcout << L"Warning: Drawing fluid shader with transformcomponent, fluids do not have one!" << endl;
}

void FluidBlurMat::Draw(const MainManager& gameManager, const DesignString meshID)
{
	//TODO: REMOVE
	wcout << L"Warning! Method should be removed! Do not call!" << endl;
}

void FluidBlurMat::Draw(const MainManager& gameManager)
{
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);

	UINT offset = 0;
	m_VertexBufferStride = sizeof(iPPVertex);
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_VertexBufferStride, &offset);

	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechniqueX->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		m_pTechniqueX->GetPassByIndex(p)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->Draw(4, 0);
	}

	gameManager.m_pDeviceContext->GenerateMips(m_pFluidSRV);

}

void FluidBlurMat::CreateVertexBuffer(const MainManager& gameManager)
{
	//Static Buffer Object (Created once, shared between other PPMaterials)
	if (m_pVertexBuffer)
		return;

	m_VertexBufferStride = sizeof(iPPVertex);

	//Create a vertex buffer for a full screen quad. Use the VertexPosTex struct (m_pVertexBuffer)


	vector<iPPVertex> vecVertices;
	vecVertices.push_back(iPPVertex(XMFLOAT3(-1, -1, 0), XMFLOAT2(0, 1)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(-1, 1, 0), XMFLOAT2(0, 0)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(1, -1, 0), XMFLOAT2(1, 1)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(1, 1, 0), XMFLOAT2(1, 0)));
	D3D11_SUBRESOURCE_DATA initData = { 0 };
	initData.pSysMem = vecVertices.data();
	//Primitive Topology: TriangleStrip
	//Vertex Amount: NUM_VERTS (4)
	//Vertex Struct: VertexPosTex
	//Use NDC to define your vertex positions
	iPPVertex bufferStruct = {};
	HRESULT result;
	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.ByteWidth = sizeof(iPPVertex) * NUM_VERTS;
	bufferDesc.MiscFlags = 0;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.StructureByteStride = m_VertexBufferStride;

	result = gameManager.m_pDevice->CreateBuffer(&bufferDesc, &initData, &m_pVertexBuffer);
	if (FAILED(result))
	{
		wcout << L"Failed to create vertex buffer: FluidBlurMat" << endl;
	}
	//+ Check for errors (HRESULT)
	//Set 'm_VertexBufferStride' (Size in bytes of VertexPosTex)
	// Create vertex array containing three elements in system memory

}

void FluidBlurMat::SetRenderTarget(const MainManager& gameManager)
{
	gameManager.m_pDeviceContext->OMSetRenderTargets(1, &m_pFluidRTV, nullptr);
}

void FluidBlurMat::SetDepthTexture(ID3D11ShaderResourceView* srv)
{
	m_pDepthInfo->SetResource(srv);
}

void FluidBlurMat::SetThicknessTexture(ID3D11ShaderResourceView* srv)
{
	m_pThicknessInfo->SetResource(srv);
}

void FluidBlurMat::ClearRTV(const MainManager& gameManager)
{
	float color[2];
	color[0] = 0;
	color[1] = 0;
	gameManager.m_pDeviceContext->ClearRenderTargetView(m_pFluidRTV, color);
}
