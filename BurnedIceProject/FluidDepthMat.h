#pragma once

#include "../BurnedIceEngine/Content/Material.h"

class FluidDepthMat : public Material
{
public:
	FluidDepthMat(DesignString assetFile, DesignString materialID);
	~FluidDepthMat();

	//Default Material methods
	void Initialise(const MainManager& gameManager) override;
	void UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures) override;
	void UpdateShaderVariables(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp) override;
	void Draw(const MainManager& gameManager, const DesignString meshID) override;
	void Draw(const MainManager& gameManager) override;
	void SetDiffuseTexture(Texture* pDiffuse) override
	{
		m_pDiffuseTexture = pDiffuse;
		if (m_pTexDiffuse != nullptr)
			m_pTexDiffuse->SetResource(m_pDiffuseTexture->GetSRV());
	}
	
	//Custom Fluid Methods
	void SetPosition(float4 position) { m_Position = position; }
	void SetRadius(float radius) { m_Radius = radius; }

	ID3D11RenderTargetView * GetRTV() const { return m_pFluidRTV; }
	ID3D11ShaderResourceView * GetSRV() const { return m_pFluidSRV; }
	ID3D11Texture2D * GetTexture2D() const { return m_pFluidTex2D; }

	void CreateVertexBuffer(const MainManager& gameManager, const int size);

	void SetRenderTarget(const MainManager& gameManager);
	void ClearRTV(const MainManager& gameManager);

private:

	HRESULT CreateDepthStencil(const MainManager& gameManager);
	ID3DX11EffectMatrixVariable * m_pViewInverse;
	ID3DX11EffectMatrixVariable *m_pProjection;
	ID3DX11EffectMatrixVariable * m_pWorldView;

	ID3DX11EffectVectorVariable * m_pEyePos;

	ID3D11Buffer *m_pVertexBuffer;

	ID3DX11EffectTechnique *m_pTechnique;


	ID3D11RenderTargetView *m_pFluidRTV = nullptr;
	ID3D11ShaderResourceView * m_pFluidSRV = nullptr;
	ID3D11Texture2D * m_pFluidTex2D = nullptr;
	ID3D11DepthStencilView * m_pDepthStencilView;
	ID3D11Texture2D * m_pDepthStencilBuffer;


	float4 m_Position;
	float m_Radius;

	Texture* m_pDiffuseTexture = nullptr;
};

