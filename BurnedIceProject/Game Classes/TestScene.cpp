#include "stdafx.h"
#include "TestScene.h"
#include "../../BurnedIceEngine/Scene Classes/Components/TransformComponent.h"
#include "TestObject.h"
#include "../../BurnedIceEngine/Scene Classes/Components/CubeMeshComponent.h"
#include "../../BurnedIceEngine/Scene Classes/Prefabs/FreeCamera.h"
#include "../../BurnedIceEngine/Scene Classes/Components/ModelComponent.h"

TestScene::TestScene()
{
}


TestScene::~TestScene()
{
}

bool TestScene::Initialise(const MainManager &gameManager)
{
	//Load content
	gameManager.Content->LoadMesh(DesignString("./Resources/teapot.fbx"), DesignString("cube"));
	gameManager.Content->LoadMaterial(DesignString("./Resources/CubeMeshEffect.fx"), DesignString("cubeMat"));
	gameManager.Content->LoadTexture(DesignString("./Resources/Crate_Tex.jpg"), DesignString("cubeTex"));

	//Add game objects to the scene
	m_pCube = new TestObject();
	m_pCube->SetTag(DesignString("Test"));

	auto tCmp = new TransformComponent();
	tCmp->Scale(0.05f, 0.05f, 0.05f);
	auto mCmp = new ModelComponent();
	mCmp->SetMesh(DesignString("cube"));
	mCmp->AddMaterial(DesignString("cubeMat"));
	mCmp->AddTexture(DesignString("cubeTex"), TextureUsage::DIFFUSE);
	m_pCube->AddComponent(tCmp);
	m_pCube->AddComponent(mCmp);

	auto cube2 = new TestObject();
	cube2->AddComponent(new TransformComponent());
	auto mCmp2 = new ModelComponent();
	mCmp2->SetMesh(DesignString("cube"));
	mCmp2->AddMaterial(DesignString("cubeMat"));
	mCmp2->AddTexture(DesignString("cubeTex"), TextureUsage::DIFFUSE);
	cube2->AddComponent(mCmp2);
	cube2->GetTransform()->Translate(100, 0, 0);
	m_pCube->AddChild(cube2);

	auto cube3 = new TestObject();
	cube3->AddComponent(new TransformComponent());
	auto mCmp3 = new ModelComponent();
	mCmp3->SetMesh(DesignString("cube"));
	mCmp3->AddMaterial(DesignString("cubeMat"));
	mCmp3->AddTexture(DesignString("cubeTex"), TextureUsage::DIFFUSE);
	cube3->AddComponent(mCmp3);
	cube3->GetTransform()->Translate(0, 0, 100);
	cube2->AddChild(cube3);


	auto freeCamera = new FreeCamera();
	auto cameraCmp = new CameraComponent();
	auto cameraTransform = new TransformComponent();
	cameraTransform->Translate(0, 1, -10);
	freeCamera->AddComponent(cameraTransform);
	freeCamera->AddComponent(cameraCmp);
	AddObjectAtInitialise(freeCamera);
	SetActiveCamera(freeCamera->GetCameraComponent());

	AddObjectAtInitialise(m_pCube);


	return true;
}

void TestScene::Tick(const MainManager& gameManager)
{
	if (gameManager.Input->IsKeyPressed('r'))
	{
		m_Rotation += gameManager.GetDeltaTime();
		if (m_Rotation > 360)
			m_Rotation -= 360;

		m_pCube->GetTransform()->RotateEuler(0, m_Rotation, 0);
		m_pCube->GetChildren()[0]->GetTransform()->RotateEuler(m_Rotation, 0, 0);
	}

}

void TestScene::Draw(const MainManager& gameManager)
{
}

void TestScene::Clean()
{

}
