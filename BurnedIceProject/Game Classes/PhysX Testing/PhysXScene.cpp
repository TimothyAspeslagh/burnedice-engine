#include "stdafx.h"
#include "PhysXScene.h"
#include "../TestObject.h"
#include "../../../BurnedIceEngine/Scene Classes/Components/ModelComponent.h"
#include "../../../BurnedIceEngine/Scene Classes/Prefabs/FreeCamera.h"
#include "../../../BurnedIceEngine/Scene Classes/Components/RigidBodyComponent.h"
#include "../../../BurnedIceEngine/PhysX/FleXProxy.h"


PhysXScene::PhysXScene()
{
}


PhysXScene::~PhysXScene()
{
}

bool PhysXScene::Initialise(const MainManager& gameManager)
{
	auto physX = gameManager.Physics->GetPhysX();
	auto bouncyMaterial = physX->createMaterial(0.5f, 0.5f, 1.0f);

	gameManager.Content->LoadMesh(DesignString("./Resources/Sphere.fbx"), DesignString("sphere"));
	gameManager.Content->LoadMaterial(DesignString("./Resources/CubeMeshEffect.fx"), DesignString("Mat"));
	gameManager.Content->LoadTexture(DesignString("./Resources/Crate_Tex.jpg"), DesignString("Tex"));


	/*
	//GROUND
	//******
	auto ground = new GameObject();
	
	ground->AddComponent(new RigidBodyComponent(true));
	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	auto pCollider = new Collider(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1))));
	ground->GetComponent<RigidBodyComponent>()->AddCollider(pCollider);

	//ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddObjectAtInitialise(ground);


	//SPHERE 1 (Group0) + (Ignore Group1 & Group2)
	//*********
	auto pSphere = new GameObject();
	pSphere->AddComponent(new TransformComponent());
	auto mCmp = new ModelComponent();
	mCmp->SetMesh(DesignString("sphere"));
	mCmp->AddMaterial(DesignString("Mat"));
	mCmp->AddTexture(DesignString("Tex"), TextureUsage::DIFFUSE);
	pSphere->AddComponent(mCmp);

	pSphere->GetTransform()->Translate(0, 30, 0);

	auto pRigidBody = new RigidBodyComponent(false);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group0); //SET GROUP
	pRigidBody->SetCollisionIgnoreGroup(static_cast<CollisionGroupFlag>(Group1 | Group2)); //IGNORE GROUP	
	pSphere->AddComponent(pRigidBody);

	std::shared_ptr<PxGeometry> spheregeom(new PxSphereGeometry(1));
	pCollider = new Collider(spheregeom, *bouncyMaterial, PxTransform(PxIdentity));//PxQuat(XM_PIDIV2, PxVec3(0, 0, 1))
	pRigidBody->AddCollider(pCollider);
	//pSphere->AddComponent(new ColliderComponent(spheregeom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddObjectAtInitialise(pSphere);

	//SPHERE 2 (Group1)

	pSphere = new GameObject();
	pSphere->AddComponent(new TransformComponent());

	pSphere->GetTransform()->Translate(0, 20, 0);
	mCmp = new ModelComponent();
	mCmp->SetMesh(DesignString("sphere"));
	mCmp->AddMaterial(DesignString("Mat"));
	mCmp->AddTexture(DesignString("Tex"), TextureUsage::DIFFUSE);
	pSphere->AddComponent(mCmp);

	pRigidBody = new RigidBodyComponent(false);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1); //SET GROUP
	pSphere->AddComponent(pRigidBody);
	
	pCollider = new Collider(spheregeom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1))));
	pRigidBody->AddCollider(pCollider);

	//pSphere->AddComponent(new ColliderComponent(spheregeom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddObjectAtInitialise(pSphere);

	//SPHERE 3 (Group2)
	pSphere = new GameObject();
	pSphere->AddComponent(new TransformComponent());
	pSphere->GetTransform()->Translate(0, 10, 0);
	mCmp = new ModelComponent();
	mCmp->SetMesh(DesignString("sphere"));
	mCmp->AddMaterial(DesignString("Mat"));
	mCmp->AddTexture(DesignString("Tex"), TextureUsage::DIFFUSE);
	pSphere->AddComponent(mCmp);

	pRigidBody = new RigidBodyComponent(false);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group2); //SET GROUP
	pSphere->AddComponent(pRigidBody);
	
	pCollider = new Collider(spheregeom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1))));
	pRigidBody->AddCollider(pCollider);

	//pSphere->AddComponent(new ColliderComponent(spheregeom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));

	AddObjectAtInitialise(pSphere);
	*/
	


	//CAMERA
	//******
	auto freeCamera = new FreeCamera();
	auto cameraCmp = new CameraComponent();
	auto cameraTransform = new TransformComponent();
	cameraTransform->Translate(0, 10, -30);
	freeCamera->AddComponent(cameraTransform);
	freeCamera->AddComponent(cameraCmp);
	AddObjectAtInitialise(freeCamera);
	SetActiveCamera(freeCamera->GetCameraComponent());
	//m_pSphere = pSphere;


	//FLEX
	//****
	auto fleX = GetFleXProxy();
	if(fleX != nullptr)
		fleX->AddFluidBox(PxVec3(0, 5, 0), 10, 20, 10, .1f);

	return true;
}

void PhysXScene::Tick(const MainManager& gameManager)
{
	//if(gameManager.Input->IsKeyPressed('t'))
	//{
	//	m_pSphere->GetComponent<RigidBodyComponent>()->AddForce(PxVec3(5, 0, 0), PxForceMode::eFORCE, true);
	//}

	if (gameManager.Input->IsKeyPressed(VK_UP))
	{
		GetFleXProxy()->SetGravity(float3(0.0f, 5.0f, 0.0f));
	}

	if (gameManager.Input->IsKeyPressed(VK_DOWN))
	{
		GetFleXProxy()->SetGravity(float3(0.0f, -9.81f, 0.0f));
	}

	if (gameManager.Input->IsKeyPressed(VK_RIGHT))
	{
		GetFleXProxy()->SetGravity(float3(5.0f, 0.0f, 0.0f));
	}

	if (gameManager.Input->IsKeyPressed(VK_LEFT))
	{
		GetFleXProxy()->SetGravity(float3(-5.0f, 0.0f, 0.0f));
	}
}

void PhysXScene::Draw(const MainManager& gameManager)
{
}

void PhysXScene::Clean()
{
	m_pSphere = nullptr;
}
