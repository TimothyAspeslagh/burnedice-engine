#pragma once
#include "../BurnedIceEngine/Scene Classes/GameScene.h"
#include "../TestObject.h"

class PhysXScene :
	public GameScene
{
public:
	PhysXScene();
	~PhysXScene();
	bool Initialise(const MainManager& gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Draw(const MainManager& gameManager) override;
	void Clean() override;

private:
	GameObject * m_pSphere = nullptr;
};

