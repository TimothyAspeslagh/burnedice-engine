#pragma once
#include "../BurnedIceEngine/Scene Classes/GameScene.h"

class TestObject;

class TestScene :
	public GameScene
{
public:
	TestScene();
	~TestScene();

	bool Initialise(const MainManager &gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Draw(const MainManager& gameManager) override;
	void Clean() override;

private:
	TestObject * m_pCube = nullptr;
	float m_Rotation = 0.0f;
	float m_yRot = 0.0f;
	float m_xRot = 13.0f;
};

