#pragma once
#include "../BurnedIceEngine/Scene Classes/GameObject.h"

class TestObject :
	public GameObject
{
public:
	TestObject();
	~TestObject();

	void Initialise(const MainManager &gameManager) override;
	void Tick(const MainManager &gameManager) override;
	void Draw(const MainManager &gameManager) override;
	void Clean() override;

private:
	float m_TotalTime = 0.0f;

};

