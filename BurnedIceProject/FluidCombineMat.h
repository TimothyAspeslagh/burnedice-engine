#pragma once

#include "../BurnedIceEngine/Content/Material.h"
class FluidCombineMat :
	public Material
{
public:
	FluidCombineMat(DesignString assetFile, DesignString materialID);
	~FluidCombineMat();
	void Initialise(const MainManager& gameManager) override;
	void UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures) override;
	void UpdateShaderVariables(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp) override;
	void Draw(const MainManager& gameManager, const DesignString meshID) override;
	void Draw(const MainManager& gameManager) override;
	void SetDiffuseTexture(Texture* pDiffuse) override
	{
		m_pDiffuseTexture = pDiffuse;
		if (m_pTexDiffuse != nullptr)
			m_pTexDiffuse->SetResource(m_pDiffuseTexture->GetSRV());
	}

	//Custom methods
	ID3D11RenderTargetView * GetRTV() const { return m_pFluidRTV; }
	ID3D11ShaderResourceView * GetSRV() const { return m_pFluidSRV; }
	ID3D11Texture2D * GetTexture2D() const { return m_pFluidTex2D; }

	void SetTextureInfo(ID3D11ShaderResourceView* srv);
	void SetTexture(ID3D11ShaderResourceView* srv);

private:
	void CreateVertexBuffer(const MainManager& gameManager);

	ID3D11Buffer *m_pVertexBuffer;
	ID3DX11EffectTechnique *m_pTechnique;
	ID3D11RenderTargetView *m_pFluidRTV = nullptr;
	ID3D11ShaderResourceView * m_pFluidSRV = nullptr;
	ID3D11Texture2D * m_pFluidTex2D = nullptr;

	ID3DX11EffectShaderResourceVariable * m_pTextureInfo;
	ID3DX11EffectShaderResourceVariable * m_pTexture;
	ID3DX11EffectMatrixVariable * m_pViewProjInverse = nullptr;
	ID3DX11EffectVectorVariable * m_pInvTexScale = nullptr;
	ID3DX11EffectVectorVariable * m_pClipPosToEye = nullptr;


	static const int NUM_VERTS = 4;
	unsigned int m_VertexBufferStride;


	Texture* m_pDiffuseTexture = nullptr;

};

