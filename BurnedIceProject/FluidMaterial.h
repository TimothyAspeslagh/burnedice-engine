#pragma once
#include "../BurnedIceEngine/Content/Material.h"

class FluidMaterial :
	public Material
{
public:
	FluidMaterial(DesignString assetFile, DesignString materialID);
	~FluidMaterial();

	void UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures) override;

	void Initialise(const MainManager &gameManager) override;
	void CreateVertexBuffer(const MainManager& gameManager, int size);
	void Clean() override;
	void Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp) override;
	void Draw(const MainManager& gameManager) override;
	void DrawDebug(const MainManager& gameManager);
	void UpdateShaderVariables(const MainManager& gameManager) override;
	//void SetPositions(const MainManager& gameManager, vector<pair<int, iFluidVertex>> positions);
	void SetPosition(float4 position);
	void SetRadius(float radius) { m_Radius = radius; }
	void SetAnisotropy(float4 anisotropy1, float4 anisotropy2, float4 anisotropy3);
	void SetDebugMode(bool enabled) { m_DebugEnabled = enabled; }
	void SetDiffuseTexture(Texture* pDiffuse) override
	{ 
		m_pDiffuseTexture = pDiffuse; 
	if (m_pTexDiffuse != nullptr)
		m_pTexDiffuse->SetResource(m_pDiffuseTexture->GetSRV());
	}

	void SetMesh(const MainManager& gameManager, Mesh* mesh);

	void Begin(const MainManager& gameManager);
	void End(const MainManager& gameManager);

	friend class Mesh;
private:
	ID3DX11EffectVectorVariable * m_pAnisotropy1;
	ID3DX11EffectVectorVariable * m_pAnisotropy2;
	ID3DX11EffectVectorVariable * m_pAnisotropy3;

	ID3DX11EffectMatrixVariable * m_pViewInverse;
	ID3DX11EffectMatrixVariable *m_pProjection;
	ID3DX11EffectMatrixVariable * m_pWorldView;

	ID3DX11EffectVectorVariable * m_pEyePos;

	ID3D11Buffer *m_pVertexBuffer;

	ID3DX11EffectTechnique *m_pDebugTechnique;
	ID3DX11EffectTechnique *m_pBlurTechnique;

	ID3D11RenderTargetView *m_pFluidRTV = nullptr;
	ID3D11ShaderResourceView * m_pFluidSRV = nullptr;
	ID3D11Texture2D * m_pFluidTex2D = nullptr;
	ID3DX11EffectShaderResourceVariable * m_pFluidVariable = nullptr;


	float4 m_Position, m_Anisotropy1, m_Anisotropy2, m_Anisotropy3;
	float m_Radius;

	bool m_DebugEnabled = false;
	Texture* m_pDiffuseTexture = nullptr;
	Mesh* m_pMesh = nullptr;
};

