#include "stdafx.h"
#include "FluidCombineMat.h"


FluidCombineMat::FluidCombineMat(DesignString assetFile, DesignString materialID) : Material(assetFile, materialID)
{
}

FluidCombineMat::~FluidCombineMat()
{
}

void FluidCombineMat::Initialise(const MainManager& gameManager)
{
#pragma region Shader Compiler
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/FluidDraw.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: FluidDraw.fx" << endl;
	}

	compilationMsgs->Release();
#pragma endregion

	m_pTechnique = m_pEffect->GetTechniqueByName("DrawTechnique");
	m_pTextureInfo = m_pEffect->GetVariableByName("gTextureInfo")->AsShaderResource();
	m_pTexture = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	m_pViewProjInverse = m_pEffect->GetVariableByName("gViewProjInverse")->AsMatrix();
	m_pInvTexScale = m_pEffect->GetVariableByName("gInvTexScale")->AsVector();
	m_pClipPosToEye = m_pEffect->GetVariableByName("gClipPosToEye")->AsVector();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0 ,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: FluidDraw" << endl;
	}

	CreateVertexBuffer(gameManager);

}

void FluidCombineMat::UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures)
{
	//nothing to do 
}

void FluidCombineMat::UpdateShaderVariables(const MainManager& gameManager)
{
///	XMFLOAT4X4 viewProj = gameManager.GetActiveCamera()->GetViewProjection();

	XMFLOAT4X4 viewProj = gameManager.GetActiveCamera()->GetProjection();
	XMMATRIX viewProjMat = XMLoadFloat4x4(&viewProj);
	auto viewProjInvMat = XMMatrixInverse(nullptr, viewProjMat);
	float screenAspect = ((float)gameManager.GetGameSettings()->WindowWidth / (gameManager.GetGameSettings()->WindowHeight));
	XMFLOAT4 invTexScale = XMFLOAT4(1.0f / (float)gameManager.GetGameSettings()->WindowWidth, 1.0f / (float)gameManager.GetGameSettings()->WindowHeight, 0.0f, 0.0f);
	XMVECTOR invTexScaleVec = XMLoadFloat4(&invTexScale);
	//invTexScale = float4(1.0f / screenAspect, 1.0f, 0.0f, 0.0f);

	auto cpte = gameManager.GetClipPosToEye();
	XMFLOAT4 clipPosToEye = XMFLOAT4(cpte.x, cpte.y, cpte.z, cpte.w);
	XMVECTOR clipPosToEyeVec = XMLoadFloat4(&clipPosToEye);

	if (m_pViewProjInverse != nullptr)
		m_pViewProjInverse->SetMatrix(reinterpret_cast<float*>(&viewProjInvMat));

	if (m_pInvTexScale != nullptr)
		m_pInvTexScale->SetFloatVector(reinterpret_cast<float*>(&invTexScaleVec));

	if (m_pClipPosToEye != nullptr)
		m_pClipPosToEye->SetFloatVector(reinterpret_cast<float*>(&clipPosToEyeVec));
}

void FluidCombineMat::Clean()
{
	if (m_pFluidRTV != nullptr)
	{
		m_pFluidRTV->Release();
	}

	if (m_pFluidSRV != nullptr)
	{
		m_pFluidSRV->Release();
	}
	if (m_pFluidTex2D != nullptr)
	{
		m_pFluidTex2D->Release();
	}

	if(m_pViewProjInverse != nullptr)
	{
//		m_pViewProjInverse->Release();
	}

	if(m_pInvTexScale != nullptr)
	{
//		m_pInvTexScale->Release();
	}

	if(m_pClipPosToEye != nullptr)
	{
		//m_pClipPosToEye->Release();
	}
}

void FluidCombineMat::Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp)
{
	wcout << L"Warning: Drawing fluid shader with transformcomponent, fluids do not have one!" << endl;
}

void FluidCombineMat::Draw(const MainManager& gameManager, const DesignString meshID)
{
	//TODO: REMOVE
	wcout << L"Warning! Method should be removed! Do not call!" << endl;
}

void FluidCombineMat::Draw(const MainManager& gameManager)
{
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);

	UINT offset = 0;
	m_VertexBufferStride = sizeof(iPPVertex);
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_VertexBufferStride, &offset);

	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		m_pTechnique->GetPassByIndex(p)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->Draw(4, 0);
	}

	//gameManager.m_pDeviceContext->GenerateMips(m_pFluidSRV);

}

void FluidCombineMat::SetTextureInfo(ID3D11ShaderResourceView* srv)
{
	m_pTextureInfo->SetResource(srv);
}

void FluidCombineMat::SetTexture(ID3D11ShaderResourceView* srv)
{
	m_pTexture->SetResource(srv);
}

void FluidCombineMat::CreateVertexBuffer(const MainManager& gameManager)
{
	//Static Buffer Object (Created once, shared between other PPMaterials)
	if (m_pVertexBuffer)
		return;

	m_VertexBufferStride = sizeof(iPPVertex);

	//Create a vertex buffer for a full screen quad. Use the VertexPosTex struct (m_pVertexBuffer)


	vector<iPPVertex> vecVertices;
	vecVertices.push_back(iPPVertex(XMFLOAT3(-1, -1, 0), XMFLOAT2(0, 1)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(-1, 1, 0), XMFLOAT2(0, 0)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(1, -1, 0), XMFLOAT2(1, 1)));
	vecVertices.push_back(iPPVertex(XMFLOAT3(1, 1, 0), XMFLOAT2(1, 0)));
	D3D11_SUBRESOURCE_DATA initData = { 0 };
	initData.pSysMem = vecVertices.data();
	//Primitive Topology: TriangleStrip
	//Vertex Amount: NUM_VERTS (4)
	//Vertex Struct: VertexPosTex
	//Use NDC to define your vertex positions
	iPPVertex bufferStruct = {};
	HRESULT result;
	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.ByteWidth = sizeof(iPPVertex) * NUM_VERTS;
	bufferDesc.MiscFlags = 0;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.StructureByteStride = m_VertexBufferStride;

	result = gameManager.m_pDevice->CreateBuffer(&bufferDesc, &initData, &m_pVertexBuffer);
	if (FAILED(result))
	{
		wcout << L"Failed to create vertex buffer: FluidBlurMat" << endl;
	}
	//+ Check for errors (HRESULT)
	//Set 'm_VertexBufferStride' (Size in bytes of VertexPosTex)
	// Create vertex array containing three elements in system memory

}
