#pragma once
#include "../BurnedIceEngine/Content/Material.h"
class FluidDebugDraw :
	public Material
{
public:
	FluidDebugDraw(DesignString assetFile, DesignString materialID);
	~FluidDebugDraw();
	void Initialise(const MainManager& gameManager) override;
	void UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures) override;
	void UpdateShaderVariables(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp) override;
	void Draw(const MainManager& gameManager, const DesignString meshID) override;
	void Draw(const MainManager& gameManager) override;
	void SetDiffuseTexture(Texture* pDiffuse) override
	{
		m_pDiffuseTexture = pDiffuse;
		if (m_pTexDiffuse != nullptr)
			m_pTexDiffuse->SetResource(m_pDiffuseTexture->GetSRV());
	}

	void SetRadius(float rad);
	void SetPosition(float4 position);

private:
	void CreateVertexBuffer(const MainManager& gameManager);

	ID3D11Buffer *m_pVertexBuffer;
	ID3DX11EffectTechnique *m_pTechnique;

	ID3DX11EffectMatrixVariable * m_pViewInverse;
	ID3DX11EffectMatrixVariable *m_pProjection;
	ID3DX11EffectMatrixVariable * m_pWorldView;
	ID3DX11EffectVectorVariable * m_pEyePos;

	ID3DX11EffectShaderResourceVariable * m_pTexture;
	float4 m_Position;
	float m_Radius;


	static const int NUM_VERTS = 4;
	unsigned int m_VertexBufferStride;


	Texture* m_pDiffuseTexture = nullptr;

};

