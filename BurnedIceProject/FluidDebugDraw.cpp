#include "stdafx.h"
#include "FluidDebugDraw.h"


FluidDebugDraw::FluidDebugDraw(DesignString assetFile, DesignString materialID) : Material(assetFile, materialID)
{
}

FluidDebugDraw::~FluidDebugDraw()
{
}

void FluidDebugDraw::Initialise(const MainManager& gameManager)
{
#pragma region Shader Compiler
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/FluidDebug.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: FluidDraw.fx" << endl;
	}

	compilationMsgs->Release();
#pragma endregion

	m_pTechnique = m_pEffect->GetTechniqueByName("DebugTechnique");
	m_pWorldViewProjMat = m_pEffect->GetVariableByName("gWorldViewProj")->AsMatrix();
	m_pWorldMat = m_pEffect->GetVariableByName("gWorld")->AsMatrix();
	m_pProjection = m_pEffect->GetVariableByName("gProjection")->AsMatrix();
	m_pWorldView = m_pEffect->GetVariableByName("gWorldView")->AsMatrix();
	m_pViewInverse = m_pEffect->GetVariableByName("gViewInverse")->AsMatrix();
	m_pEyePos = m_pEffect->GetVariableByName("gEyePos")->AsVector();
	m_pTexture = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",0, DXGI_FORMAT_R32G32B32_FLOAT, 0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: FluidDebugDraw" << endl;
	}

	CreateVertexBuffer(gameManager);
}

void FluidDebugDraw::UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>>& textures)
{

}

void FluidDebugDraw::UpdateShaderVariables(const MainManager& gameManager)
{
	auto posMat = XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);
	//auto posMat = XMMatrixTranslation(0,0,0);
	auto RotMat = XMMatrixRotationQuaternion(XMQuaternionIdentity());
	// auto scaleMat = XMMatrixScaling(m_Radius / 2.0f, m_Radius / 2.0f, m_Radius / 2.0f);
	auto scaleMat = XMMatrixScaling(1, 1, 1);
	XMMATRIX world = scaleMat * RotMat * posMat;
	XMFLOAT4X4 viewProjMat = gameManager.GetActiveCamera()->GetViewProjection();
	XMFLOAT4X4 viewInv = gameManager.GetActiveCamera()->GetViewInverse();
	XMFLOAT4X4 proj = gameManager.GetActiveCamera()->GetProjection();
	XMFLOAT4X4 view = gameManager.GetActiveCamera()->GetView();
	XMMATRIX projMat = XMLoadFloat4x4(&proj);
	XMMATRIX viewInvMat = XMLoadFloat4x4(&viewInv);
	XMMATRIX viewProj = XMLoadFloat4x4(&viewProjMat);
	XMMATRIX viewMat = XMLoadFloat4x4(&view);
	XMMATRIX worldView = world * viewMat;
	XMFLOAT4X4 worldMat, wvpMat;
	XMStoreFloat4x4(&worldMat, world);
	XMMATRIX wvp = world* viewProj;
	//XMStoreFloat4x4(&wvpMat, wvp);

	XMVECTOR eyePos = gameManager.GetActiveCamera()->GetEyePos();


	if (m_pViewInverse != nullptr)
		m_pViewInverse->SetMatrix(reinterpret_cast<float*>(&viewInvMat));

	if (m_pWorldMat != nullptr)
		m_pWorldMat->SetMatrix(reinterpret_cast<float*>(&worldMat));

	if (m_pWorldViewProjMat != nullptr)
		m_pWorldViewProjMat->SetMatrix(reinterpret_cast<float*>(&wvp));

	if (m_pProjection != nullptr)
		m_pProjection->SetMatrix(reinterpret_cast<float*>(&projMat));

	if (m_pWorldView != nullptr)
		m_pWorldView->SetMatrix(reinterpret_cast<float*>(&worldView));

	if (m_pTexture != nullptr)
		m_pTexture->SetResource(m_pDiffuseTexture->GetSRV());

	if (m_pEyePos != nullptr)
		m_pEyePos->SetFloatVector(reinterpret_cast<float*>(&eyePos));

}

void FluidDebugDraw::Clean()
{
}

void FluidDebugDraw::Draw(const MainManager& gameManager, const DesignString meshID, TransformComponent* transformCmp)
{
	wcout << L"Warning: Drawing fluid shader with transformcomponent, fluids do not have one!" << endl;
}

void FluidDebugDraw::Draw(const MainManager& gameManager, const DesignString meshID)
{
	//TODO: REMOVE
	wcout << L"Warning! Method should be removed! Do not call!" << endl;
}

void FluidDebugDraw::Draw(const MainManager& gameManager)
{
	size_t stride = sizeof(iFluidVertex);
	size_t offset = 0;
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	D3DX11_TECHNIQUE_DESC tD;
	m_pTechnique->GetDesc(&tD);
	for (size_t i = 0; i < tD.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->Draw(1, 0);
	}

}

void FluidDebugDraw::SetRadius(float rad)
{
	m_Radius = rad;
}

void FluidDebugDraw::SetPosition(float4 position)
{
	m_Position = position;
}

void FluidDebugDraw::CreateVertexBuffer(const MainManager& gameManager)
{	
	//VERTEX BUFFER
	vector<iFluidVertex> vertexList = { iFluidVertex(0,0,0) };
	//vertexList.resize(size);
	D3D11_BUFFER_DESC vbd;
	vbd.ByteWidth = sizeof(iFluidVertex) *vertexList.size();
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	//vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	//vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vsrd = { 0 };
	vsrd.pSysMem = vertexList.data();

	auto result = gameManager.m_pDevice->CreateBuffer(&vbd, &vsrd, &m_pVertexBuffer);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create vertex buffer in FluidMaterial" << endl;
	}

}
