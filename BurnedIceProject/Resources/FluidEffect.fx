
//GLOBAL VARIABLES
//****************
cbuffer cbPerObject
{
	float4x4 gWorldViewProj : WORLDVIEWPROJECTION;
	float4x4 gWorld : WORLD;
	float4x4 gWorldView : WORLDVIEW;
	float4x4 gViewInverse : ViewInverse;
	float4x4 gProjection : PROJECTION;
	//float4x4 gProjectionInv: PROJINVERSE;
	float3 gEyePos: EyePosition;

};

float3 gLightDirection = float3(-0.577f, 0.577f, -0.577f);
Texture2D gDiffuseMap;
Texture2D gFluidMap;
float gRadius = .2f;

//STATES
//******
RasterizerState gRS_NoCulling { CullMode = BACK; };

SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;// or Mirror or Clamp or Border
	AddressV = Wrap;// or Mirror or Clamp or Border
};
BlendState gAdditive
{
	BlendEnable[0] = TRUE; // 0 stands for index in render target array
	SrcBlend = SRC_ALPHA;
	DestBlend = INV_SRC_ALPHA;
	BlendOp = ADD;
	SrcBlendAlpha = ZERO;
	DestBlendAlpha = ZERO;
	BlendOpAlpha = ADD;
	RenderTargetWriteMask[0] = 0x0F;

	//BlendEnable[0] = TRUE;
	//SrcBlend = SRC_COLOR;
	//DestBlend = SRC_COLOR;
	//BlendOp = ADD; //Additive
	////BlendOpAlpha = ADD;
};

//PostProcessing states

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
RasterizerState Solid
{
	CullMode = BACK;
};

DepthStencilState gNoDepth
{
	DepthEnable = false;
	StencilEnable = false;
};

//STRUCTS
//*******

struct VertexIn
{
	float3 Pos : POSITION;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
};

struct GS_DATA
{
	float4 Position: SV_POSITION;
	float4 EyePos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 Color: COLOR;
};

//PostProcessing blur structs

struct VS_PP_INPUT
{
	float3 Position : POSITION;
	float2 texCoord : TEXCOORD0;
};

struct PS_PP_INPUT
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};

struct PS_INFO_OUT
{
	float2 Info: SV_TARGET0;

};

struct PixelOut
{
	float4 Color : COLOR;
};

//MAIN VERTEX SHADER
//******************
VertexOut MainVS(VertexIn vin)
{
	VertexOut vout;

	vout.PosH = float4(vin.Pos, 1.0f); // mul(float4(vin.Pos, 1.0f), gWorldViewProj);

	return vout;
}

//BLUR VERTEX SHADER
PS_PP_INPUT VS_PP(VS_PP_INPUT vin)
{
	PS_PP_INPUT output = (PS_PP_INPUT)0;

	output.Position = float4(input.Position, 1.0f);
	output.TexCoord = input.TexCoord;
	
	return output;
}

//GEOMETRY SHADER
//***************

void CreateVertex(inout TriangleStream<GS_DATA> triStream, float3 pos, float2 texCoord, float4 col, float2x2 uvRotation)
{
	//Step 1. Create a GS_DATA object
	GS_DATA data;

	//Step 2. Transform the position using the WVP Matrix and assign it to (GS_DATA object).Position (Keep in mind: float3 -> float4, Homogeneous Coordinates)

	//float3 homogeneousMat = gWorldViewProj
	data.Position = mul(float4(pos, 1), gWorldViewProj);
	data.EyePos = mul(float4(pos, 1), gWorldView);

	//Step 3. Assign texCoord to (GS_DATA object).TexCoord

	//This is a little formula to do texture rotation by transforming the texture coordinates (Can cause artifacts)
	//texCoord -= float2(0.5f, 0.5f);
	//texCoord = mul(texCoord, uvRotation);
	//texCoord += float2(0.5f, 0.5f);

	data.TexCoord = texCoord;
	//Step 4. Assign color to (GS_DATA object).Color
	data.Color = col;
	//Step 5. Append (GS_DATA object) to the TriangleStream parameter (TriangleStream::Append(...))
	triStream.Append(data);

	//TEMP
	//triStream.Append((GS_DATA)0); //Remove this line
}


[maxvertexcount(4)]
void MainGS(point VertexOut vertex[1], inout TriangleStream<GS_DATA> triStream)
{
	//Use these variable names
	float3 topLeft, topRight, bottomLeft, bottomRight;
	//float size = vertex[0].Size;
	//float hSize = vertex[0].Size / 2.0f;
	float size = gRadius;
	float hSize = gRadius / 2.0f;
	float3 origin = vertex[0].PosH.xyz;

	//Vertices (Keep in mind that 'origin' contains the center of the quad
	topLeft = float3(-hSize, hSize, 0);
	topRight = float3(hSize, hSize, 0);
	bottomLeft = float3(-hSize, -hSize, 0);
	bottomRight = float3(hSize, -hSize, 0);

	//Transform the vertices using the ViewInverse 
	//(Rotational Part Only!!! (~ normal transformation)), 
	//this will force them to always point towards the camera 
	//(cfr. BillBoarding)
	topLeft = mul(topLeft, (float3x3)gViewInverse) + origin;
	topRight = mul(topRight, (float3x3)gViewInverse) + origin;
	bottomLeft = mul(bottomLeft, (float3x3)gViewInverse) + origin;
	bottomRight = mul(bottomRight, (float3x3)gViewInverse) + origin;

	//This is the 2x2 rotation matrix we need to transform our TextureCoordinates (Texture Rotation)
	//float2x2 uvRotation = { cos(vertex[0].Rotation), -sin(vertex[0].Rotation),
	//	sin(vertex[0].Rotation), cos(vertex[0].Rotation) };

	float2x2 uvRotation = {0,0,0,0};

	//Create Geometry (Trianglestrip)
	float4 Color = float4(0, 1, 0, 1);

	CreateVertex(triStream, bottomLeft, float2(0, 1),Color, uvRotation);
	CreateVertex(triStream, topLeft, float2(0, 0), Color, uvRotation);
	CreateVertex(triStream, bottomRight, float2(1, 1), Color, uvRotation);
	CreateVertex(triStream, topRight, float2(1, 0), Color, uvRotation);
}


//MAIN PIXEL SHADER
//*****************
PS_INFO_OUT DepthPS(GS_DATA input) 
{
	PS_IFNO_OUT pout;

	float3 N;
	N.xy = input.TexCoord * 2.0 - 1.0;
	float r2 = dot(N.xy, N.xy);
	if (r2 > 1.0) discard;
	N.z = -sqrt(1.0 - r2);


	////calculate depth
	float4 pixelPos = float4(input.EyePos + N*gRadius, 1.0);
	float4 clipSpacePos = mul(pixelPos, gProjection);
	float depth = clipSpacePos.z / clipSpacePos.w;
	

	pout.Info = float2(0.f, depth);
	
	//Normal visualisation
	//float diffuse = max(0.05, dot(N, gLightDirection));
	//pout.Color = diffuse * float4(0.3, 0.3, 0.8, 1);

	//thickness visualisation (uncomment in technique)
	//float additive = 1.0f;
	//pout.Color = float4(additive, additive, additive, .05f);

	return pout;
}

PixelOut DebugPS(GS_DATA input)
{
	PixelOut pout;
	pout.Color = float4(0, 1, 0, 1);

	float3 N;
	N.xy = input.TexCoord * 2.0 - 1.0;
	float r2 = dot(N.xy, N.xy);
	if (r2 > 1.0) discard;
	N.z = -sqrt(1.0 -r2);

	float diffuse = max(0.05, dot(N, gLightDirection));
	pout.Color = diffuse * float4(0.3, 0.3, 0.8, 1);



	return pout;
}

PS_INFO_OUT PS_PP(PS_PP_INPUT input)
{
	PS_INFO_OUT output;

	return output;
}

PS_INFO_OUT ThicknessPS(GS_DATA input)
{
	PS_INFO_OUT pout;
	tout.Info = float2(.03f, 0.0f);
	return tout;

}


//TECHNIQUES
//**********
technique10 DefaultTechnique {
	pass p0 {
		//SetBlendState(gAdditive, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		//SetDepthStencilState(gNoDepth, 0);
		SetBlendState(gAdditive, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetDepthStencilState(gNoDepth, 0);

		SetRasterizerState(gRS_NoCulling);
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader(CompileShader(gs_4_0, MainGS()));
		SetPixelShader(CompileShader(ps_4_0, ThicknessPS()));
	
	}
	pass p1 {
		SetBlendState(NULL);
		SetDepthStencilState(NULL);
		SetVertexShader(NULL);
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, DepthPS()));
	}
}
technique10 DebugTechnique
{
	pass p0
	{
		SetRasterizerState(gRS_NoCulling);
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader(CompileShader(gs_4_0, MainGS()));
		SetPixelShader(CompileShader(ps_4_0, DebugPS()));

	}
}

technique10 BlurTechnique
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, VS_PP()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS_PP()));
	};
};