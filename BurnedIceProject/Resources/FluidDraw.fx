//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samPoint
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = Mirror;
	AddressV = Mirror;
};

float4x4 gViewProjInverse;
float4 gInvTexScale;
float4 gClipPosToEye;
Texture2D gTextureInfo;
Texture2D gTexture;
float3 gLightDirection = float3(-0.577f, -0.577f, 0.577f);

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

BlendState gBlend
{
	BlendEnable[0] = TRUE;
	SrcBlend = SRC_ALPHA;
	DestBlend = INV_SRC_ALPHA;
	BlendOp = ADD;
	SrcBlendAlpha = ZERO;
	DestBlendAlpha = ZERO;
	BlendOpAlpha = ADD;
	RenderTargetWriteMask[0] = 0x0F;
};
/// Create Rasterizer State (Backface culling) 
RasterizerState Solid
{
	CullMode = BACK;
};

//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 Color : SV_TARGET0;
};


//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	output.Position = float4(input.Position, 1.0f);
	output.TexCoord = input.TexCoord;
	// Set the Position
	// Set the TexCoord

	return output;
}

float3 uvToEye(float2 texCoord, float depth)
{
	//float4 pos;
	//pos.xy = texCoord * 2 - 1;
	//pos.z = depth;
	//pos.w = 1.0f;
	////pos = gViewProjInverse * pos;
	//pos /= pos.w;
	//

	float z = depth;
	float x = texCoord.x * 2 - 1;
	float y = (1 - texCoord.y) * 2 - 1;
	float4 vProjectedPos = float4(x, y, z, 1.0f);
	float4 vPositionVS = mul(vProjectedPos, gViewProjInverse);
	return (vPositionVS.xyz / vPositionVS.w);

	///float2 uv = float2(texCoord.x * 2.0f - 1.0f, (1.0f - texCoord.y) * 2.0f - 1.0f) * gClipPosToEye.xy;
	///return float3(-uv * depth, depth);
	//return pos.xyz;
}

//PIXEL SHADER
//------------
PS_OUTPUT PS(PS_INPUT input)
{
	float2 depthThickness = gTextureInfo.Sample(samPoint, input.TexCoord);
	//float4 color = gTexture.Sample(samPoint, input.TexCoord);
	//if (depthThickness.x >= .99999f)
	//	discard;
	if (depthThickness.x < 0.00001f)
		discard;

	float2 uvCoord = float2(input.TexCoord.x, input.TexCoord.y);
	float3 posEye = uvToEye(uvCoord, depthThickness.x);

	//calculate differences

	float2 ddxTC = input.TexCoord + float2(gInvTexScale.x, 0.0f);
	float2 ddx2TC = input.TexCoord + float2(-gInvTexScale.x, 0.0f);
	float2 ddyTC = input.TexCoord + float2(0.0f, gInvTexScale.y);
	float2 ddy2TC = input.TexCoord + float2(0.0f,- gInvTexScale.y);

	float ddxDepth = gTextureInfo.Sample(samPoint, ddxTC).x;
	float ddx2Depth = gTextureInfo.Sample(samPoint, ddx2TC).x;
	float ddyDepth = gTextureInfo.Sample(samPoint, ddyTC).x;
	float ddy2Depth = gTextureInfo.Sample(samPoint, ddy2TC).x;

	float3 ddx = uvToEye(ddxTC, ddxDepth) - posEye;
	float3 ddy = uvToEye(ddyTC, ddyDepth) - posEye;
	float3 ddx2 = posEye - uvToEye(ddx2TC, ddx2Depth);
	float3 ddy2 = posEye - uvToEye(ddy2TC, ddy2Depth);

	if (abs(ddx.z) > abs(ddx2.z))
		ddx = ddx2;
	
	if (abs(ddy2.z) < abs(ddy.z))
		ddy = ddy2;

	float3 normal = cross(ddx, ddy);
	normal = normalize(normal);


	PS_OUTPUT finalColor;
	finalColor.Color = float4(normal.x, normal.y, normal.z, 1.0f);

	float strength = dot(normal, -gLightDirection);
	float3 diffuseColor = float3(0.f, 0.0f, depthThickness.y);
	//strength = strength * 0.5 + 0.5;
	strength = saturate(strength);
	diffuseColor = diffuseColor * strength;
	finalColor.Color = float4(diffuseColor.x, diffuseColor.y, diffuseColor.z, depthThickness.y);


	//finalColor.Color = float4(depthThickness.x,  depthThickness.y, 0.f, 1.f);
	//finalColor.Color = float4(depthThickness.x, depthThickness.x, depthThickness.x, 1.f);

	//float4 finalColor = float4(color.x, color.y + depthThickness.x, color.z + depthThickness.y, color.w);

	//skip the rest for now
	return finalColor;

}


//TECHNIQUE
//---------
technique11 DrawTechnique
{
	pass P0
	{
		SetBlendState(gBlend, float4(0.f, 0.f, 0.f, 0.f), 0xFFFFFFFF);
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}