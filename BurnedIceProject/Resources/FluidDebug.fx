//GLOBAL VARIABLES
//****************
cbuffer cbPerObject
{
	float4x4 gWorldViewProj : WORLDVIEWPROJECTION;
	float4x4 gWorld : WORLD;
	float4x4 gWorldView : WORLDVIEW;
	float4x4 gViewInverse : ViewInverse;
	float4x4 gProjection : PROJECTION;
	float3 gEyePos: EyePosition;

};

float gRadius = .2f;
Texture2D gTexture;
//STATES
//******
RasterizerState gRS_NoCulling { CullMode = BACK; };

SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;// or Mirror or Clamp or Border
	AddressV = Wrap;// or Mirror or Clamp or Border
};

BlendState gNoBlend
{
	BlendEnable[0] = FALSE;
};

DepthStencilState gDepthState
{
	DepthEnable = true;
	StencilEnable = false;
};

//STRUCTS
//*******

struct VertexIn
{
	float3 Pos : POSITION;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
};

struct GS_DATA
{
	float4 Position: SV_POSITION;
	float4 EyePos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 Color: COLOR;
};

struct PS_OUT
{
	float4 Color : SV_TARGET0;
};

//MAIN VERTEX SHADER
//******************
VertexOut MainVS(VertexIn vin)
{
	VertexOut vout;

	vout.PosH = float4(vin.Pos, 1.0f); // mul(float4(vin.Pos, 1.0f), gWorldViewProj);

	return vout;
}

//GEOMETRY SHADER
//***************

void CreateVertex(inout TriangleStream<GS_DATA> triStream, float3 pos, float2 texCoord, float4 col, float2x2 uvRotation)
{
	//Step 1. Create a GS_DATA object
	GS_DATA data;

	//Step 2. Transform the position using the WVP Matrix and assign it to (GS_DATA object).Position (Keep in mind: float3 -> float4, Homogeneous Coordinates)

	//float3 homogeneousMat = gWorldViewProj
	data.Position = mul(float4(pos, 1), gWorldViewProj);
	data.EyePos = mul(float4(pos, 1), gWorldView);

	//Step 3. Assign texCoord to (GS_DATA object).TexCoord

	//This is a little formula to do texture rotation by transforming the texture coordinates (Can cause artifacts)
	//texCoord -= float2(0.5f, 0.5f);
	//texCoord = mul(texCoord, uvRotation);
	//texCoord += float2(0.5f, 0.5f);

	data.TexCoord = texCoord;
	//Step 4. Assign color to (GS_DATA object).Color
	data.Color = col;
	//Step 5. Append (GS_DATA object) to the TriangleStream parameter (TriangleStream::Append(...))
	triStream.Append(data);

	//TEMP
	//triStream.Append((GS_DATA)0); //Remove this line
}

[maxvertexcount(4)]
void MainGS(point VertexOut vertex[1], inout TriangleStream<GS_DATA> triStream)
{
	//Use these variable names
	float3 topLeft, topRight, bottomLeft, bottomRight;
	//float size = vertex[0].Size;
	//float hSize = vertex[0].Size / 2.0f;
	float size = gRadius;
	float hSize = gRadius / 2.0f;
	float3 origin = vertex[0].PosH.xyz;

	//Vertices (Keep in mind that 'origin' contains the center of the quad
	topLeft = float3(-hSize, hSize, 0);
	topRight = float3(hSize, hSize, 0);
	bottomLeft = float3(-hSize, -hSize, 0);
	bottomRight = float3(hSize, -hSize, 0);

	//Transform the vertices using the ViewInverse 
	//(Rotational Part Only!!! (~ normal transformation)), 
	//this will force them to always point towards the camera 
	//(cfr. BillBoarding)
	topLeft = mul(topLeft, (float3x3)gViewInverse) + origin;
	topRight = mul(topRight, (float3x3)gViewInverse) + origin;
	bottomLeft = mul(bottomLeft, (float3x3)gViewInverse) + origin;
	bottomRight = mul(bottomRight, (float3x3)gViewInverse) + origin;

	//This is the 2x2 rotation matrix we need to transform our TextureCoordinates (Texture Rotation)
	//float2x2 uvRotation = { cos(vertex[0].Rotation), -sin(vertex[0].Rotation),
	//	sin(vertex[0].Rotation), cos(vertex[0].Rotation) };

	float2x2 uvRotation = { 0,0,0,0 };

	//Create Geometry (Trianglestrip)
	float4 Color = float4(0, 1, 0, 1);

	CreateVertex(triStream, bottomLeft, float2(0, 1), Color, uvRotation);
	CreateVertex(triStream, topLeft, float2(0, 0), Color, uvRotation);
	CreateVertex(triStream, bottomRight, float2(1, 1), Color, uvRotation);
	CreateVertex(triStream, topRight, float2(1, 0), Color, uvRotation);
}

//MAIN PIXEL SHADER
//*****************
PS_OUT DepthPS(GS_DATA input)
{
	PS_OUT dOut;


	float3 N;
	N.xy = input.TexCoord * 2.0 - 1.0;
	float r2 = dot(N.xy, N.xy);
	if (r2 > 1.0) discard;


	dOut.Color = gTexture.Sample(samLinear, input.TexCoord);

	return dOut;
}

//TECHNIQUES
//**********
technique10 DebugTechnique {
	pass p0 {
		SetBlendState(gNoBlend, float4(0.f, 0.f, 0.f, 0.f), 0xFFFFFFFF);
		SetRasterizerState(gRS_NoCulling);
		SetDepthStencilState(gDepthState, 0);
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader(CompileShader(gs_4_0, MainGS()));
		SetPixelShader(CompileShader(ps_4_0, DepthPS()));
	}
}
