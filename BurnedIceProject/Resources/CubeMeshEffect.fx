
//GLOBAL VARIABLES
//****************
cbuffer cbPerObject
{
	float4x4 gWorldViewProj : WORLDVIEWPROJECTION;
	float4x4 gWorld : WORLD;
};

float3 gLightDirection = float3(-0.577f, -0.577f, 0.577f);
Texture2D gDiffuseMap;

//STATES
//******
RasterizerState gRS_NoCulling { CullMode = BACK; };

SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;// or Mirror or Clamp or Border
	AddressV = Wrap;// or Mirror or Clamp or Border
};

//STRUCTS
//*******

struct VertexIn
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
};

struct PixelOut
{
	float4 Color : SV_TARGET;
};


//MAIN VERTEX SHADER
//******************
VertexOut MainVS(VertexIn vin)
{
	VertexOut vout;

	vout.PosH = mul(float4(vin.Pos, 1.0f), gWorldViewProj);
	//vout.Normal = vin.Normal;
	vout.Normal = normalize(mul(vin.Normal, (float3x3)gWorld));
	vout.TexCoord = vin.TexCoord;

	return vout;
}

//MAIN PIXEL SHADER
//*****************
PixelOut MainPS(VertexOut pin)
{
	PixelOut pout;
	float4 diffuseColor = gDiffuseMap.Sample(samLinear, pin.TexCoord);
	float strength = dot(pin.Normal, -gLightDirection);
	//strength = strength * 0.5 + 0.5;
	strength = saturate(strength);
	pout.Color = diffuseColor * strength;
	pout.Color.w = 1.0f;
	return pout;
}


//TECHNIQUES
//**********
technique10 DefaultTechnique {
	pass p0 {
		SetRasterizerState(gRS_NoCulling);
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MainPS()));

	}
}