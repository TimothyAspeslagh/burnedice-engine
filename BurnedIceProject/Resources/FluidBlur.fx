//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samPoint
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = Mirror;
	AddressV = Mirror;
};

Texture2D gDepthTexture;
Texture2D gThicknessTexture;

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};
/// Create Rasterizer State (Backface culling) 
RasterizerState Solid
{
	CullMode = BACK;
};

BlendState gNoBlend
{
	BlendEnable[0] = FALSE;
};

//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};

struct PS_OUTPUT
{
	float2 Output : SV_TARGET0;
};


//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	output.Position = float4(input.Position, 1.0f);
	output.TexCoord = input.TexCoord;
	// Set the Position
	// Set the TexCoord

	return output;
}


//PIXEL SHADER
//------------

PS_OUTPUT PSX(PS_INPUT input)
{
	int NUM_SAMPLES = 4;

	float depth = gDepthTexture.Sample(samPoint, input.TexCoord);
	float thickness = gThicknessTexture.Sample(samPoint, input.TexCoord);
	float result = depth;
	float normalization = 1;
	//for (int i = 0; i < NUM_SAMPLES; ++i)
	//{
	//	float depthSample = gDepthTexture.Sample(samPoint, input.TexCoord);
	//	float gaussianCoeff = 
	//}

	PS_OUTPUT pOut;
	pOut.Output = float2(depth, thickness);
	return pOut;
}

PS_OUTPUT PSY(PS_INPUT input)
{
	int NUM_SAMPLES = 4;

	float depth = gDepthTexture.Sample(samPoint, input.TexCoord);
	float thickness = gThicknessTexture.Sample(samPoint, input.TexCoord);
	float result = depth;
	float normalization = 1;
	//for (int i = 0; i < NUM_SAMPLES; ++i)
	//{
	//	float depthSample = gDepthTexture.Sample(samPoint, input.TexCoord);
	//	float gaussianCoeff = 
	//}

	PS_OUTPUT pOut;
	pOut.Output = float2(depth, thickness);
	return pOut;
}
/*PS_OUTPUT PS(PS_INPUT input)
{
	float depth = gDepthTexture.Sample(samPoint, input.TexCoord);
	if (depth < 0.0001f)
		discard;
	float thickness = gThicknessTexture.Sample(samPoint, input.TexCoord);
	PS_OUTPUT finalColor;
	//finalColor.Output = float2(depth, thickness);

	//VARIABLES
	float filterRadius = 5.0f;
	float blurScale = 3.0f;
	float2 blurDir = float2(1.0f, 1.0f);
	float blurDepthFalloff = 3.0f;
	//BLUR CODE
	float sum = 0;
	float wsum = 0;
	for (float x = -filterRadius; x <= filterRadius; x += 1.0)
	{
		float depthSample = gDepthTexture.Sample(samPoint, input.TexCoord + x*blurDir);

		//spatial domain
		float r = x * blurScale;
		float w = exp(-r*r);

		//range domain
		float r2 = (depthSample - depth) * blurDepthFalloff;
		float g = exp(-r2*r2);
		sum += depthSample * w * g;
		wsum += w * g;
	}

	if (wsum > 0.0f)
		sum /= wsum;
	finalColor.Output = float2(sum, 0);

	//skip the rest for now
	return finalColor;
}

*/

//GAUSSIAN
//------------
PS_OUTPUT PSG(PS_INPUT input)
{
	float depth = gDepthTexture.Sample(samPoint, input.TexCoord);
	float thickness = gThicknessTexture.Sample(samPoint, input.TexCoord);
	if (thickness < 0.0001f)
		discard;

	float2 finalColor = float2(depth, thickness);
	float width;
	float height;
	// Step 1: find the dimensions of the texture (the texture has a method for that)	
	gDepthTexture.GetDimensions(width, height);

	// Step 2: calculate dx and dy	
	float dx = 1 / width;
	float dy = 1 / height;

	// Step 3: Create a double for loop (5 iterations each)
	//		   Inside the look, calculate the offset in each direction. Make sure not to take every pixel but move by 2 pixels each time
	//			Do a texture lookup using your previously calculated uv coordinates + the offset, and add to the final color

	float offsetX = input.TexCoord.x;
	float offsetY = input.TexCoord.y;

	for (int i = 0; i < 5; ++i)
	{
		for (int e = 0; e < 5; ++e)
		{
			float2 texCoordCustom;
			texCoordCustom.x = offsetX + i * dx * 2;
			texCoordCustom.y = offsetY + e * dy * 2;
			float addedDepth = gDepthTexture.Sample(samPoint, texCoordCustom);
			float addedThickness = gThicknessTexture.Sample(samPoint, texCoordCustom);
			finalColor += float2(addedDepth, addedThickness);
		}
	}

	// Step 4: Divide the final color by the number of passes (in this case 5*5)	
	finalColor /= 25;

	// Step 5: return the final color
	PS_OUTPUT output;
	output.Output = finalColor;
	//output.Output.x = depth;
	//output.Output = float2(depth, thickness);
	return output;
}



//TECHNIQUE
//---------
technique11 BlurXTechnique
{
	pass P0
	{
		SetBlendState(gNoBlend, float4(0.f, 0.f, 0.f, 0.f), 0xFFFFFFFF);
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		//SetPixelShader(CompileShader(ps_4_0, PSX()));
		SetPixelShader(CompileShader(ps_4_0, PSG()));
	}
}

technique11 BlurYTechnique
{
	pass P0
	{
		SetBlendState(gNoBlend, float4(0.f, 0.f, 0.f, 0.f), 0xFFFFFFFF);
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PSY()));
	}
}
