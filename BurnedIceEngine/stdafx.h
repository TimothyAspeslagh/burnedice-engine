#pragma once

#pragma region WINDOWS

#include <stdio.h> 
#include <stdlib.h>


#include <windows.h>
#include <Winuser.h>
#include <wchar.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <memory>
using namespace std;

#pragma endregion 

#pragma region DirectX

#include <dxgi.h>
#pragma comment(lib, "dxgi.lib")

#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")

#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

#include <DirectXMath.h>
#include <DirectXColors.h>
#include <DirectXPackedVector.h>
#include <DirectXCollision.h>

using namespace DirectX;

#pragma endregion

#pragma region Assimp

#if defined (DEBUG) || defined(_DEBUG)
#pragma comment(lib, "assimpd.lib")
#else
#pragma comment(lib,  "assimp.lib")
#endif

#include <assimp\cimport.h>
#include <assimp\types.h>
#pragma endregion


#pragma region PhysX
#include <PxPhysicsAPI.h>
using namespace physx;

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "PxTaskDEBUG.lib")
#pragma comment(lib, "PhysX3DEBUG_x86.lib")
#pragma comment(lib, "PhysX3CommonDEBUG_x86.lib")
#pragma comment(lib, "PhysX3ExtensionsDEBUG.lib")
#pragma comment(lib, "PhysXProfileSDKDEBUG.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDKDEBUG.lib")
#pragma comment(lib, "PhysX3CharacterKinematicDEBUG_x86.lib")

#else 
#pragma comment(lib, "PxTask.lib")
#pragma comment(lib, "PhysX3_x86.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PhysXProfileSDK.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDK.lib")
#pragma comment(lib, "PhysX3CharacterKinematic_x86.lib")
#endif
#pragma endregion

#pragma region FleX
#include <NvFlex.h>
#include <NvFlexExt.h>
#if defined (DEBUG) || defined(_DEBUG)
#pragma comment(lib, "NvFlexDebugD3D_x86.lib")
#pragma comment(lib, "NvFlexDeviceDebug_x86.lib")
#pragma comment(lib, "NvFlexExtDebugD3D_x86.lib")
#else
#pragma comment(lib, "NvFlexDeviceRelease_x86.lib")
#pragma comment(lib, "NvFlexExtReleaseD3D_x86.lib")
#pragma comment(lib, "NvFlexReleaseD3D_x86.lib")
#endif
#pragma endregion

//**EFFECTS11 (Helper for loading Effects (D3DX11))
//https://fx11.codeplex.com/
#include "d3dx11effect.h" //[AdditionalLibraries/DX_Effects11/include/d3dx11effect.h]
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "DxEffects11_vc14_Debug.lib")
#else 
#pragma comment(lib, "DxEffects11_vc14_Release.lib")
#endif


//*DXTEX (Helper for loading Textures (D3DX11))
//http://directxtex.codeplex.com/
#include "DirectXTex.h"
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "DxTex_vc14_Debug.lib")
#else 
#pragma comment(lib, "DxTex_vc14_Release.lib")
#endif


#pragma region Engine Headers

#include "../BurnedIceEngine/Helpers/IceMath.h"
#include "../BurnedIceEngine/Helpers/IceString.h"
#include "../BurnedIceEngine/Helpers/IceGeneral.h"
#include "../BurnedIceEngine/Helpers/IceGraphics.h"
#include "../BurnedIceEngine/Managers/MainManager.h"

#pragma endregion