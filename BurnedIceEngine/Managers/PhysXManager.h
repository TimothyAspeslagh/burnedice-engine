#pragma once
class PhysXErrorCallback;

class PhysXManager : public PxVisualDebuggerConnectionHandler
{
public:
	PhysXManager();
	~PhysXManager();

	void Initialise();
	void Tick();
	void Clean();

	PxPhysics* GetPhysX() const { return m_pPhysics; }
	PxDefaultCpuDispatcher* GetCpuDispatcher() const { return m_pDefaultCpuDispatcher; }
	bool ToggleVisualDebuggerConnection() const;
	//PxConvexMesh* CookConvexMesh(vector<iVertex> vertices);
	PxVec3 GetGravity() const { return m_Gravity; }


	//PVD
	virtual void onPvdSendClassDescriptions(PxVisualDebuggerConnection& connection);
	virtual void onPvdConnected(PxVisualDebuggerConnection& connection);
	virtual void onPvdDisconnected(PxVisualDebuggerConnection& connection);

private:
	PxFoundation * m_pFoundation = nullptr;
	PxProfileZoneManager* m_pProfileZoneManager = nullptr;
	PxPhysics* m_pPhysics = nullptr;
	PxVec3 m_Gravity = PxVec3(0.0f, -9.81f, 0.0f);
	PxDefaultCpuDispatcher* m_pDefaultCpuDispatcher = nullptr;
	PxCudaContextManager * m_pCudaContextManager = nullptr;
	//PxCooking* m_pCooking = nullptr;
	PhysXErrorCallback * m_pDefaultErrorCallback = nullptr;
	//PxDefaultAllocator * m_pDefaultAllocatorCallback = nullptr;
};

