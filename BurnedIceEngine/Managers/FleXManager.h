#pragma once
class FleXManager
{
public:
	FleXManager();
	~FleXManager();

	void Initialise();
	void Tick();
	void Clean();

	NvFlexLibrary* GetLibrary() const { return m_pLibrary; }

private:
	NvFlexLibrary* m_pLibrary = nullptr;
};

