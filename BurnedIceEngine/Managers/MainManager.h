#pragma once
#include "TimeManager.h"
#include "../Scene Classes/Components/CameraComponent.h"
#include "../Scene Classes/GameScene.h"
#include "InputManager.h"
#include "ContentManager.h"
#include "PhysXManager.h"
#include "FleXManager.h"

class BurnedIceGame;

class MainManager
{
public:
	MainManager();
	~MainManager();

	void Initialise(ID3D11Device* device, ID3D11DeviceContext * deviceContext, GameSettings* settings, HWND * pWindow, BurnedIceGame * pGame, bool flexEnabled);
	void Tick();
	void Clean();
	void SetActiveScene(GameScene* scene)
	{
		m_pActiveScene = scene;
	}

	const CameraComponent* GetActiveCamera() const
	{
		if (m_pActiveScene != nullptr)
			return m_pActiveScene->GetActiveCamera();

		return nullptr;
	}

	bool GetFleXEnabled() const { return m_FlexEnabled; }

	//read only
	const GameSettings* GetGameSettings() const { return m_Settings;  }
	const float4 GetClipPosToEye() const;

	const float GetDeltaTime()const { return Time->GetDeltaTime(); }

	ID3D11RenderTargetView * GetDefaultRenderTargetView() const;
	ID3D11DepthStencilView* GetDefaultDSV() const;
	void BindDefaultRenderTarget() const;
	ID3D11ShaderResourceView* GetSRV() const;
	ID3D11Device * m_pDevice;
	HWND * m_pWindow;
	ID3D11DeviceContext* m_pDeviceContext;
	TimeManager * Time = nullptr;
	InputManager * Input = nullptr;
	ContentManager * Content = nullptr;
	GameScene* m_pActiveScene = nullptr;
	PhysXManager * Physics = nullptr;
	FleXManager * FleX = nullptr;


private:
	bool m_FlexEnabled = false;
	GameSettings* m_Settings;

	BurnedIceGame * m_pGame = nullptr;


};

