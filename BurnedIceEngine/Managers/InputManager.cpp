﻿#include "stdafx.h"
#include "InputManager.h"

InputManager::InputManager()
{
}

InputManager::~InputManager()
{

}

void InputManager::Initialise(HWND * pWindow)
{
	for(int i = 0x01; i <= 0xFE; ++i)
	{
		InputState state = InputState::UP;
		InputInfo info(state, 0.f);
		
	
		m_InputMap.insert(pair<int, InputInfo>(i, info));
	}
	m_InputMap.at('b').SetState(InputState::DOWN, 0.f);
	m_IsInitialised = true;
	m_pWindow = pWindow;
}

void InputManager::Tick()
{
	if(!m_ProcessedThisFrame && m_ProcessNextFrame)
	{
		ProcessKeys(false);
	}

	if(m_ProcessedThisFrame && m_ProcessNextFrame)
	{
		m_ProcessedThisFrame = false;
	}

}

void InputManager::Clean()
{
	m_pWindow = nullptr;
}

void InputManager::ProcessKeys(bool processNextFrame)
{
	for(int i = 0x01; i <= 0xFE; ++i)
	{
		bool stateSet = false;
		SHORT result = GetAsyncKeyState(i); //has 8 bits
		InputState processedState = InputState::UP;
		InputState currentState = m_InputMap.at(i).GetState();

		if(result == -32767 || result == -32768) //if key is down, the highest bit (the sign bit) is set to one
		{
			processedState = InputState::DOWN;
		}

		//if the key is held down, the last frame will have the down state
		//but in this frame, it needs to be set to pressed
		if(currentState == InputState::DOWN && processedState == InputState::DOWN)
		{
			m_InputMap.at(i).SetState(InputState::PRESSED, 0.0f);
			stateSet = true; //todo : break;
		}

		if(currentState != processedState && stateSet == false)
		{
			if(!(currentState == InputState::PRESSED && processedState == InputState::DOWN))
				m_InputMap.at(i).SetState(processedState, 0.0f);
		}
	}
	m_ProcessNextFrame = processNextFrame;
	m_ProcessedThisFrame = true;
}


bool InputManager::IsKeyDown(int vKey) const
{
	return CompareInputstates(vKey, InputState::DOWN);
}

bool InputManager::IsKeyUp(int vKey) const
{
	return CompareInputstates(vKey, InputState::UP);
}

bool InputManager::IsKeyPressed(int vKey) const
{
	return CompareInputstates(vKey, InputState::PRESSED);
}

XMFLOAT2 InputManager::GetMousePosition()
{
	POINT pos;
	BOOL result = GetCursorPos(&pos);

	if (!result)
		return XMFLOAT2();

	result = ScreenToClient(*m_pWindow, &pos);

	if (!result)
		return XMFLOAT2();

	XMFLOAT2 position = XMFLOAT2(pos.x, pos.y);
	return position;
}

XMFLOAT2 InputManager::GetWindowsMousePosition()
{
	POINT pos;
	BOOL result = GetCursorPos(&pos);
	if (!result)
		return XMFLOAT2();
	
	return XMFLOAT2(pos.x, pos.y);
}

bool InputManager::CompareInputstates(int vKey, InputState state) const
{
	if (!m_IsInitialised)
		return false;

	if (m_InputMap.at(vKey).GetState() != state)
		return false;


	return true;
}

bool InputManager::IsKeyDown(char key) const
{
	return IsKeyDown(toupper((int)key));
}

bool InputManager::IsKeyUp(char key) const
{
	return IsKeyUp(toupper((int)key));
}

bool InputManager::IsKeyPressed(char key) const
{
	return IsKeyPressed(toupper((int)key));
}