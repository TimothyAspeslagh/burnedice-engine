#include "stdafx.h"
#include "TimeManager.h"


TimeManager::TimeManager()
{
	__int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	m_SecondsPerCount = 1.0f / (float)countsPerSec;
}


TimeManager::~TimeManager()
{
}

void TimeManager::Tick()
{
	__int64 currentTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);
	m_DeltaTime = (float)(currentTime - m_LastTickTime) * m_SecondsPerCount;
	if (m_DeltaTime < 0.0f)
		m_DeltaTime = 0.0f;

	m_LastTickTime = currentTime;
	m_Time += m_DeltaTime;
}
