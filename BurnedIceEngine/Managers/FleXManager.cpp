#include "stdafx.h"
#include "FleXManager.h"


FleXManager::FleXManager()
{
}


FleXManager::~FleXManager()
{
}

void FleXManager::Initialise()
{
	m_pLibrary = NvFlexInit();
}


void FleXManager::Tick()
{

}

void FleXManager::Clean()
{
	NvFlexShutdown(m_pLibrary);
	m_pLibrary = nullptr;
}
