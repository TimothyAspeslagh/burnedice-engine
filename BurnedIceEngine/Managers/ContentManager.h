﻿#pragma once
#include "../Content/Mesh.h"
#include "../Content/Material.h"
#include "../Content/Texture.h"

class ContentManager
{
public:
	ContentManager(MainManager* pParent) : m_pParent(pParent) {}
	~ContentManager(){}

	void Initialise();
	void Tick();
	void Clean();

	
	Mesh* LoadMesh(DesignString assetFile, DesignString meshID);
	Material* LoadMaterial(DesignString assetFile, DesignString materialID);
	template<class T> T* LoadMaterial(DesignString assetFile, DesignString materialID);
	Texture* LoadTexture(DesignString assetFile, DesignString textureID);

	Mesh* GetMesh(DesignString meshID) { return m_MeshMap.at(meshID); }
	Material* GetMaterial(DesignString materialID) { return m_MaterialMap.at(materialID); }
	//template<class T> T* GetMaterial(DesignString materialID) { return m_MaterialMap.at(materialID); }
	Texture* GetTexture(DesignString textureID) { return m_TextureMap.at(textureID); }

private:
	MainManager* m_pParent;
	map<DesignString, Mesh*> m_MeshMap;
	map<DesignString, Material*> m_MaterialMap;
	map<DesignString, Texture*> m_TextureMap;
	
};

template<class T>
T* ContentManager::LoadMaterial(DesignString assetFile, DesignString materialID)
{

	for (auto it = m_MaterialMap.begin(); it != m_MaterialMap.end(); ++it)
	{
		if (it->first.Compare(materialID))
			return dynamic_cast<T*>(m_MaterialMap.at(materialID));
	}

	auto newMaterial = new T(assetFile, materialID);

	if (dynamic_cast<Material*>(newMaterial) == nullptr)
		return nullptr;

	newMaterial->Initialise(*m_pParent);
	m_MaterialMap.insert(std::pair<DesignString, T*>(materialID, newMaterial));
	return newMaterial;

}
