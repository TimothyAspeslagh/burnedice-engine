﻿#include "stdafx.h"
#include "ContentManager.h"
#include "../Content/Texture.h"

void ContentManager::Initialise()
{
}

void ContentManager::Tick()
{
}

void ContentManager::Clean()
{
	for(auto it = m_MaterialMap.begin(); it != m_MaterialMap.end(); ++it)
	{
		it->second->RootClean(); //Can inherit from base class Material
	}

	for(auto it = m_MeshMap.begin(); it != m_MeshMap.end(); ++it)
	{
		it->second->Clean();
	}

	for(auto it = m_TextureMap.begin(); it != m_TextureMap.end(); ++it)
	{
		it->second->Clean();
	}

	m_MaterialMap.clear();
	m_MeshMap.clear();
	m_TextureMap.clear();
}

//todo: refactor these functions in one compact template function

Mesh* ContentManager::LoadMesh(DesignString assetFile, DesignString meshID)
{
	for(auto it = m_MeshMap.begin(); it != m_MeshMap.end(); ++it)
	{
		if (it->first.Compare(meshID))
			return m_MeshMap.at(meshID);
	}

	auto newMesh = new Mesh(assetFile, meshID);
	newMesh->Initialise(*m_pParent);
	m_MeshMap.insert(std::pair<DesignString, Mesh*>(meshID, newMesh));
	return newMesh;
}

Material* ContentManager::LoadMaterial(DesignString assetFile, DesignString materialID)
{
	for (auto it = m_MaterialMap.begin(); it != m_MaterialMap.end(); ++it)
	{
		if (it->first.Compare(materialID))
			return m_MaterialMap.at(materialID);
	}

	auto newMaterial = new Material(assetFile, materialID);
	newMaterial->Initialise(*m_pParent);
	m_MaterialMap.insert(std::pair<DesignString, Material*>(materialID, newMaterial));
	return newMaterial;
}

Texture* ContentManager::LoadTexture(DesignString assetFile, DesignString textureID)
{
	for(auto it = m_TextureMap.begin(); it != m_TextureMap.end(); ++it)
	{
		if (it->first.Compare(textureID))
			return m_TextureMap.at(textureID);
	}

	auto newTexture = new Texture(assetFile, textureID);
	newTexture->Initialise(*m_pParent);
	m_TextureMap.insert(std::pair<DesignString, Texture*>(textureID, newTexture));
	return newTexture;
}
