#include "stdafx.h"
#include "MainManager.h"
#include "../Main/BurnedIceGame.h"

MainManager::MainManager()
{
}


MainManager::~MainManager()
{
}

void MainManager::Initialise(ID3D11Device* device, ID3D11DeviceContext * deviceContext, GameSettings* settings, HWND * pWindow, BurnedIceGame * pGame, bool flexEnabled) //BurnedIceGame * pGame,
{
	m_pDevice = device;
	m_pDeviceContext = deviceContext;
	Time = new TimeManager();
	Input = new InputManager();
	Input->Initialise(pWindow);
	Content = new ContentManager(this);
	Content->Initialise();
	Physics = new PhysXManager();
	Physics->Initialise();

	m_pGame = pGame;
	m_FlexEnabled = flexEnabled;

	if(flexEnabled)
	{
		FleX = new FleXManager();
		FleX->Initialise();
	}
	m_Settings = settings;
	m_pWindow = pWindow;
}

void MainManager::Tick()
{
	Time->Tick();
	Input->Tick();
	Content->Tick();
	Physics->Tick();

	if(m_FlexEnabled)
		FleX->Tick();
}

void MainManager::Clean()
{
	m_pGame = nullptr;

	delete Time;
	Time = nullptr;

	Input->Clean();
	delete Input;
	Input = nullptr;

	Content->Clean();
	delete Content;
	Content = nullptr;

	if (m_FlexEnabled)
	{
		FleX->Clean();
		delete FleX;
		FleX = nullptr;
	}


	Physics->Clean();
	delete Physics;
	Physics = nullptr;

	m_pActiveScene = nullptr;
	m_pWindow = nullptr;
}

const float4 MainManager::GetClipPosToEye() const
{
	float x = tanf((m_pActiveScene->GetActiveCamera()->GetFOV() * 0.5f) * (m_Settings->WindowWidth / m_Settings->WindowHeight));
	float y = tanf(m_pActiveScene->GetActiveCamera()->GetFOV() * 0.5f);
	float4 result = float4(x, y, 0.0f, 0.0f);
	return result;
}

ID3D11RenderTargetView* MainManager::GetDefaultRenderTargetView() const
{
	return m_pGame->GetDefaultRenderTargetView();
}

ID3D11DepthStencilView* MainManager::GetDefaultDSV() const
{
	return m_pGame->GetDefaultDSV();
}

void MainManager::BindDefaultRenderTarget() const
{
	m_pGame->BindDefaultRenderTargetView();
}

ID3D11ShaderResourceView* MainManager::GetSRV() const
{
	return m_pGame->GetSRV();
}
