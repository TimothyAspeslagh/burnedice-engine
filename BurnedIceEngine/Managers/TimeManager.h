#pragma once
class TimeManager
{
public:
	TimeManager();
	~TimeManager();

	void Tick();
	__int64 GetTotalTime() const { return m_Time; }
	float GetDeltaTime() const { return m_DeltaTime; }
private:
	float m_Time = 0;
	__int64 m_LastTickTime = 0;
	float m_DeltaTime = 0.0f;
	float m_SecondsPerCount = 0.0f;

};

