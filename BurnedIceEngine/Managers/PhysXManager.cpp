#include "stdafx.h"
#include "PhysXManager.h"
#include "../PhysX/physXCallback.h"

static const char* DEFAULT_PVD_HOST = "127.0.0.1";
static const PxU32 DEFAULT_PVD_PORT = 5425;

PhysXManager::PhysXManager()
{
}


PhysXManager::~PhysXManager()
{
}

void PhysXManager::Initialise()
{
	static PxDefaultAllocator gDefaultAllocatorCallback;
	m_pDefaultErrorCallback = new PhysXErrorCallback();

	m_pFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, *m_pDefaultErrorCallback);
	if (m_pFoundation == nullptr)
	{
		wcout << L"PhysX: Failed to create m_pFoundation!" << endl;
		assert(false);
	}

	bool recordMemoryAllocations = false;
#if defined (DEBUG) || defined(_DEBUG)
	recordMemoryAllocations = true;
#endif
	m_pProfileZoneManager = &PxProfileZoneManager::createProfileZoneManager(m_pFoundation);
	if(m_pProfileZoneManager== nullptr)
	{
		wcout << L"PhysX: Failed to create m_pProfileZoneManager!" << endl;
		assert(false);
	}

	m_pPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_pFoundation, PxTolerancesScale(), recordMemoryAllocations, m_pProfileZoneManager);
	if(m_pPhysics == nullptr)
	{
		wcout << L"PhysX: Failed to create m_pPhysics" << endl;
		assert(false);
	}

	//Vehicle SDK Initialization
	//PxInitVehicleSDK(*m_pPhysics);
	//
	//PxVec3 up = PxVec3(0, 1, 0);
	//PxVec3 forward = PxVec3(0, 0, 1);
	//PxVehicleSetBasisVectors(up, forward);

	m_pDefaultCpuDispatcher = PxDefaultCpuDispatcherCreate(1);


	ToggleVisualDebuggerConnection();
	if (m_pPhysics->getPvdConnectionManager())
		m_pPhysics->getPvdConnectionManager()->addHandler(*this);


	//m_pCooking = PxCreateCooking(PX_PHYSICS_VERSION, *m_pFoundation, PxCookingParams(PxTolerancesScale()));
	//if(m_pCooking == nullptr)
	//{
	//	wcout << L"PhysX: Failed to create m_pCooking!" << endl;
	//	assert(false);
	//}
}

void PhysXManager::Tick()
{
}

void PhysXManager::Clean()
{
//	m_pCooking->release();
	//m_pCudaContextManager->release();
	m_pDefaultCpuDispatcher->release();
	m_pPhysics->release();
	m_pProfileZoneManager->release();
	m_pFoundation->release();
	delete m_pDefaultErrorCallback;
	m_pDefaultErrorCallback = nullptr;
}

bool PhysXManager::ToggleVisualDebuggerConnection() const
{
	if (!m_pPhysics || !m_pPhysics->getPvdConnectionManager()) return false;

	auto pvdConnectionManager = m_pPhysics->getPvdConnectionManager();
	if (pvdConnectionManager->isConnected())
	{
		pvdConnectionManager->disconnect();
		return false;
	}

	PxVisualDebuggerConnectionFlags connectionFlags(PxVisualDebuggerExt::getAllConnectionFlags());
	PxVisualDebuggerConnection* conn = PxVisualDebuggerExt::createConnection(pvdConnectionManager, DEFAULT_PVD_HOST, DEFAULT_PVD_PORT, 4000, connectionFlags);

	if (conn)
	{
		conn->release();
		return true;
	}
	return false;
}

void PhysXManager::onPvdSendClassDescriptions(PxVisualDebuggerConnection& connection)
{
}

void PhysXManager::onPvdConnected(PxVisualDebuggerConnection& connection)
{
	m_pPhysics->getVisualDebugger()->setVisualizeConstraints(true);
}

void PhysXManager::onPvdDisconnected(PxVisualDebuggerConnection& connection)
{
}

//PxConvexMesh* PhysXManager::CookConvexMesh(vector<iVertex> vertices)
//{
//	if (vertices.size() > 255)
//		return nullptr;
//
////	PxVec3 convexVerts[vertices.size()];
//	for(size_t i = 0; i != vertices.size(); ++i)
//	{
//		//convexVerts[i] = XmToPxVec3(vertices[i].Pos);
//	}
//
//	return nullptr;
//}
