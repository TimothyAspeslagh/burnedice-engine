﻿#pragma once
#include <map>


enum InputState
{
	UP,
	PRESSED,
	DOWN,
};

class InputInfo
{
public:
	InputInfo(InputState state, float timeStamp): State(state), Timestamp(timeStamp)
	{
		
	}
	~InputInfo() {}

	void SetState(InputState state, float timeStamp)
	{
		State = state;
		Timestamp = timeStamp;
	}
	InputState GetState() const { return State; }
	float GetTimeStamp() const { return Timestamp; }
private:
	InputState State;
	float Timestamp;
};

class InputManager
{
public:
	InputManager();
	~InputManager();

	void Initialise(HWND * pWindow);
	void Tick();
	void Clean();
	void ProcessKeys(bool processNextFrame);

	bool IsKeyDown(char key) const;
	bool IsKeyDown(int vKey) const;
	bool IsKeyUp(char key) const;
	bool IsKeyUp(int vKey) const;
	bool IsKeyPressed(char key) const;
	bool IsKeyPressed(int vKey) const;

	//in screen coordinates
	XMFLOAT2 GetMousePosition();
	XMFLOAT2 GetWindowsMousePosition();

private:
	bool CompareInputstates(int vKey, InputState state) const;
	map<int, InputInfo> m_InputMap;
	bool m_IsInitialised = false;
	bool m_ProcessNextFrame = false;
	bool m_ProcessedThisFrame = false;
	HWND * m_pWindow = nullptr;
};
