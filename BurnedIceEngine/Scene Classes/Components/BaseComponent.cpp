#include "stdafx.h"
#include "BaseComponent.h"
#include "../../Managers/MainManager.h"
#include "../GameObject.h"


BaseComponent::BaseComponent()
{
}


GameObject* BaseComponent::GetGameObject()
{
	return m_pGameObject;
}

TransformComponent* BaseComponent::GetTransform()
{
	return m_pGameObject->GetTransform();
}

void BaseComponent::SetGameObject(GameObject* obj)
{
	m_pGameObject = obj;
}

void BaseComponent::BaseInitialise(const MainManager &gameManager)
{
	Initialise(gameManager);
}

void BaseComponent::Update(const MainManager &gameManager)
{
	Tick(gameManager);
}

void BaseComponent::BaseClean()
{
	Clean();
}

void BaseComponent::BaseDraw(const MainManager &gameManager)
{
	Draw(gameManager);
}
