#pragma once
#include "BaseComponent.h"


class TransformComponent: public BaseComponent
{
public:
	TransformComponent();
	~TransformComponent();



	void Initialise(const MainManager &gameManager) override;
	void Tick(const MainManager &gameManager) override;
	void Draw(const MainManager &gameManager) override;
	void Clean() override;

	void Translate(float x, float y, float z);
	void Translate(XMFLOAT3 translation);
	void Rotate(float x, float y, float z, float w);
	void Rotate(XMFLOAT4 rotation);
	void RotateEuler(float x, float y, float z);
	void RotateEuler(XMFLOAT3 EulerRotation);
	void Scale(float x, float y, float z);
	void Scale(XMFLOAT3 scale);
	void SetAxisCorrection(bool state) { m_HasAxisCorrection = state; }
	
	XMFLOAT3  GetPosition() const { return m_Position; }
	XMFLOAT4  GetRotation() const { return m_Rotation; }
	XMFLOAT3 GetEulerRotation()const;
	XMFLOAT3 GetScale()const { return m_Scale; }
	XMFLOAT4X4  GetWorldMatrix() const { return m_World; }
	XMFLOAT4X4 GetUncorrectedWorldMatrix() const { return m_UncorrectedWorld; }

	XMFLOAT3 GetUp() const { return m_Up; }
	XMFLOAT3 GetForward() const { return m_Forward; }
	XMFLOAT3 GetRight() const { return m_Right; }

private:
	bool m_HasAxisCorrection = false;
	XMFLOAT3 m_Position;
	XMFLOAT4 m_Rotation;
	XMFLOAT3 m_Scale;

	bool m_Translate = false, m_Rotate = false;

	XMFLOAT4X4 m_World;
	XMFLOAT4X4 m_UncorrectedWorld;
	XMFLOAT3 m_Up, m_Forward, m_Right;

	friend class RigidBodyComponent;
};

