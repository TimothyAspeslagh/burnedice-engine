#pragma once
#include "BaseComponent.h"


class Collider;

enum CollisionGroupFlag : UINT32
{
	Group0 = (1 << 0),
	Group1 = (1 << 1),
	Group2 = (1 << 2),
	Group3 = (1 << 3),
	Group4 = (1 << 4),
	Group5 = (1 << 5),
	Group6 = (1 << 6),
	Group7 = (1 << 7),
	Group8 = (1 << 8),
	Group9 = (1 << 9)
};

class RigidBodyComponent :
	public BaseComponent
{
public:
	RigidBodyComponent(bool isStatic);
	~RigidBodyComponent();

	void Initialise(const MainManager& gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager& gameManager) override;

	PxRigidBody* GetPxRigidBody();
	bool IsStatic() const { return m_IsStatic; }
	bool IsKinematic() const { return m_IsKinematic; }
	void SetKinematic(bool isKinematic);

	void AddForce(const PxVec3 &force, PxForceMode::Enum mode, bool setActive = true);
	void AddTorque(const PxVec3& torque, PxForceMode::Enum mode, bool setActive = true);
	void ClearForce(PxForceMode::Enum mode);
	void ClearTorque(PxForceMode::Enum mode);
	void SetCollisionGroup(CollisionGroupFlag group);
	void SetCollisionIgnoreGroup(CollisionGroupFlag group);
	void SetDensity(float density);
	PxShape* CreateShape(const PxGeometry& geometry, const PxMaterial& material, PxShapeFlags shapeFlags);
	//void SetSimulationFilterData(const PxFilterData& data);
	void AddCollider(Collider * collider);


	void SetInactive();
protected:

	void CreateActor();
	void Translate(XMFLOAT3 pos);
	void Rotate(XMFLOAT4 rot);
	void Transform(XMFLOAT3 pos, XMFLOAT4 rot);

	XMFLOAT3 GetPosition();
	XMFLOAT4 GetRotation();

protected:
	bool m_IsStatic, m_IsKinematic;
	PxFilterData m_CollisionGroups;
	vector<Collider*> m_Colliders;
	vector<Collider*> m_CollidersToInitialise;
	PxRigidActor* m_pActor;
	PhysXManager* m_pPhysXManager = nullptr;

private:
	friend class Collider;
	friend class TransformComponent;

};

class Collider
{
public:

	Collider(std::shared_ptr<PxGeometry>& geometry, const PxMaterial& material, const PxTransform& pose);
	~Collider() {};

	PxShape* GetShape() const { return m_pShape; }
	bool IsTrigger() const { return m_IsTrigger; }
	void SetTriggerState(bool isTrigger);
	void SetShape(PxShape* shape);

private:
	shared_ptr<PxGeometry> m_GeometryVec;
	const PxMaterial& m_Material;
	const PxTransform m_Pose;
	PxShape * m_pShape;
	bool m_IsTrigger;

	friend class RigidBodyComponent;
};
