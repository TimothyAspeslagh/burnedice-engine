#include "stdafx.h"
#include "RigidBodyComponent.h"
#include "../GameObject.h"
#include "../../PhysX/PhysXProxy.h"
//#include "../../Helpers/IceMath.h"


Collider::Collider(std::shared_ptr<PxGeometry>& geometry, const PxMaterial& material, const PxTransform& pose): m_Material(material), m_GeometryVec(geometry),
m_Pose(pose), m_pShape(nullptr), m_IsTrigger(false)
{

}

void Collider::SetTriggerState(bool isTrigger)
{
	{
		m_IsTrigger = isTrigger;
		if (m_pShape)
		{
			m_pShape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, isTrigger);
			m_pShape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, !isTrigger);
		}
	}
}

void Collider::SetShape(PxShape* shape)
{
	m_pShape = shape;
	if (m_IsTrigger)
		SetTriggerState(m_IsTrigger);
}

RigidBodyComponent::RigidBodyComponent(bool isStatic) : m_IsStatic(isStatic), m_pActor(nullptr),
m_IsKinematic(false), m_CollisionGroups(PxFilterData(CollisionGroupFlag::Group0, 0, 0, 0))
{
}


RigidBodyComponent::~RigidBodyComponent()
{
}

void RigidBodyComponent::Initialise(const MainManager& gameManager)
{
	m_pPhysXManager = gameManager.Physics;
	for(auto collider : m_CollidersToInitialise)
	{
		AddCollider(collider);
	}
	m_CollidersToInitialise.clear();
	
	if (!m_pActor || m_IsStatic)
		return;

	reinterpret_cast<PxRigidDynamic*>(m_pActor)->wakeUp();

}

void RigidBodyComponent::Tick(const MainManager& gameManager)
{


	if (!m_pActor || m_IsStatic)
		return;

	//wcout << GetPosition().x << L" " << GetPosition().y << L" " << GetPosition().z << endl;
}

void RigidBodyComponent::Clean()
{
	for(auto it = m_Colliders.begin(); it != m_Colliders.end(); ++it)
	{
		delete (*it);
		(*it) = nullptr;
	}
	m_Colliders.clear();

	if (m_pActor != nullptr)
	{
		auto physxScene = GetGameObject()->GetScene()->GetPhysXProxy()->GetScene();
		physxScene->removeActor(*m_pActor);
		m_pActor->release();
	}
	m_pPhysXManager = nullptr;
}

void RigidBodyComponent::Draw(const MainManager& gameManager)
{
}

PxRigidBody* RigidBodyComponent::GetPxRigidBody()
{
	return reinterpret_cast<PxRigidBody*>(m_pActor);
}

void RigidBodyComponent::SetKinematic(bool isKinematic)
{
	if(m_IsStatic)
	{
		wcout << L"physX: Static actor cannot be made kinematic" << endl;
		return;
	}

	m_IsKinematic = isKinematic;
	
	if (m_pActor)
		GetPxRigidBody()->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, m_IsKinematic);

}

void RigidBodyComponent::AddForce(const PxVec3& force, PxForceMode::Enum mode, bool setActive)
{
	if (!m_pActor || m_IsKinematic || m_IsStatic)
		return;
	GetPxRigidBody()->addForce(force, mode, setActive);

}

void RigidBodyComponent::AddTorque(const PxVec3& torque, PxForceMode::Enum mode, bool setActive)
{
	if (!m_pActor || m_IsKinematic || m_IsStatic)
		return;
	GetPxRigidBody()->addTorque(torque, mode, setActive);

}

void RigidBodyComponent::ClearForce(PxForceMode::Enum mode)
{
	if (!m_pActor || m_IsKinematic || m_IsStatic)
		return;

	GetPxRigidBody()->clearForce(mode);
}

void RigidBodyComponent::ClearTorque(PxForceMode::Enum mode)
{
	if (!m_pActor || m_IsKinematic || m_IsStatic)
		return;

	GetPxRigidBody()->clearTorque(mode);
}

void RigidBodyComponent::SetCollisionGroup(CollisionGroupFlag group)
{
	m_CollisionGroups.word0 = group;
	for(auto it = m_Colliders.begin(); it != m_Colliders.end(); ++it)
	{
		(*it)->GetShape()->setSimulationFilterData(m_CollisionGroups);
		(*it)->GetShape()->setQueryFilterData(m_CollisionGroups);
	}
}

void RigidBodyComponent::SetCollisionIgnoreGroup(CollisionGroupFlag group)
{
	m_CollisionGroups.word1 = group;
	for(auto it = m_Colliders.begin(); it != m_Colliders.end(); ++it)
	{
		(*it)->GetShape()->setSimulationFilterData(m_CollisionGroups);
	}
}

void RigidBodyComponent::SetDensity(float density)
{
	if (m_pActor && m_pActor->isRigidBody())
		PxRigidBodyExt::updateMassAndInertia(*m_pActor->isRigidBody(), density);
}

PxShape* RigidBodyComponent::CreateShape(const PxGeometry& geometry, const PxMaterial& material, PxShapeFlags shapeFlags)
{
	if (!m_pActor)
		CreateActor();

	return m_pActor->createShape(geometry, material, shapeFlags);
	
}

void RigidBodyComponent::SetInactive()
{
	if (!m_pActor || m_IsStatic)
		return;

	reinterpret_cast<PxRigidDynamic*>(m_pActor)->putToSleep();
}

void RigidBodyComponent::CreateActor()
{
	if (m_pActor || m_Colliders.size() == 0)
		return;


	auto physX = m_pPhysXManager->GetPhysX();
	auto pxScene = GetGameObject()->GetScene()->GetPhysXProxy()->GetScene();
	
	if (m_IsStatic)
		m_pActor = physX->createRigidStatic(PxTransform(XmToPxVec3(GetTransform()->GetPosition())));
	else
	{
		m_pActor = physX->createRigidDynamic(PxTransform(XmToPxVec3(GetTransform()->GetPosition())));
	
		GetPxRigidBody()->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, m_IsKinematic);
	}

	for(auto it = m_Colliders.begin(); it != m_Colliders.end(); ++it)
	{
		(*it)->SetShape(m_pActor->createShape(*(*it)->m_GeometryVec, (*it)->m_Material, (*it)->m_Pose));
	}
	
	m_pActor->userData = this;
	pxScene->addActor(*m_pActor);
}

void RigidBodyComponent::AddCollider(Collider* collider)
{
	if(!m_pPhysXManager)
	{
		m_CollidersToInitialise.push_back(collider);
		return;
	}


	m_Colliders.push_back(collider);
	if (!m_pActor)
		CreateActor();
	else
		collider->SetShape(m_pActor->createShape(*collider->m_GeometryVec, collider->m_Material, collider->m_Pose));

	collider->GetShape()->setSimulationFilterData(m_CollisionGroups);
	collider->GetShape()->setQueryFilterData(m_CollisionGroups);
}

void RigidBodyComponent::Translate(XMFLOAT3 pos)
{
	Transform(pos, GetRotation());
}

void RigidBodyComponent::Rotate(XMFLOAT4 rot)
{
	Transform(GetPosition(), rot);
}

void RigidBodyComponent::Transform(XMFLOAT3 pos, XMFLOAT4 rot)
{
	if (!m_pActor || m_IsStatic)
		return;

	if (m_IsKinematic)
		reinterpret_cast<PxRigidDynamic*>(m_pActor)->setKinematicTarget(PxTransform(XmToPxVec3(pos), XmToPxQuat(rot)));
	else
		m_pActor->setGlobalPose(PxTransform(XmToPxVec3(pos), XmToPxQuat(rot)));

}

XMFLOAT3 RigidBodyComponent::GetPosition()
{
	if(m_pActor)
	{
		PxTransform pose;
		if (!m_IsKinematic || !reinterpret_cast<PxRigidDynamic*>(m_pActor)->getKinematicTarget(pose))
			pose = m_pActor->getGlobalPose();

		return XMFLOAT3(pose.p.x, pose.p.y, pose.p.z);

	}


	return XMFLOAT3();
}

XMFLOAT4 RigidBodyComponent::GetRotation()
{
	if(m_pActor)
	{
		PxTransform pose;
		if (!m_IsKinematic || !reinterpret_cast<PxRigidDynamic*>(m_pActor)->getKinematicTarget(pose))
			pose = m_pActor->getGlobalPose();

		return XMFLOAT4(pose.q.x, pose.q.y, pose.q.z, pose.q.w);

	}

	return XMFLOAT4();
}
