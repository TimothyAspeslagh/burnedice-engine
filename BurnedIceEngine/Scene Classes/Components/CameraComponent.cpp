#include "stdafx.h"
#include "CameraComponent.h"
#include "TransformComponent.h"

CameraComponent::CameraComponent()
{
	m_FoVAngleY = .75f;
	m_NearZ = 1.0f;
	m_FarZ = 100.0f;
}

CameraComponent::~CameraComponent()
{
}

void CameraComponent::Initialise(const MainManager& gameManager)
{
	XMStoreFloat4x4(&m_ViewProjection, XMMatrixIdentity());
}

void CameraComponent::Tick(const MainManager& gameManager)
{
	//Create camera matrices here
	float width = gameManager.GetGameSettings()->WindowWidth;
	float height = gameManager.GetGameSettings()->WindowHeight;
	float aspectRatio = width / height;
	//position, rotation, etc of object is in transform component
	auto transform = GetTransform();
	auto projectionMat = XMMatrixPerspectiveFovLH(m_FoVAngleY, aspectRatio, m_NearZ, m_FarZ);

	
	XMFLOAT3 eyePos = transform->GetPosition();
	//float test = eyePos.y;
	m_EyePosVec = XMLoadFloat3(&eyePos);
	
	XMFLOAT3 up = transform->GetUp();
	XMVECTOR upVec = XMLoadFloat3(&up);

	XMFLOAT3 forward = transform->GetForward();
	XMVECTOR forwardVec = XMLoadFloat3(&forward);
	XMVECTOR targetPos = m_EyePosVec + forwardVec;

	auto viewMat = XMMatrixLookAtLH(m_EyePosVec, targetPos, upVec);
	auto viewInverse = XMMatrixInverse(nullptr, viewMat);

	auto viewProjMat = viewMat * projectionMat;

	XMStoreFloat4x4(&m_ViewProjection, viewProjMat);
	XMStoreFloat4x4(&m_ViewInverse, viewInverse);
	XMStoreFloat4x4(&m_Projection, projectionMat);
	XMStoreFloat4x4(&m_View, viewMat);
}


void CameraComponent::Clean()
{
}

void CameraComponent::Draw(const MainManager& gameManager)
{
}
