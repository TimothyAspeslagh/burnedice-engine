#pragma once
#include "BaseComponent.h"
class CubeMeshComponent :
	public BaseComponent
{
public:
	CubeMeshComponent();
	~CubeMeshComponent();
	void Initialise(const MainManager &gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager &gameManager) override;

private:
	ID3D11Buffer * m_pVertexBuffer;
	ID3D11Buffer * m_pIndexBuffer;
	ID3DX11Effect *m_pCubeEffect;
	ID3DX11EffectTechnique *m_pCubeTechnique;
	ID3D11InputLayout *m_pInputLayout;
	vector<iPrimitiveVertex> m_VertexList;
	vector<iIndex> m_IndexList;

	ID3DX11EffectMatrixVariable * m_pWorldViewProjMat;
};

