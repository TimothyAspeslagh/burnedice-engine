#pragma once

class TransformComponent;
class MainManager;
class GameObject;

class BaseComponent
{
public:
	BaseComponent();
	virtual ~BaseComponent(){};



	GameObject* GetGameObject();
	TransformComponent* GetTransform();
	void SetGameObject(GameObject * obj);


	virtual void Initialise(const MainManager &gameManager) = 0;
	virtual void Tick(const MainManager &gameManager) = 0;
	virtual void Clean() = 0;
	virtual void Draw(const MainManager &gameManager) = 0;

	void BaseInitialise(const MainManager &gameManager);
	void Update(const MainManager &gameManager);
	void BaseClean();
	void BaseDraw(const MainManager &gameManager);
	const bool IsInitialised() const { return m_IsInitialised; }

protected:
		bool m_IsInitialised = false;
		GameObject * m_pGameObject = nullptr;

};
