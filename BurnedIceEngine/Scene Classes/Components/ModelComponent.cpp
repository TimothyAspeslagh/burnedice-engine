#include "stdafx.h"
#include "ModelComponent.h"


ModelComponent::ModelComponent()
{
}


ModelComponent::~ModelComponent()
{
}

void ModelComponent::Initialise(const MainManager &gameManager)
{
}

void ModelComponent::Tick(const MainManager& gameManager)
{

}

void ModelComponent::Clean()
{
}

void ModelComponent::Draw(const MainManager &gameManager)
{
	if(m_ActiveMaterialIndex > m_MaterialsAdded.size())
	{
		wcout << L"Warning! no valid material active, skip drawing this mesh" << endl;
		return;
	}
	gameManager.Content->GetMaterial(m_MaterialsAdded[m_ActiveMaterialIndex])->RootUpdateShaderVariables(gameManager, GetTransform(), m_Textures);

	gameManager.Content->GetMaterial(m_MaterialsAdded[m_ActiveMaterialIndex])->Draw(gameManager, m_MeshID, GetTransform());
}

void ModelComponent::SetActiveMaterial(DesignString materialID)
{
	for (size_t i = 0; i != m_MaterialsAdded.size(); ++i)
	{
		if (m_MaterialsAdded[i].Compare(materialID))
		{
			m_ActiveMaterialIndex = i;
			return;
		}
	}
}