#pragma once
#include "BaseComponent.h"

class CameraComponent : public BaseComponent
{
public:
	CameraComponent();
	~CameraComponent();
	void Initialise(const MainManager& gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager& gameManager) override;

	const XMFLOAT4X4& GetViewProjection() const { return m_ViewProjection; }
	bool IsActive() { return m_IsActive; }
	void Activate() { m_IsActive = true; }
	void DeActivate() { m_IsActive = false; }

	XMFLOAT4X4 GetViewInverse() const { return m_ViewInverse; }
	XMFLOAT4X4 GetProjection() const { return m_Projection; }
	XMVECTOR GetEyePos() const { return m_EyePosVec; }
	float GetFOV() const { return m_FoVAngleY; }
	XMFLOAT4X4 GetView() const { return m_View; }

private:
	float m_FoVAngleY, m_NearZ, m_FarZ;
	XMFLOAT4X4 m_View;
	XMFLOAT4X4 m_ViewProjection;
	XMFLOAT4X4 m_ViewInverse;
	XMFLOAT4X4 m_Projection;
	XMVECTOR m_EyePosVec = {};
	bool m_IsActive = false;
};

