#include "stdafx.h"
#include "CubeMeshComponent.h"
#include "../GameObject.h"

CubeMeshComponent::CubeMeshComponent()
{
}


CubeMeshComponent::~CubeMeshComponent()
{
}

void CubeMeshComponent::Initialise(const MainManager &gameManager)
{
	//This test code was written to get the first primitives on the screen
	//and was not intended to be actively used in the engine
	//loading effects can be done with the manager now


	//Build effect
	//Compile shader

	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/CubeMeshEffect.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pCubeEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: CubeMeshEffect.fx" << endl;
	}

	compilationMsgs->Release();

	m_pCubeTechnique = m_pCubeEffect->GetTechniqueByName("DefaultTechnique");
	m_pWorldViewProjMat = m_pCubeEffect->GetVariableByName("gWorldViewProj")->AsMatrix();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",0, DXGI_FORMAT_R32G32B32_FLOAT, 0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL" ,0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA,0 },
	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pCubeTechnique->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: CubeMeshComponent" << endl;
	}
	//Create Cube buffer

	//VERTEX BUFFER
	
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(-1, -1, -1), XMFLOAT4(1,0,0,1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(-1, 1, -1), XMFLOAT4(0, 1, 0, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(1, 1, -1), XMFLOAT4(0, 0, 1, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(1, -1, -1), XMFLOAT4(1, 1, 0, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(1, -1, 1), XMFLOAT4(1, 0, 1, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(1, 1, 1), XMFLOAT4(0, 1, 1, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(-1, 1, 1), XMFLOAT4(1, 1, 1, 1)));
	m_VertexList.push_back(iPrimitiveVertex(XMFLOAT3(-1, -1, 1), XMFLOAT4(0, 0, 0, 1)));

	//UINT vByteSize = vBuffer.size();



	//UINT iByteSize = iBuffer.size();

	//VERTEX BUFFER DESC
	D3D11_BUFFER_DESC vbd;
	vbd.ByteWidth = sizeof(iPrimitiveVertex) * m_VertexList.size();
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	//vbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vsrd = {0};
	vsrd.pSysMem = m_VertexList.data();

	result = gameManager.m_pDevice->CreateBuffer(&vbd, &vsrd, &m_pVertexBuffer);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Primitive cubemesh vertex buffer" << endl;
	}

	//INDEX BUFFER
	m_IndexList.push_back(iIndex(0, 1, 2));
	m_IndexList.push_back(iIndex(0, 2, 3));
	m_IndexList.push_back(iIndex(4, 5, 6));
	m_IndexList.push_back(iIndex(4, 6, 7));
	m_IndexList.push_back(iIndex(0, 3, 7));
	m_IndexList.push_back(iIndex(2, 4, 3));
	m_IndexList.push_back(iIndex(0, 7, 1));
	m_IndexList.push_back(iIndex(7, 6, 1));
	m_IndexList.push_back(iIndex(1, 6, 5));
	m_IndexList.push_back(iIndex(1, 5, 2));
	m_IndexList.push_back(iIndex(2, 5, 4));
	m_IndexList.push_back(iIndex(4, 7, 3));

	//INDEX BUFFER DESC
	D3D11_BUFFER_DESC ibd;
	ibd.ByteWidth = sizeof(iIndex) * m_IndexList.size();
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	//ibd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA isrd = {0};
	isrd.pSysMem = m_IndexList.data();

	result = gameManager.m_pDevice->CreateBuffer(&ibd, &isrd, &m_pIndexBuffer);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create Primitive cubemesh Index buffer" << endl;
	}

}

void CubeMeshComponent::Tick(const MainManager& gameManager)
{
	//Set matrix value
	auto world = GetTransform()->GetWorldMatrix();
	auto worldMat = XMLoadFloat4x4(&world);
	auto viewProj = gameManager.GetActiveCamera()->GetViewProjection();
	auto viewProjMat = XMLoadFloat4x4(&viewProj);
	auto wvp = worldMat * viewProjMat;

	m_pWorldViewProjMat->SetMatrix(reinterpret_cast<float*>(&wvp));
}

void CubeMeshComponent::Clean()
{
	m_pVertexBuffer->Release();
	m_pIndexBuffer->Release();
	m_pCubeEffect->Release();
	m_pInputLayout->Release();
}

void CubeMeshComponent::Draw(const MainManager &gameManager)
{
	size_t stride = sizeof(iPrimitiveVertex);
	size_t offset = 0;
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	gameManager.m_pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3DX11_TECHNIQUE_DESC tD;
	m_pCubeTechnique->GetDesc(&tD);
	for(size_t i = 0; i < tD.Passes; ++i)
	{
		m_pCubeTechnique->GetPassByIndex(i)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->DrawIndexed(m_IndexList.size() * 3,0, 0);
	}
}
