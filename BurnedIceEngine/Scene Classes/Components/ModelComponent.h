#pragma once
#include "BaseComponent.h"


class ModelComponent :
	public BaseComponent
{
public:
	ModelComponent();
	~ModelComponent();
	void Initialise(const MainManager &gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Clean() override;
	void Draw(const MainManager &gameManager) override;

	void AddMaterial(DesignString materialID) { m_MaterialsAdded.push_back(materialID); }
	void SetActiveMaterial(DesignString materialID);
	void SetMesh(DesignString meshID) { m_MeshID = meshID; }
	void AddTexture(DesignString textureID, TextureUsage usage) { m_Textures.push_back(pair<DesignString, TextureUsage>(textureID, usage)); }

private:
	vector<DesignString> m_MaterialsAdded;
	size_t m_ActiveMaterialIndex = 0;
	DesignString m_MeshID;
	vector<pair<DesignString, TextureUsage>> m_Textures;
};

