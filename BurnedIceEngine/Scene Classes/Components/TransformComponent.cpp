#include "stdafx.h"
#include "TransformComponent.h"
#include "../../Scene Classes/GameObject.h"
#include "RigidBodyComponent.h"

//#include "../../Helpers/IceMath.h"

TransformComponent::TransformComponent()
{
	m_Scale = XMFLOAT3(1, 1, 1);
	m_Position = XMFLOAT3(0, 0, 0);
	XMStoreFloat4(&m_Rotation,XMQuaternionIdentity());
}


TransformComponent::~TransformComponent()
{
}

void TransformComponent::Initialise(const MainManager &gameManager)
{

}

void TransformComponent::Tick(const MainManager &gameManager)
{
	//Update rigidbody
	auto rigid = GetGameObject()->GetComponent<RigidBodyComponent>();
	if(rigid)
	{
		if (m_Translate)
		{
			m_Translate = false;
			rigid->Translate(m_Position);
		}
		else
			m_Position = rigid->GetPosition();

		if (m_Rotate)
		{
			m_Rotate = false;
			rigid->Rotate(m_Rotation);
		}
		else
			m_Rotation = rigid->GetRotation();
	}

	//Calculate world matrix
	auto correctionRot = XMFLOAT4(0, 0, 0, 0);
	if (m_HasAxisCorrection)
	{
		correctionRot = EulerToQuaternion(ToRadians(-90), 0, 0);
	}
	XMVECTOR correctionRotVec = XMLoadFloat4(&correctionRot);
	auto correctionRotMat = XMMatrixRotationQuaternion(correctionRotVec);

	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	auto rotMat = XMMatrixRotationQuaternion(rot); //reused later
	XMMATRIX world = XMMatrixScaling(m_Scale.x, m_Scale.y, m_Scale.z) *
		correctionRotMat *rotMat *
		XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);

	XMMATRIX uncorrectedWorld = XMMatrixScaling(m_Scale.x, m_Scale.y, m_Scale.z) *
		rotMat * 
		XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);


	auto parentObj = m_pGameObject->GetParent();
	if(parentObj != nullptr)
	{
		auto parentWorld = parentObj->GetTransform()->GetUncorrectedWorldMatrix();
		XMMATRIX parentWorldMat = XMLoadFloat4x4(&parentWorld);
		world *= parentWorldMat;
		uncorrectedWorld *= parentWorldMat;
	}

	XMStoreFloat4x4(&m_UncorrectedWorld, uncorrectedWorld);
	XMStoreFloat4x4(&m_World, world);

	auto worldUp = XMFLOAT3(0, 1, 0);
	auto worldForward = XMFLOAT3(0, 0, 1);
	XMVECTOR worldUpVec = XMLoadFloat3(&worldUp);
	XMVECTOR worldForwardVec = XMLoadFloat3(&worldForward);
	XMVECTOR upVec = XMVector3TransformCoord(worldUpVec, rotMat);
	XMVECTOR forwardVec = XMVector3TransformCoord(worldForwardVec, rotMat);
	XMVECTOR rightVec = XMVector3Cross(upVec, forwardVec);


	XMStoreFloat3(&m_Up, upVec);
	XMStoreFloat3(&m_Forward, forwardVec);
	XMStoreFloat3(&m_Right, rightVec);
}

void TransformComponent::Draw(const MainManager &gameManager)
{

}

void TransformComponent::Clean()
{

}

void TransformComponent::Translate(float x, float y, float z)
{
	m_Position.x = x;
	m_Position.y = y;
	m_Position.z = z;
	m_Translate = true;
}

void TransformComponent::Translate(XMFLOAT3 translation)
{
	Translate(translation.x, translation.y, translation.z);
}

void TransformComponent::Rotate(float x, float y, float z, float w)
{
	Rotate(XMFLOAT4(x, y, z, w));
}

void TransformComponent::Rotate(XMFLOAT4 rotation)
{
	m_Rotation = rotation;
	m_Rotate = true;
}

void TransformComponent::RotateEuler(float x, float y, float z)
{
	XMFLOAT4 quat = EulerToQuaternion(x, y, z);
	Rotate(quat);
}

void TransformComponent::RotateEuler(XMFLOAT3 EulerRotation)
{
	RotateEuler(EulerRotation.x, EulerRotation.y, EulerRotation.z);
}

void TransformComponent::Scale(float x, float y, float z)
{
	m_Scale = XMFLOAT3(x, y, z);
}

void TransformComponent::Scale(XMFLOAT3 scale)
{
	Scale(scale.x, scale.y, scale.z);
}

XMFLOAT3 TransformComponent::GetEulerRotation() const
{
	return QuaternionToEuler(m_Rotation.x, m_Rotation.y, m_Rotation.z, m_Rotation.w);
}
