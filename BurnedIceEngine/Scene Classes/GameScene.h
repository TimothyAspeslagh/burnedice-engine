#pragma once
#include "Components/CameraComponent.h"


class MainManager;
struct DesignString;
class GameObject;
class PhysXProxy;
class FleXProxy;

class GameScene : public PxSimulationEventCallback
{
public:
	GameScene();
	virtual ~GameScene();

	//These functions serve to call all correct Initialise, Tick, and Draw functions of all children without cluttering the User Workspace
	void BaseInitialise(const MainManager &gameManager);
	void Update(const MainManager &gameManager);
	void BaseDraw(const MainManager &gameManager);
	void BaseClean();

	virtual bool Initialise(const MainManager &gameManager) = 0;
	virtual void Tick(const MainManager &gameManager) = 0;
	virtual void Draw(const MainManager &gameManager) = 0;

	virtual void Clean() =0;

	
	bool AddObjectAtInitialise(GameObject * object);

	bool AddRuntimeObject(GameObject * object, const MainManager& gameManager);

	GameObject * GetObjectWithTag(const DesignString &tag);
	vector<GameObject*> GetObjectsWithTag(const DesignString &tag);
	template<class T> T* GetObjectOfType();
	template<class T> vector<T*> GetObjectsOfType();
	template<class T> T* GetObjectOfTypeWithTag(const DesignString &tag);
	template<class T> vector<T*> GetObjectsOfTypeWithTag(const DesignString &tag);

	void SetActiveCamera(CameraComponent * camera);
	CameraComponent* GetActiveCamera() const { return m_pActiveCamera; }
	PhysXProxy * GetPhysXProxy()  const {return m_pPhysXProxy;}
	FleXProxy * GetFleXProxy() const { return m_pFleXProxy; }

	void onConstraintBreak(PxConstraintInfo* constraints, PxU32 count) override;
	void onWake(PxActor** actors, PxU32 count) override;
	void onSleep(PxActor** actors, PxU32 count) override;
	void onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs) override;
	void onTrigger(PxTriggerPair* pairs, PxU32 count) override;
private:
	bool m_IsInitialised = false;
	vector<GameObject*> m_pChildren;
	CameraComponent * m_pActiveCamera = nullptr;
	PhysXProxy * m_pPhysXProxy = nullptr;
	FleXProxy * m_pFleXProxy = nullptr;
};

template <class T>
T* GameScene::GetObjectOfType()
{
	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		auto castResult = dynamic_cast<T*>(m_pChildren[i]);
		if (castResult != nullptr)
			return castResult;
	}
	return nullptr;
}

template <class T>
vector<T*> GameScene::GetObjectsOfType()
{
	auto vec = vector<T*>();

	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		auto castResult = dynamic_cast<T*>(m_pChildren[i]);
		if (castResult != nullptr)
			vec.push_back(castResult);
	}
	return vec;
}

template <class T>
T* GameScene::GetObjectOfTypeWithTag(const DesignString& tag)
{
	//casting is expensive, dequalify on tag first

	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		if(m_pChildren[i]->CompareTag(tag))
		{
			auto castResult = dynamic_cast<T*>(m_pChildren[i]);
			if (castResult != nullptr)
				return castResult;
		}
	}
	return nullptr;
}

template <class T>
vector<T*> GameScene::GetObjectsOfTypeWithTag(const DesignString& tag)
{
	auto vec = vector<T*>();
	//casting is expensive, dequalify on tag first

	for (size_t i = 0; i != m_pChildren.size(); ++i)
	{
		if (m_pChildren[i]->CompareTag(tag))
		{
			auto castResult = dynamic_cast<T*>(m_pChildren[i]);
			if (castResult != nullptr)
				vec.push_back(castResult);
		}
	}
	return vec;
}
