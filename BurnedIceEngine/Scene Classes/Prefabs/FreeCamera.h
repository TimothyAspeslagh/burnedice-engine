﻿#pragma once
#include "../GameObject.h"

class FreeCamera: public GameObject
{
public:
	FreeCamera();
	virtual ~FreeCamera() {}
	void Initialise(const MainManager& gameManager) override;
	void Tick(const MainManager& gameManager) override;
	void Draw(const MainManager& gameManager) override;
	void Clean() override;

private:
	XMFLOAT2 m_LastMousePosition;
	float m_Pitch; 
	float m_Yaw;
};
