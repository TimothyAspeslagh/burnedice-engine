﻿#include "stdafx.h"
#include "FreeCamera.h"

FreeCamera::FreeCamera()
{

}

void FreeCamera::Initialise(const MainManager& gameManager)
{
	m_LastMousePosition = XMFLOAT2();
	m_Pitch = GetTransform()->GetEulerRotation().x;
	m_Yaw = GetTransform()->GetEulerRotation().z;
}

void FreeCamera::Tick(const MainManager& gameManager)
{
	auto cameraCmp = GetComponent<CameraComponent>();
	if (cameraCmp == nullptr)
	{
		auto newCameraCmp = new CameraComponent();
		AddComponent(newCameraCmp);
		newCameraCmp->Initialise(gameManager);
	}

	//camera logic
	auto transform = GetTransform();
	XMFLOAT3 currentPos = transform->GetPosition();
	XMVECTOR currentPosVec = XMLoadFloat3(&currentPos);

	float speed = 10.0f;

	bool positionUpdated = false;

	//Transform
	if (gameManager.Input->IsKeyPressed(VK_SHIFT))
	{
		speed *= 4;
	}

	if(gameManager.Input->IsKeyPressed('w'))
	{
		positionUpdated = true;
		currentPosVec += XMLoadFloat3(&transform->GetForward()) * speed * gameManager.GetDeltaTime();
	}

	if(gameManager.Input->IsKeyPressed('a'))
	{
		positionUpdated = true;
		currentPosVec -= XMLoadFloat3(&transform->GetRight()) * speed * gameManager.GetDeltaTime();
	}

	if(gameManager.Input->IsKeyPressed('s'))
	{
		positionUpdated = true;
		currentPosVec -= XMLoadFloat3(&transform->GetForward()) * speed * gameManager.GetDeltaTime();
	}

	if(gameManager.Input->IsKeyPressed('d'))
	{
		positionUpdated = true;
		currentPosVec += XMLoadFloat3(&transform->GetRight()) * speed * gameManager.GetDeltaTime();
	}

	if(gameManager.Input->IsKeyPressed('q'))
	{
		positionUpdated = true;
		currentPosVec -= XMLoadFloat3(&transform->GetUp()) * speed * gameManager.GetDeltaTime();
	}

	if(gameManager.Input->IsKeyPressed('e'))
	{
		positionUpdated = true;
		currentPosVec += XMLoadFloat3(&transform->GetUp()) * speed * gameManager.GetDeltaTime();
	}

	//Rotation
	if(gameManager.Input->IsKeyDown(VK_RBUTTON))
	{
		m_LastMousePosition = gameManager.Input->GetWindowsMousePosition();
	}
	else if(gameManager.Input->IsKeyPressed(VK_RBUTTON))
	{
		float speed = 5.0f;

		//calculate how the mouse moved
		XMFLOAT2 newMousePosition = gameManager.Input->GetWindowsMousePosition();
		XMVECTOR mousePosDiffVec = XMLoadFloat2(&m_LastMousePosition) - XMLoadFloat2(&newMousePosition);
		XMFLOAT2 mousePosDiff;
		XMStoreFloat2(&mousePosDiff, mousePosDiffVec);


		//XMFLOAT3 mouseRot = XMFLOAT3(static_cast<float>(-mousePosDiff.x), 0.0f, static_cast<float>(-mousePosDiff.y));

		//Store the mouse position for our next frame
		m_LastMousePosition = newMousePosition;
	
		//Calculate rotation
		m_Pitch -= mousePosDiff.y * speed * gameManager.GetDeltaTime();
		m_Yaw -= mousePosDiff.x * speed * gameManager.GetDeltaTime();
			
		//Apply rotation
		float decrease = 3.0f;
		transform->RotateEuler(m_Pitch / decrease, m_Yaw / decrease, 0);
	}

	XMFLOAT3 newPos = XMFLOAT3();
	XMStoreFloat3(&newPos, currentPosVec);

	if(positionUpdated)
		transform->Translate(newPos);
}

void FreeCamera::Draw(const MainManager& gameManager)
{
}

void FreeCamera::Clean()
{
}
