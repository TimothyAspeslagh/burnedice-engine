#include "stdafx.h"
#include "GameScene.h"
#include "GameObject.h"
#include "../Managers/MainManager.h"
#include "../PhysX/PhysXProxy.h"
#include "../PhysX/FleXProxy.h"


GameScene::GameScene()
{
}


GameScene::~GameScene()
{
}

void GameScene::BaseInitialise(const MainManager & gameManager)
{
	m_pPhysXProxy = new PhysXProxy();
	m_pPhysXProxy->SetParent(this);
	m_pPhysXProxy->Initialise(gameManager);

	if(gameManager.GetFleXEnabled())
	{
		m_pFleXProxy = new FleXProxy();
		m_pFleXProxy->SetParent(this);
		m_pFleXProxy->Initialise(gameManager);
	}

	//Initialise the scene
	Initialise(gameManager);

	for (size_t i = 0; i != m_pChildren.size(); ++i)
	{
		//Call all correct initialise functions of children
		m_pChildren[i]->BaseInitialise(gameManager, this);
	}

	m_IsInitialised = true;
}

void GameScene::BaseDraw(const MainManager &gameManager)
{
	//Draw the scene
	Draw(gameManager);

	//Draw the children in the scene
	for (size_t i = 0; i != m_pChildren.size(); ++i)
	{
		m_pChildren[i]->BaseDraw(gameManager);
	}
	m_pPhysXProxy->Draw(gameManager);
}


void GameScene::Update(const MainManager &gameManager)
{
	if (!m_IsInitialised)
		return;

	Tick(gameManager);

	for (size_t i = 0; i != m_pChildren.size(); ++i)
	{
		m_pChildren[i]->Update(gameManager);
	}
	m_pPhysXProxy->Tick(gameManager);

	if(gameManager.GetFleXEnabled())
		m_pFleXProxy->Tick(gameManager);
}

void GameScene::BaseClean()
{
	//Clean children
	for (size_t i = 0; i != m_pChildren.size(); ++i)
	{
		m_pChildren[i]->BaseClean();


		delete m_pChildren[i];
		m_pChildren[i] = nullptr;
	}

	if(m_pFleXProxy != nullptr)
	{
		m_pFleXProxy->Clean();
		delete m_pFleXProxy;
		m_pFleXProxy = nullptr;
	}

	m_pPhysXProxy->Clean();
	delete m_pPhysXProxy;
	m_pPhysXProxy = nullptr;
	
	//Clean object itself
	Clean();
}

bool GameScene::AddObjectAtInitialise(GameObject* object)
{
	m_pChildren.push_back(object);
	return true;
}

bool GameScene::AddRuntimeObject(GameObject* object, const MainManager& gameManager)
{
	if (!object->IsInitialised())
		object->BaseInitialise(gameManager, this);

	m_pChildren.push_back(object);
	return true;
}

GameObject* GameScene::GetObjectWithTag(const DesignString &tag)
{
	//no clue why iterators don't work here
	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		if (m_pChildren[i]->CompareTag(tag))
			return m_pChildren[i];
	}
	return nullptr;
}

vector<GameObject*> GameScene::GetObjectsWithTag(const DesignString &tag)
{
	vector<GameObject*> localVec;
	
	for(size_t i = 0; i!= m_pChildren.size(); ++i)
	{
		if (m_pChildren[i]->CompareTag(tag))
			localVec.push_back(m_pChildren[i]);
	}

	return localVec;
}

void GameScene::SetActiveCamera(CameraComponent* camera)
{
	if(m_pActiveCamera != nullptr)
		m_pActiveCamera->DeActivate(); 
	
	m_pActiveCamera = camera; 
	m_pActiveCamera->Activate();
}

void GameScene::onConstraintBreak(PxConstraintInfo* constraints, PxU32 count)
{
}

void GameScene::onWake(PxActor** actors, PxU32 count)
{
}

void GameScene::onSleep(PxActor** actors, PxU32 count)
{
}

void GameScene::onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs)
{
}

void GameScene::onTrigger(PxTriggerPair* pairs, PxU32 count)
{
}
