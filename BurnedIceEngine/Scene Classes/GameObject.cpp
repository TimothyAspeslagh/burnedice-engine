#include "stdafx.h"
#include "GameObject.h"
#include "../Managers/MainManager.h"
#include "Components/ModelComponent.h"

GameObject::GameObject()
{
	m_Tag = "Default";
}

GameObject::GameObject(DesignString tag)
{
	m_Tag = tag;
}

GameObject::~GameObject()
{
}

void GameObject::BaseInitialise(const MainManager &gameManager, GameScene* parent)
{
	//Designate scene pointer
	m_pScene = parent;
	//Initialise the Object
	Initialise(gameManager);

	//Call All initialise methods in children and components

	//Guarantee there is a transform component
	if (m_pTransformComponent == nullptr)
		AddComponent(new TransformComponent());

	m_pTransformComponent->BaseInitialise(gameManager);

	if (m_pCameraComponent != nullptr)
		m_pCameraComponent->BaseInitialise(gameManager);

	for (size_t i = 0; i != m_pComponents.size(); ++i)
		m_pComponents[i]->BaseInitialise(gameManager);

	for (size_t i = 0; i != m_pChildren.size(); ++i)
		m_pChildren[i]->BaseInitialise(gameManager, parent);

	m_IsInitialised = true;
}


void GameObject::Update(const MainManager &gameManager)
{
	if (!m_IsInitialised)
		return;

	//Update object itself
	Tick(gameManager);

	if(m_pTransformComponent != nullptr)
	{
		//if a mesh has been loaded in with assimp, the model will be rotated due to axis conventions
		//here I notify the transform component to counter this rotation
		if (m_AxisCorrectionStateChanged)
		{
			m_pTransformComponent->SetAxisCorrection(m_HasModelComponent);
			m_AxisCorrectionStateChanged = false; //only do it once
		}
		m_pTransformComponent->Update(gameManager);

	}

	if(m_pCameraComponent != nullptr)
		m_pCameraComponent->Update(gameManager);

	//Update children
	for (size_t i = 0; i != m_pComponents.size(); ++i)
		m_pComponents[i]->Update(gameManager);

	for (size_t i = 0; i != m_pChildren.size(); ++i)
		m_pChildren[i]->Update(gameManager);

}

void GameObject::BaseClean()
{
	//Clean children and components
	for (size_t i = 0; i != m_pComponents.size(); ++i)
		m_pComponents[i]->BaseClean();

	for (size_t i = 0; i != m_pChildren.size(); ++i)
		m_pChildren[i]->BaseClean();

	for (size_t i = 0; i != m_pComponents.size(); ++i)
	{
		delete m_pComponents[i];
		m_pComponents[i] = nullptr;
	}

	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		delete m_pChildren[i];
		m_pChildren[i] = nullptr;
	}

	if (m_pCameraComponent != nullptr)
	{
		m_pCameraComponent->Clean();
		delete m_pCameraComponent;
		m_pCameraComponent = nullptr;
	}

	if(m_pTransformComponent != nullptr)
	{
		m_pTransformComponent->Clean();
		delete m_pTransformComponent;
		m_pTransformComponent = nullptr;
	}

	//Call user clean method
	Clean();
	m_pScene = nullptr;
	m_IsInitialised = false;
}

void GameObject::BaseDraw(const MainManager &gameManager)
{
	//Draw the object itself
	Draw(gameManager);

	//Draw the children
	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		m_pChildren[i]->BaseDraw(gameManager);
	}

	for(size_t i = 0; i != m_pComponents.size(); ++i)
	{
		m_pComponents[i]->BaseDraw(gameManager);
	}
}

bool GameObject::CompareTag(DesignString tag)
{
	return (tag.Compare(m_Tag));
}

bool GameObject::AddComponent(BaseComponent* cmp)
{
	//Cannot add a component that has been added to another object
	if (cmp->GetGameObject() != nullptr)
		return false;

	cmp->SetGameObject(this);
	//Check if component is special (TransformComponent, CameraComponent, allows for quick fetching)
	auto TransformCmp = dynamic_cast<TransformComponent*>(cmp);
	if(TransformCmp != nullptr)
	{
		//Can only add one transformComponent
		if(m_pTransformComponent != nullptr)
		{
			cmp->SetGameObject(nullptr);
			return false;
		}
		m_pTransformComponent = TransformCmp;
		return true;
	}

	auto cameraCmp = dynamic_cast<CameraComponent*>(cmp);
	if(cameraCmp != nullptr)
	{
		if(m_pCameraComponent != nullptr)
		{
			cmp->SetGameObject(nullptr);
			return false;
		}
		m_pCameraComponent = cameraCmp;
		return true;
	}

	if (dynamic_cast<ModelComponent*>(cmp) != nullptr)
	{
		m_AxisCorrectionStateChanged = true;
		m_HasModelComponent = true;

	}

	m_pComponents.push_back(cmp);
	
	return true;
}

bool GameObject::RemoveComponent(BaseComponent* cmp)
{
	//there are no non-initialised components possibly attached
	
	//TODO: test if removing a non-existing component does anything

	for(auto it = m_pComponents.begin(); it != m_pComponents.end(); ++it)
	{
		if (*it == cmp)
		{
			m_pComponents.erase(it);
			return true;
		}
	}
	if (dynamic_cast<ModelComponent*>(cmp) != nullptr)
	{
		m_AxisCorrectionStateChanged = true;
		m_HasModelComponent = false;

	}

	cmp->SetGameObject(nullptr);
	return false;
}

bool GameObject::DeleteComponent(BaseComponent* cmp)
{
	bool result = RemoveComponent(cmp);
	delete cmp;
	return result;
}

bool GameObject::AddChild(GameObject* child)
{
	//cannot add itself to the list
	if (child == this)
		return false;

	if (child->GetParent() != nullptr)
		return false;

	for(auto it = m_pChildren.begin(); it != m_pChildren.end(); ++it)
	{
		if (*it == child)
			return false;
	}

	m_pChildren.push_back(child);
	child->m_pParent = this;
	return true;
}

bool GameObject::RemoveChild(GameObject* child)
{
	if (child == this)
		return false;

	for(auto it = m_pChildren.begin(); it != m_pChildren.end(); ++it)
	{
		if (*it == child)
		{
			m_pChildren.erase(it);
			return true;
		}
	}
	return false;
}

bool GameObject::DeleteChild(GameObject* child)
{
	bool result = RemoveChild(child);
	if (result)
		delete child;

	return result;
}


GameObject* GameObject::GetChildByTag(DesignString tag)
{
	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		if (m_pChildren[i]->CompareTag(tag))
			return m_pChildren[i];
	}

	return nullptr;
}

const vector<GameObject*> GameObject::GetChildrenByTag(DesignString tag)
{
	vector<GameObject*> localVec;

	for(size_t i = 0; i != m_pChildren.size(); ++i)
	{
		if (m_pChildren[i]->CompareTag(tag))
			localVec.push_back(m_pChildren[i]);
	}

	return localVec;
}

