#pragma once
#include "../Helpers/IceString.h"
#include <vector>
#include "Components/BaseComponent.h"
#include "Components/TransformComponent.h"
#include "Components/CameraComponent.h"

class MainManager;

class GameObject
{
public:
	GameObject();
	GameObject(DesignString tag);
	virtual ~GameObject();




	void BaseInitialise(const MainManager &gameManager, GameScene* parent);
	void Update(const MainManager &gameManager);
	void BaseDraw(const MainManager &gameManager);
	void BaseClean();

	//virtual void Initialise(const MainManager &gameManager) = 0;
	//virtual void Tick(const MainManager &gameManager) = 0;
	//virtual void Draw(const MainManager &gameManager) = 0;
	//virtual void Clean() = 0;

	virtual void Initialise(const MainManager &gameManager) {};
	virtual void Tick(const MainManager &gameManager) {};
	virtual void Draw(const MainManager &gameManager) {};
	virtual void Clean() {};



	GameObject* GetParent() const
	{
		return m_pParent;
	}

	GameScene* GetScene() const
	{
		return m_pScene;
	}
	
	void SetScene(GameScene * pScene) { m_pScene = pScene; }

	const DesignString & GetTag() const
	{
		return m_Tag;
	}

	void SetTag(DesignString tag)
	{
		m_Tag = tag;
	}

	bool CompareTag(DesignString tag);

	bool AddComponent(BaseComponent * cmp);
	bool RemoveComponent(BaseComponent * cmp);
	bool DeleteComponent(BaseComponent * cmp);
	template<class T> T* GetComponent();

	bool AddChild(GameObject * child);
	bool RemoveChild(GameObject * child);
	bool DeleteChild(GameObject * child);
	const vector<GameObject*>& GetChildren() { return m_pChildren; }
	GameObject* GetChildByTag(DesignString tag);
	const vector<GameObject*> GetChildrenByTag(DesignString tag);
	const bool IsInitialised() const { return m_IsInitialised; }
	TransformComponent* GetTransform() const { return m_pTransformComponent; }
	CameraComponent* GetCameraComponent() const { return m_pCameraComponent; }
	bool HasModelComponent() const { return m_HasModelComponent; }

private:
	bool m_IsInitialised = false;
	bool m_HasModelComponent = false;
	bool m_AxisCorrectionStateChanged = false;

	vector<GameObject*> m_pChildren;
	vector<BaseComponent*> m_pComponents;
	TransformComponent* m_pTransformComponent = nullptr;
	CameraComponent* m_pCameraComponent = nullptr;
	GameObject* m_pParent = nullptr;
	DesignString m_Tag;
	GameScene* m_pScene = nullptr;
};


template <class T>
T* GameObject::GetComponent()
{
	for (size_t i = 0; i != m_pComponents.size(); ++i)
	{
		auto castResult = dynamic_cast<T*>(m_pComponents[i]);
		if (castResult != nullptr)
			return castResult;
	}

	//Check on special components
	{
		auto castResult = dynamic_cast<T*>(m_pTransformComponent);
		if (castResult != nullptr)
			return castResult;
	}
	//I'm too lazy to rename castResult but not lazy enough to type this. Go Figure
	{
		auto castResult = dynamic_cast<T*>(m_pCameraComponent);
		if (castResult != nullptr)
			return castResult;

	}

	return nullptr;
}
