#pragma once
using namespace DirectX;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////										Burned Ice Engine												   ////
////								  Written by Timothy Aspeslagh											   ////
////						      Intended for educational purposes only									   ////
////									Do not copy or distribute											   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																										   ////
////											IceGraphics.h												   ////
////			   		This file contains all Vertex definitions for DirectX			   					   ////
////																										   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Used for meshes
struct iVertex
{
	XMFLOAT3 Pos;
	XMFLOAT3 Normal;
	XMFLOAT2 Tex;
};

//used for fluids
struct iFluidVertex
{
	iFluidVertex(float x, float y, float z) : Pos(x,y,z)
	{

	}
	iFluidVertex(): Pos(0,0,0)
	{
		
	}
	XMFLOAT3 Pos;
};

struct iPPVertex
{
public:

	iPPVertex() {};
	iPPVertex(XMFLOAT3 pos, XMFLOAT2 uv) :
		Position(pos), UV(uv) {}

	XMFLOAT3 Position;
	XMFLOAT2 UV;
};

//Use this for testing only
struct iPrimitiveVertex
{
	//Initialise members
	iPrimitiveVertex(XMFLOAT3 pos, XMFLOAT4 color, XMFLOAT3 normal) :Pos(pos), Color(color), Normal(normal)
	{

	}
	iPrimitiveVertex(XMFLOAT3 pos): Pos(pos)
	{
		
	}
	iPrimitiveVertex(XMFLOAT3 pos, XMFLOAT4 color) : Pos(pos) ,Color(color)
	{
		
	}
	//Default constructor, do nothing
	iPrimitiveVertex()
	{
		
	}
	XMFLOAT3 Pos = XMFLOAT3(0,0,0); 
	XMFLOAT4 Color = XMFLOAT4(1,0,0,1);
	XMFLOAT3 Normal = XMFLOAT3(0,0,0);
};

struct iIndex
{
	iIndex()
	{
		
	}
	iIndex(int i, int j, int k): i(i),j(j),k(k)
	{
		
	}
	int i = 0;
	int j = 0;
	int k = 0;
};