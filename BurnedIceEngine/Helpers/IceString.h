#pragma once
#include <string>
#include <iostream>
#include <ostream>
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////										Burned Ice Engine												   ////
////								  Written by Timothy Aspeslagh											   ////
////						      Intended for educational purposes only									   ////
////									Do not copy or distribute											   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																										   ////
////											IceString.h													   ////
////				 Game engine relies on hashing to compare strings, for user friendliness,				   ////	
////							these strings wrap around std::string										   ////
////																										   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// use this class when dealing with strings that are not to be represented to the player.
// for example, when adding a tag to a game object
struct DesignString
{
public:
	DesignString(const std::string& text)
		: m_Text(text)
	{
		m_Hash = HashText(text);
	}
	DesignString()
	{
		m_Text = "";
		m_Hash = HashText(m_Text);
	}

	DesignString(const DesignString& text): m_Text(text.m_Text), m_Hash(text.m_Hash)
	{
		//m_Text = text.GetText();
		//m_Hash = text.GetHash();
	}

	~DesignString() { };

	const std::string& GetText() const { return m_Text; }
	const size_t GetHash() const { return m_Hash; }

	bool Compare(const DesignString & otherString) const
	{
		return (m_Hash == otherString.GetHash());
	}

	//please prefer using a pre-hashed DesignString
	bool Compare(const std::string & otherString) const
	{
		return(m_Hash == HashText(otherString));
	}

	DesignString& operator=(const std::string & text)
	{
		auto str = DesignString(text);
		return *this;
	}

	DesignString& operator=(const DesignString & other)
	{
		if(this != &other)
		{
			m_Text = other.GetText();
			auto str = DesignString(m_Text);
			m_Hash = str.GetHash();

		}
		return *this;
	}

	bool operator==(const DesignString & other)
	{
		return this->Compare(other);
	}

	bool operator!=(const DesignString & other)
	{
		return !this->Compare(other);
	}



private:

	//Change hashing function here
	size_t HashText(const std::string& text) const
	{
		return std::hash<std::string>{}(text);
	}


	
	typedef std::basic_string<char> string_type;

	string_type m_Text;
	size_t m_Hash;
};

static std::wostream & operator<<(std::wostream & stream, DesignString str)
{
	for(char c : str.GetText())
	{
		stream << c;
	}
	return stream;
}

//This piece of code allows the use of DesignString as a key in a map container
namespace std
{
	template<> struct less<DesignString>
	{
		bool operator() (const DesignString& lhs, const DesignString& rhs) const
		{
			return lhs.GetHash() < rhs.GetHash();
		}
	};
}