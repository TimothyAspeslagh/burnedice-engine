#pragma once


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////										Burned Ice Engine												   ////
////								  Written by Timothy Aspeslagh											   ////
////						      Intended for educational purposes only									   ////
////									Do not copy or distribute											   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																										   ////
////											IceMath.h													   ////
////  This file contains all helper functions and classes to convert between DirectX Math and the game Math    ////
////																										   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// X is pitch, Y is yaw, Z is roll

static XMFLOAT4 EulerToQuaternion(float pitch, float yaw, float roll)
{
	XMFLOAT4 result;
	float t0 = cos(roll * 0.5f);
	float t1 = sin(roll * 0.5f);
	float t2 = cos(pitch * 0.5f);
	float t3 = sin(pitch * 0.5f);
	float t4 = cos(yaw * 0.5f);
	float t5 = sin(yaw * 0.5f);

	result.x = t0 * t3 * t4 - t1 * t2 * t5;
	result.y = t0 * t2 * t5 + t1 * t3 * t4;
	result.z = t1 * t2 * t4 - t0 * t3 * t5;
	result.w = t0 * t2 * t4 + t1 * t3 * t5;

	return result;


	//float c1 = cos(yaw * 0.5f);
	//float s1 = sin(yaw * 0.5f);
	//float c2 = cos(pitch * 0.5f);
	//float s2 = sin(pitch * 0.5f);
	//float c3 = cos(roll * 0.5f);
	//float s3 = sin(roll * 0.5f);
	//XMFLOAT4 result = XMFLOAT4();
	//result.x = s1 * s2 * c3 + c1 * c2 * s3;
	//result.y = s1 * c2 * c3 + c1 * s2 * s3;
	//result.z = c1 * s2 * c3 - s1 * c2 * s3;
	//result.w = c1 * c2 * c3 - s1 * s2 * s3;
	//return result;
}

// X is pitch, Y is yaw, Z is roll
static XMFLOAT3 QuaternionToEuler(float x, float y, float z, float w)
{
	XMFLOAT3 result = XMFLOAT3();

	//result.x = asin(2.0f * x * y + 2.0f * z * w);  //Pitch / Attitude
	//result.y = atan2(2 * y * w - 2 * x * z, 1 - 2 * y * y - 2 * z * z);  //Yaw   / Heading
	//result.z = atan2(2 * x * w - 2 * y * z, 1 - 2 * x * x - 2 * z * z);  //Roll  / Bank

	float ysqr = y * y;

	float t0 = 2.0f * (w * x + y * z);
	float t1 = 1.0f - 2.0f * (x * x + ysqr);
	float t2 = 2.0f * (w * y - z * x);
	float t3 = 2.0f * (w * z + x * y);
	float t4 = 1.0f - 2.0f * (ysqr + z * z);

	t2 = t2 > 1.0 ? 1.0 : t2;
	t2 = t2 < -1.0 ? -1.0 : t2;

	result.x = asin(t2);  // pitch
	result.y = atan2(t3, t4); // yaw
	result.z = atan2(t0, t1); // roll


	return result;
}

static float ToRadians(float degrees)
{
	return degrees * (XM_PI / 180.0f);
}

static PxVec3 XmToPxVec3(XMFLOAT3 vector)
{
	return PxVec3(vector.x, vector.y, vector.z);
}

static PxVec4 XmToPxVec4(XMFLOAT4 quat)
{
	return PxVec4(quat.x, quat.y, quat.z, quat.w);
}

static PxQuat XmToPxQuat(XMFLOAT4 quat)
{
	return PxQuat(quat.x, quat.y, quat.z, quat.w);
}

//FleX structs

struct float4
{
	float4(float x, float y, float z, float w):
		x(x), y(y), z(z), w(w)
	{
		
	}

	float4() :x(0),y(0),z(0),w(0)
	{

	}
	float x;
	float y;
	float z;
	float w;
};

static float4 XmTofloat4(XMFLOAT4 quat)
{
	return float4(quat.x, quat.y, quat.z, quat.w);
}

struct float3
{
	float3(float x, float y, float z) :
		x(x), y(y), z(z)
	{

	}

	float3():
		x(0), y(0), z(0)
	{
		
	}

	float x;
	float y;
	float z;
};
