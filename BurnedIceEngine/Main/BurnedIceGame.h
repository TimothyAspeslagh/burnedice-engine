#pragma once
#include "../Scene Classes/GameScene.h"
#include "../Managers/MainManager.h"

class BurnedIceGame
{
public:
	//methods
	BurnedIceGame(GameSettings * settings);
	virtual ~BurnedIceGame() {}

	ID3D11RenderTargetView* GetDefaultRenderTargetView() const {return m_pRenderTargetView;}

	GameScene * AddScene(GameScene * scene);

	void SetActiveScene(GameScene* scene)
	{
		m_pActiveScene = scene;
		m_GameManager.SetActiveScene(scene);
	}

	bool BaseInitialise();
	virtual bool Initialise() = 0;
	bool Run();
	void Clean();

	ID3D11Device* GetDevice() const { return m_pDevice; }
	ID3D11DeviceContext * GetDeviceContext() const { return m_pDeviceContext; }

	ID3D11DepthStencilView* GetDefaultDSV() const { return m_pDepthStencilView; }

	void BindDefaultRenderTargetView() const;
	ID3D11ShaderResourceView* GetSRV() const;
protected:
	bool m_FlexEnabled = false;

private:
	void Update();
	bool InitialiseWindow();
	bool InitialiseDX();
	void CleanDX();
	void CleanWindow();

	//members
	bool m_IsInitialised = false;
	vector<GameScene*> m_pScenes;
	GameScene * m_pActiveScene = nullptr;
	GameSettings* m_Settings;
	MainManager m_GameManager;

	//Window pointers
	HWND * m_pWindowHandle;
	HINSTANCE m_hInstance;


	//DirectX pointers
	IDXGISwapChain* m_pSwapChain;
	ID3D11Device * m_pDevice;
	//D3D_FEATURE_LEVEL * m_pFeatureLevel;
	ID3D11DeviceContext* m_pDeviceContext;
	ID3D11RenderTargetView * m_pRenderTargetView;
	ID3D11ShaderResourceView* m_pShaderResourceView;

	ID3D11Texture2D * m_pDepthStencilBuffer;
	ID3D11DepthStencilView * m_pDepthStencilView;

};

