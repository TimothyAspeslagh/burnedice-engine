#include "stdafx.h"
#include "BurnedIceGame.h"
#include "../Helpers/IceString.h"
#include <thread>
#include <chrono>
#include "../PhysX/FleXProxy.h"

bool gInputChanged = false;


BurnedIceGame::BurnedIceGame(GameSettings * settings)
{
	m_Settings = settings;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


GameScene * BurnedIceGame::AddScene(GameScene * scene)
{
	m_pScenes.push_back(scene);
	//return scene for easy reusage (e.g. when you want to set the scene as active)
	return scene;
}

bool BurnedIceGame::BaseInitialise()
{
	bool result = InitialiseWindow();
	if (!result)
		return result;
	result = InitialiseDX();
	if (!result)
		return result;
	
	//Initialise Managers
	m_GameManager.Initialise(m_pDevice, m_pDeviceContext, m_Settings, m_pWindowHandle, this, m_FlexEnabled);



	//Add Scenes to the game
	result = Initialise();
	if (!result)
		return result;
	
	//Initialise scenes and objects in scenes
	for (size_t i = 0; i != m_pScenes.size(); ++i)
		m_pScenes[i]->BaseInitialise(m_GameManager);

	if(m_FlexEnabled)
		m_pScenes[0]->GetFleXProxy()->SetDebugRendering(false);

	m_IsInitialised = true;
	return true;
}


bool BurnedIceGame::Run()
{
	if (m_IsInitialised == false)
		return false;

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	//Perform Tick once to handle deltaTime correctly;
	m_GameManager.Tick();

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
			Update();

	}
	return true;
}

void BurnedIceGame::Clean()
{
	//Cleanup happens in reverse order
	for(size_t i =0; i != m_pScenes.size(); ++i)
	{
		//Call all correct clean functions in all scenes
		m_pScenes[i]->BaseClean();

		delete m_pScenes[i];
		m_pScenes[i] = nullptr;
	}

	m_pActiveScene = nullptr;
	m_GameManager.Clean();


	CleanDX();
	CleanWindow();
}

void BurnedIceGame::BindDefaultRenderTargetView() const
{
	m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
}

ID3D11ShaderResourceView* BurnedIceGame::GetSRV() const
{
	return m_pShaderResourceView;
}

void BurnedIceGame::Update()
{
	//Process input first
	if (gInputChanged)
	{
		gInputChanged = false;
		m_GameManager.Input->ProcessKeys(true);
	}
	float frameCorrection = (1.0f / 60.0f) - m_GameManager.GetDeltaTime();
	if(frameCorrection > 0.0f)
	{
		//this_thread::sleep_for(std::chrono::seconds((long)frameCorrection));
	} //TODO: fix framerate


	m_GameManager.Tick();

	if (m_pActiveScene != nullptr)
		m_pActiveScene->Update(m_GameManager);

	m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, reinterpret_cast<const float*>(&Colors::CornflowerBlue));
	//m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, reinterpret_cast<const float*>(&Colors::Black));

	m_pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	m_pActiveScene->BaseDraw(m_GameManager);

	//DRAW FLUIDS
	if(m_FlexEnabled)
	{
		auto fleX = m_pActiveScene->GetFleXProxy();
		if (fleX->GetDebugRendering())
			fleX->DrawDebug(m_GameManager);
		else
		{
			fleX->Draw(m_GameManager);
		}

	}
	

	m_pSwapChain->Present(0, 0);

}

bool BurnedIceGame::InitialiseWindow()
{
	WNDCLASSEX wc;

	m_hInstance = GetModuleHandle(NULL);

	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_Settings->ApplicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	if (!RegisterClassEx(&wc))
	{
		wcout << L"RegisterClassEx() failed!" << endl;
		return false;
	}

	int nStyle = WS_OVERLAPPED | WS_SYSMENU | WS_VISIBLE | WS_CAPTION | WS_MINIMIZEBOX;

	m_pWindowHandle = new HWND();

	*m_pWindowHandle = CreateWindowEx(
		WS_EX_APPWINDOW,
		m_Settings->ApplicationName,
		m_Settings->ApplicationName, 
		nStyle, 
		CW_USEDEFAULT, 
		CW_USEDEFAULT, 
		m_Settings->WindowWidth, 
		m_Settings->WindowHeight, 
		NULL, 
		NULL, 
		m_hInstance, 
		NULL);

	if(m_pWindowHandle == NULL)
	{
		wcout << L"Failed to create window!" << endl;
		return false;
	}

	ShowWindow(*m_pWindowHandle, SW_SHOW);
	SetForegroundWindow(*m_pWindowHandle);
	SetFocus(*m_pWindowHandle);

	return true;
}

bool BurnedIceGame::InitialiseDX()
{
	auto featureLevel = D3D_FEATURE_LEVEL_11_0;

	UINT flags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	//Create Device
	HRESULT hr = D3D11CreateDevice(
		0,
		D3D_DRIVER_TYPE_HARDWARE,
		0,
		flags,
		0,
		0,
		D3D11_SDK_VERSION,
		&m_pDevice,
		&featureLevel,
		&m_pDeviceContext);

	if(FAILED(hr))
	{
		//TODO: Log result, make a LogManager
		wcout << L"Failed to Create Device!" << endl;
		return false;
	}
	if(featureLevel < D3D_FEATURE_LEVEL_11_0)
	{
		wcout << L"Direct3D Feature Level 11 unsupported!" << endl;
		return false;
	}

	//UINT MsaaQuality;
	//hr = m_pDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &MsaaQuality);
	//if(MsaaQuality <=0 || FAILED(hr))
	//{
	//	wcout << L"4X MSAA Quality not unsupported!" << endl;
	//	return false;
	//}

	//Describe Swap chain
	auto sD = DXGI_SWAP_CHAIN_DESC();
	sD.BufferDesc.Width = m_Settings->WindowWidth;
	sD.BufferDesc.Height = m_Settings->WindowHeight;
	sD.BufferDesc.RefreshRate.Numerator = 60;
	sD.BufferDesc.RefreshRate.Denominator = 1;
	sD.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sD.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sD.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sD.SampleDesc.Count = 1;
	sD.SampleDesc.Quality = 0;
	sD.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sD.BufferCount = 1;
	sD.OutputWindow = *m_pWindowHandle;
	sD.Windowed = true;
	sD.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sD.Flags = 0;

	IDXGIDevice * device = 0;
	hr = m_pDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&device);
	if(FAILED(hr))
	{
		wcout << L"Failed to create  IDXGIDevice!" << endl;
		return false;
	}

	IDXGIAdapter* adapter = 0;
	hr = device->GetParent(__uuidof(IDXGIAdapter), (void**)&adapter);
	if(FAILED(hr))
	{
		wcout << L"Failed to create IDXGIAdapter!" << endl;
		return false;
	}
	IDXGIFactory* factory = 0;
	hr = adapter->GetParent(__uuidof(IDXGIFactory), (void**)&factory);
	if(FAILED(hr))
	{
		wcout << L"Failed to create IDXGIFactory!" << endl;
		return false;
	}

	hr = factory->CreateSwapChain(m_pDevice, &sD, &m_pSwapChain);
	device->Release();
	adapter->Release();
	factory->Release();

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(rtvDesc));
	rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
	rtvDesc.Texture2D.MipSlice = 0;
	
	
	//Create Render Target View
	ID3D11Texture2D * backBuffer;
	m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
	hr = m_pDevice->CreateRenderTargetView(backBuffer, &rtvDesc, &m_pRenderTargetView);

	if(FAILED(hr))
	{
		wcout << L"Failed to create Render Target View!" << endl;
		return false;
	}

	//Create Shader Resource View (for FleX Fluids)
	//D3D11_TEXTURE2D_DESC bbDesc;
	//backBuffer->GetDesc(&bbDesc);
	//D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	//srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	//srvDesc.Texture2D.MipLevels = bbDesc.MipLevels;
	//srvDesc.Texture2D.MostDetailedMip = 0;
	//
	//hr = m_pDevice->CreateShaderResourceView(backBuffer, nullptr, &m_pShaderResourceView);
	//backBuffer->Release();
	//if(FAILED(hr))
	//{
	//	wcout << L"Failed to create Shader Resource View!" << endl;
	//	return false;
	//}


	//Create DepthStencil buffer & view
	auto depthStencilDesc = D3D11_TEXTURE2D_DESC();
	depthStencilDesc.Width = m_Settings->WindowWidth;
	depthStencilDesc.Height = m_Settings->WindowHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	//depthStencilDesc.Format = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	hr = m_pDevice->CreateTexture2D(&depthStencilDesc, 0, &m_pDepthStencilBuffer);
	if(FAILED(hr))
	{
		wcout << L"Failed to create Depth Stencil Buffer!" << endl;
		return false;
	}

	//DEPTHSTENCIL VIEW
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));

	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	hr = m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer, &descDSV, &m_pDepthStencilView);
	if(FAILED(hr))
	{
		wcout << L"Failed to create Depth Stencil View!" << endl;
		return false;
	}

	m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

	//Set viewport
	auto viewPort = D3D11_VIEWPORT();
	viewPort.TopLeftX = 0.0f;
	viewPort.TopLeftY = 0.0f;
	viewPort.Width = (float)m_Settings->WindowWidth;
	viewPort.Height = (float)m_Settings->WindowHeight;
	viewPort.MinDepth = 0.0f;
	viewPort.MaxDepth = 1.0f;

	m_pDeviceContext->RSSetViewports(1, &viewPort);

	return true;
}

void BurnedIceGame::CleanDX()
{
	m_pRenderTargetView->Release();
	m_pDepthStencilBuffer->Release();
	m_pDepthStencilView->Release();
	//delete m_pFeatureLevel;
	//m_pFeatureLevel = nullptr;

	m_pSwapChain->Release();
	m_pDeviceContext->ClearState();
	m_pDeviceContext->Flush();
	m_pDeviceContext->Release();
	m_pDevice->Release();

	//delete m_pRenderTargetView;
	//m_pRenderTargetView = nullptr;
	//delete m_pDepthStencilBuffer;
	//m_pDepthStencilBuffer = nullptr;
	//delete m_pDepthStencilView;
	//m_pDepthStencilView = nullptr;
	//delete m_pSwapChain;
	//m_pSwapChain = nullptr;
	//delete m_pDeviceContext;
	//m_pDeviceContext = nullptr;
	//delete m_pDevice;
	//m_pDevice = nullptr;
}

void BurnedIceGame::CleanWindow()
{
	UnregisterClass(m_Settings->ApplicationName, m_hInstance);
	m_hInstance = nullptr;
	DestroyWindow(*m_pWindowHandle);

	delete m_pWindowHandle;
	m_pWindowHandle = nullptr;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	case WM_CLOSE:
		PostQuitMessage(0);
		DestroyWindow(hWnd);
		break;
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_KEYUP:
	case WM_KEYDOWN:
		//todo: Find a more elegant solution
		gInputChanged = true;
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}