#pragma once
class PhysXProxy
{
public:
	PhysXProxy();
	~PhysXProxy();

	void Initialise(const MainManager &gameManager);
	void Tick(const MainManager &gameManager);
	void Draw(const MainManager &gameManager);
	void Clean();
	void SetParent(GameScene* parent) { m_pParent = parent; }
	void SetDebugRendering(bool state) { m_EnableDebugRendering = state; }
	PxScene* GetScene() const { return m_pScene; }

	friend class GameScene;

private:
	PxScene* m_pScene = nullptr;
	PxControllerManager* m_pControllerManager = nullptr;
	GameScene * m_pParent = nullptr;
	bool m_EnableDebugRendering = false;
};

