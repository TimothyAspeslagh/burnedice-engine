#pragma once
#include "foundation/PxErrorCallback.h"
#include <iostream>
using namespace physx;

class PhysXErrorCallback : public PxErrorCallback
{
public:
	PhysXErrorCallback(void);
	~PhysXErrorCallback(void);

	virtual void reportError(PxErrorCode::Enum code, const char* message, const char* file, int line) override;
};