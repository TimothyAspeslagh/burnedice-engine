#include "stdafx.h"
#include "physXCallback.h"

PhysXErrorCallback::PhysXErrorCallback()
{
}

PhysXErrorCallback::~PhysXErrorCallback()
{
}

void PhysXErrorCallback::reportError(PxErrorCode::Enum code, const char* message, const char* file, int line)
{
	std::wcout << L"PhysX error: " << std::endl
		<< L"Code: " << code << std::endl
		<< L"message: " << message << std::endl
		<< L"file: " << file << std::endl
		<< L"line: " << line << std::endl;
}