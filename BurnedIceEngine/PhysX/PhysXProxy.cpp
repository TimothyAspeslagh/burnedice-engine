#include "stdafx.h"
#include "PhysXProxy.h"
#include "IceSimulationFilter.h"


PhysXProxy::PhysXProxy()
{
}


PhysXProxy::~PhysXProxy()
{
}

void PhysXProxy::Initialise(const MainManager& gameManager)
{
	auto physX = gameManager.Physics->GetPhysX();
	if (m_pScene != nullptr)
	{
		wcout << L"PhysX: m_pScene already initialised!" << endl;
		return;
	}
	if(m_pParent == nullptr)
	{
		wcout << L"Scene parent not defined!" << endl;
	}
		

	PxSceneDesc sceneDesc = PxSceneDesc(physX->getTolerancesScale());
	sceneDesc.setToDefault(physX->getTolerancesScale());
	sceneDesc.gravity = gameManager.Physics->GetGravity();
	sceneDesc.userData = m_pParent;
	sceneDesc.cpuDispatcher = gameManager.Physics->GetCpuDispatcher();
	sceneDesc.gpuDispatcher = nullptr; //todo: Gpu dispatcher necessary for FleX?
	sceneDesc.filterShader = IceSimulationFilter;

	m_pScene = gameManager.Physics->GetPhysX()->createScene(sceneDesc);
	if(m_pScene == nullptr)
	{
		wcout << L"PhysX: Failed to create scene!" << endl;
		assert(false);
	}


	m_pControllerManager = PxCreateControllerManager(*m_pScene);
	m_pScene->setSimulationEventCallback(m_pParent);
}

void PhysXProxy::Tick(const MainManager& gameManager)
{
	m_pScene->simulate(gameManager.GetDeltaTime());
	wcout << gameManager.GetDeltaTime() << endl;
	m_pScene->fetchResults(true);
}

void PhysXProxy::Draw(const MainManager& gameManager)
{
	if(m_EnableDebugRendering)
	{
		//TODO: Send info to Debug Renderer
	}
}

void PhysXProxy::Clean()
{
	m_pScene->release();
	m_pParent = nullptr;
	m_pControllerManager->release();
}