#pragma once
#include "../../BurnedIceProject/FluidDepthMat.h"
#include "../../BurnedIceProject/FluidThicknessMat.h"
#include "../../BurnedIceProject/FluidBlurMat.h"
#include "../../BurnedIceProject/FluidCombineMat.h"
#include "../../BurnedIceProject/FluidDebugDraw.h"
//#include "../../BurnedIceProject/FluidDebugDraw.h"
class FluidMaterial;
class MainManager;
struct FlexBuffers;
struct FluidBox;

class FleXProxy
{
public:
	FleXProxy();
	~FleXProxy();

	void Initialise(const MainManager &gameManager);
	void Tick(const MainManager &gameManager);
	void Draw(const MainManager &gameManager);
	void DrawDebug(const MainManager& gameManager);
	void Clean();
	void SetParent(GameScene* parent) { m_pParent = parent; }
	void SetDebugRendering(bool state);
	bool GetDebugRendering() const { return m_EnableDebugRendering; }
	NvFlexSolver* GetSolver() const { return m_pSolver; }
	void AddRigidByMesh(Mesh* pMesh);
	void AddFluidBox(PxVec3 pos, int dimX, int dimY, int dimZ, float density);

	void SetGravity(float3 gravity);

	friend class GameScene;
	friend class Mesh;
	friend class Material;

private:
	void ApplyChanges();
	void MapBuffers();
	void UnmapBuffers();

	NvFlexParams m_FlexParams;

	NvFlexSolver* m_pSolver = nullptr;
	GameScene * m_pParent = nullptr;
	bool m_EnableDebugRendering = true;

	FlexBuffers * m_pFlexBuffer;

	int m_ElementCount = 0;
	const int m_MaxParticles = 1005000;
	const int m_MaxDiffuseParticles = 10;
	int m_NumParticles = 0;
	vector<FluidBox> m_FluidBoxesToAdd;


	int m_FluidPhase;
	int m_RigidPhase;

	bool m_UserChangesMade = false;
	bool m_ShapesChanged = false;

	Mesh* m_pSphereMesh = nullptr;
	FluidDepthMat * m_pFluidDepthMat = nullptr;
	FluidThicknessMat *m_pFluidThicknessMat = nullptr;
	FluidBlurMat * m_pFluidBlurMat = nullptr;
	FluidCombineMat* m_pFluidCombineMat = nullptr;
	FluidDebugDraw* m_pFluidDebugMat = nullptr;

	//FluidMaterial * m_pFluidMaterial = nullptr;
	Texture * m_pDebugTexture = nullptr;
};

struct FluidBox
{
	FluidBox(PxVec3 pos, int dimX, int dimY, int dimZ, float density):pos(pos), dimX(dimX),dimY(dimY), dimZ(dimZ), density(density)
	{
		
	}
	PxVec3 pos; 
	int dimX; 
	int dimY;
	int dimZ;
	float density;
};

struct FlexBuffers
{
	//Buffers
	NvFlexVector<float4> m_Positions;
	NvFlexVector<float4> m_RestPositions;
	NvFlexVector<float3> m_Velocities;
	NvFlexVector<int> m_Phases;
	NvFlexVector<float> m_Densities;
	NvFlexVector<float4> m_Anisotropy1;
	NvFlexVector<float4> m_Anisotropy2;
	NvFlexVector<float4> m_Anisotropy3;
	NvFlexVector<float4> m_Normals;
	NvFlexVector<float4> m_SmoothPositions;
	NvFlexVector<float4> m_DiffusePositions;
	NvFlexVector<float4> m_DiffuseVelocities;
	NvFlexVector<int> m_DiffuseIndices;
	NvFlexVector<int> m_ActiveIndices;

	//Convexes
	NvFlexVector<NvFlexCollisionGeometry> m_ShapeGeometry;
	NvFlexVector<float4> m_ShapePositions;
	NvFlexVector<float4> m_ShapeRotations;
	NvFlexVector<float4> m_ShapePrevPositions;
	NvFlexVector<float4> m_ShapePrevRotations;
	NvFlexVector<int> m_ShapeFlags;

	//rigids
	NvFlexVector<int> m_RigidOffsets;
	NvFlexVector<int> m_RigidIndices;
	NvFlexVector<int> m_RigidMeshSize;
	NvFlexVector<float> m_RigidCoefficients;
	NvFlexVector<float4> m_RigidRotations;
	NvFlexVector<float3> m_RigidTranslations;
	NvFlexVector<float3> m_RigidLocalPositions;
	NvFlexVector<float4> m_RigidLocalNormals;

	FlexBuffers(NvFlexLibrary* lib) :
		m_Positions(lib),
	m_RestPositions(lib),
	m_Velocities(lib),
	m_Phases(lib),
	m_Densities(lib),
	m_Anisotropy1(lib),
	m_Anisotropy2(lib),
	m_Anisotropy3(lib),
	m_Normals(lib),
	m_SmoothPositions(lib),
	m_DiffusePositions(lib),
	m_DiffuseVelocities(lib),
	m_DiffuseIndices(lib),
	m_ActiveIndices(lib),

	//Convexes
	m_ShapeGeometry(lib),
	m_ShapePositions(lib),
	m_ShapeRotations(lib),
	m_ShapePrevPositions(lib),
	m_ShapePrevRotations(lib),
	m_ShapeFlags(lib),

	//Rigids
	m_RigidOffsets(lib),
	m_RigidIndices(lib),
	m_RigidMeshSize(lib),
	m_RigidCoefficients(lib),
	m_RigidRotations(lib),
	m_RigidTranslations(lib),
	m_RigidLocalPositions(lib),
	m_RigidLocalNormals(lib)
	{
		m_Positions.resize(0);
		m_RestPositions.resize(0);
		m_Velocities.resize(0);
		m_Phases.resize(0);
		m_Densities.resize(0);
		m_Anisotropy1.resize(0);
		m_Anisotropy2.resize(0);
		m_Anisotropy3.resize(0);
		m_Normals.resize(0);
		m_SmoothPositions.resize(0);
		m_DiffusePositions.resize(0);
		m_DiffuseVelocities.resize(0);
		m_DiffuseIndices.resize(0);
		m_ActiveIndices.resize(0);


		m_ShapeGeometry.resize(0);
		m_ShapePositions.resize(0);
		m_ShapeRotations.resize(0);
		m_ShapePrevPositions.resize(0);
		m_ShapePrevRotations.resize(0);
		m_ShapeFlags.resize(0);


		m_RigidOffsets.resize(0);
		m_RigidIndices.resize(0);
		m_RigidMeshSize.resize(0);
		m_RigidCoefficients.resize(0);
		m_RigidRotations.resize(0);
		m_RigidTranslations.resize(0);
		m_RigidLocalPositions.resize(0);
		m_RigidLocalNormals.resize(0);

	}

	~FlexBuffers()
	{
		m_Positions.destroy();
		m_RestPositions.destroy();
		m_Velocities.destroy();
		m_Phases.destroy();
		m_Densities.destroy();
		m_Anisotropy1.destroy();
		m_Anisotropy2.destroy();
		m_Anisotropy3.destroy();
		m_Normals.destroy();
		m_SmoothPositions.destroy();
		m_DiffusePositions.destroy();
		m_DiffuseVelocities.destroy();
		m_DiffuseIndices.destroy();
		m_ActiveIndices.destroy();


		m_ShapeGeometry.destroy();
		m_ShapePositions.destroy();
		m_ShapeRotations.destroy();
		m_ShapePrevPositions.destroy();
		m_ShapePrevRotations.destroy();
		m_ShapeFlags.destroy();


		m_RigidOffsets.destroy();
		m_RigidIndices.destroy();
		m_RigidMeshSize.destroy();
		m_RigidCoefficients.destroy();
		m_RigidRotations.destroy();
		m_RigidTranslations.destroy();
		m_RigidLocalPositions.destroy();
		m_RigidLocalNormals.destroy();

	}
};
