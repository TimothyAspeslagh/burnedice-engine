#include "stdafx.h"
#include "FleXProxy.h"
#include "../../BurnedIceProject/FluidMaterial.h"
#include <array>
#include "../../BurnedIceProject/FluidDebugDraw.h"


FleXProxy::FleXProxy()
{
}


FleXProxy::~FleXProxy()
{
}

void FleXProxy::Initialise(const MainManager& gameManager)
{
	//Initialise Mesh and Materials
	m_pSphereMesh = gameManager.Content->LoadMesh(DesignString("./Resources/Sphere.fbx"), DesignString("sphere"));
	m_pFluidDepthMat = gameManager.Content->LoadMaterial<FluidDepthMat>(DesignString("./Resources/FluidDepth.fx"), DesignString("FluidDepthMat"));
	m_pFluidDepthMat->CreateVertexBuffer(gameManager, m_MaxParticles);

	m_pFluidThicknessMat = gameManager.Content->LoadMaterial<FluidThicknessMat>(DesignString("./Resources/FluidThickness.fx"), DesignString("FluidThicknessMat"));
	m_pFluidThicknessMat->CreateVertexBuffer(gameManager, m_MaxParticles);

	m_pFluidBlurMat = gameManager.Content->LoadMaterial<FluidBlurMat>(DesignString("./Resources/FluidBlur.fx"), DesignString("FluidBlurMat"));

	m_pFluidCombineMat = gameManager.Content->LoadMaterial<FluidCombineMat>(DesignString("./Resources/FluidDraw.fx"), DesignString("FluidCombineMat"));

	m_pFluidDebugMat = gameManager.Content->LoadMaterial<FluidDebugDraw>(DesignString("./Resources/FluidDebug.fx"), DesignString("FluidDebugDraw"));
	
	auto texture = gameManager.Content->LoadTexture(DesignString("./Resources/karl.png"), DesignString("Pepe"));
	m_pFluidDebugMat->SetDiffuseTexture(texture);

	auto library = gameManager.FleX->GetLibrary();

	m_FluidPhase = NvFlexMakePhase(0, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid);

	//m_FluidPhase = NvFlexMakePhase(0, eNvFlexPhaseFluid);
	m_RigidPhase = NvFlexMakePhase(1, 0);

	float radius = .20f;
	#pragma region Set Parameters
	m_FlexParams.gravity[0] = 0.0f;
	m_FlexParams.gravity[1] = 0.0f;
	m_FlexParams.gravity[1] = -9.81f;
	m_FlexParams.gravity[2] = 0.0f;
	m_FlexParams.wind[0] = 0.0f;
	m_FlexParams.wind[1] = 0.0f;
	m_FlexParams.wind[2] = 0.0f;

	m_FlexParams.radius = radius;
	m_FlexParams.viscosity = 0.0f;
	m_FlexParams.dynamicFriction = 0.0f;
	m_FlexParams.staticFriction = 0.0f;
	m_FlexParams.particleFriction = 0.0f; // scale friction between particles by default
	m_FlexParams.freeSurfaceDrag = 0.0f;
	m_FlexParams.drag = 0.0f;
	m_FlexParams.lift = 0.0f;
	m_FlexParams.numIterations = 3;
	m_FlexParams.fluidRestDistance = radius * 0.7f;
	m_FlexParams.solidRestDistance = radius * 0.7f;

	m_FlexParams.anisotropyScale = 30.0f;
	m_FlexParams.anisotropyMin = 0.1f;
	m_FlexParams.anisotropyMax = 2.0f;
	m_FlexParams.smoothing = .4f;

	m_FlexParams.dissipation = 0.0f;
	m_FlexParams.damping = 0.0f;
	m_FlexParams.particleCollisionMargin = 0.0f;
	m_FlexParams.shapeCollisionMargin = 0.00f;
	m_FlexParams.collisionDistance = .05f;
	m_FlexParams.plasticThreshold = 0.0f;
	m_FlexParams.plasticCreep = 0.0f;
	m_FlexParams.fluid = true;
	m_FlexParams.sleepThreshold = 0.0f;
	m_FlexParams.shockPropagation = 0.0f;
	m_FlexParams.restitution = 0.0f;

	m_FlexParams.maxSpeed = FLT_MAX;
	m_FlexParams.maxAcceleration = 100.0f;	// approximately 10x gravity

	m_FlexParams.relaxationMode = eNvFlexRelaxationLocal;
	m_FlexParams.relaxationFactor = 1.0f;
	m_FlexParams.solidPressure = 1.0f;
	m_FlexParams.adhesion = 0.0f;
	m_FlexParams.cohesion = 0.05f;
	m_FlexParams.surfaceTension = 0.0f;
	m_FlexParams.vorticityConfinement = 0.0f;
	m_FlexParams.buoyancy = 1.0f;
	m_FlexParams.diffuseThreshold = 100.0f;
	m_FlexParams.diffuseBuoyancy = 1.0f;
	m_FlexParams.diffuseDrag = 0.8f;
	m_FlexParams.diffuseBallistic = 16;
	m_FlexParams.diffuseSortAxis[0] = 0.0f;
	m_FlexParams.diffuseSortAxis[1] = 0.0f;
	m_FlexParams.diffuseSortAxis[2] = 0.0f;
	m_FlexParams.diffuseLifetime = 2.0f;

	// planes created after particles
	m_FlexParams.numPlanes = 5;

	float boxWidth = 3.f;

	//GROUND PLANE
	m_FlexParams.planes[0][0] = 0.f;
	m_FlexParams.planes[0][1] = 1.f;
	m_FlexParams.planes[0][2] = 0.f;
	m_FlexParams.planes[0][3] = 0.f;

	//SIDE PLANES
	m_FlexParams.planes[1][0] = 1.f;
	m_FlexParams.planes[1][1] = 0.f;
	m_FlexParams.planes[1][2] = 0.f;
	m_FlexParams.planes[1][3] = boxWidth / 2.0f;

	m_FlexParams.planes[2][0] = -1.f;
	m_FlexParams.planes[2][1] = 0.f;
	m_FlexParams.planes[2][2] = 0.f;
	m_FlexParams.planes[2][3] = boxWidth / 2.0f;

	m_FlexParams.planes[3][0] = 0.f;
	m_FlexParams.planes[3][1] = 0.f;
	m_FlexParams.planes[3][2] = 1.f;
	m_FlexParams.planes[3][3] = boxWidth / 2.0f;

	m_FlexParams.planes[4][0] = 0.f;
	m_FlexParams.planes[4][1] = 0.f;
	m_FlexParams.planes[4][2] = -1.f;
	m_FlexParams.planes[4][3] = boxWidth / 2.0f;



#pragma endregion


	//Initialise the rest
	
	int byteStride = sizeof(PxVec4);
	m_pSolver = NvFlexCreateSolver(library, m_MaxParticles, m_MaxDiffuseParticles);
	m_pFlexBuffer = new FlexBuffers(library);

#pragma region Resize Buffers

	m_pFlexBuffer->m_DiffusePositions.resize(m_MaxDiffuseParticles);
	m_pFlexBuffer->m_DiffuseVelocities.resize(m_MaxDiffuseParticles);
	m_pFlexBuffer->m_DiffuseIndices.resize(m_MaxDiffuseParticles);

	m_pFlexBuffer->m_SmoothPositions.resize(m_MaxParticles);
	m_pFlexBuffer->m_Normals.resize(m_MaxParticles);

	m_pFlexBuffer->m_ActiveIndices.resize(m_pFlexBuffer->m_Positions.size());
	for (int i = 0; i < m_pFlexBuffer->m_ActiveIndices.size(); ++i)
		m_pFlexBuffer->m_ActiveIndices[i] = i;

	m_pFlexBuffer->m_Positions.resize(m_MaxParticles);
	m_pFlexBuffer->m_Velocities.resize(m_MaxParticles);
	m_pFlexBuffer->m_Phases.resize(m_MaxParticles);
	m_pFlexBuffer->m_Densities.resize(m_MaxParticles);
	m_pFlexBuffer->m_Anisotropy1.resize(m_MaxParticles);
	m_pFlexBuffer->m_Anisotropy2.resize(m_MaxParticles);
	m_pFlexBuffer->m_Anisotropy3.resize(m_MaxParticles);

	m_pFlexBuffer->m_RestPositions.resize(m_pFlexBuffer->m_Positions.size());
	for(int i = 0; i < 	m_pFlexBuffer->m_Positions.size(); ++i)
	{
		m_pFlexBuffer->m_RestPositions[i] = m_pFlexBuffer->m_Positions[i];
	}

	if(m_pFlexBuffer->m_RigidOffsets.size() != 0)
	{
		m_pFlexBuffer->m_RigidLocalPositions.resize(m_pFlexBuffer->m_RigidOffsets.back());
		//TODO: Calculate rigid local positions

		m_pFlexBuffer->m_RigidRotations.resize(m_pFlexBuffer->m_RigidOffsets.size() - 1, float4());
		m_pFlexBuffer->m_RigidTranslations.resize(m_pFlexBuffer->m_RigidOffsets.size() - 1, float3());
	}
#pragma endregion

	UnmapBuffers();

#pragma region SetBuffers
	NvFlexSetParams(m_pSolver, &m_FlexParams);
	NvFlexSetParticles(m_pSolver, m_pFlexBuffer->m_Positions.buffer, m_NumParticles);
	NvFlexSetVelocities(m_pSolver, m_pFlexBuffer->m_Velocities.buffer, m_NumParticles);
	NvFlexSetNormals(m_pSolver, m_pFlexBuffer->m_Normals.buffer, m_NumParticles);
	NvFlexSetPhases(m_pSolver, m_pFlexBuffer->m_Phases.buffer, m_pFlexBuffer->m_Phases.size());
	NvFlexSetRestParticles(m_pSolver, m_pFlexBuffer->m_RestPositions.buffer, m_pFlexBuffer->m_RestPositions.size());

	NvFlexSetActive(m_pSolver, m_pFlexBuffer->m_ActiveIndices.buffer, m_NumParticles);

	if(m_pFlexBuffer->m_RigidOffsets.size())
	{
		NvFlexSetRigids(m_pSolver, m_pFlexBuffer->m_RigidOffsets.buffer,
			m_pFlexBuffer->m_RigidIndices.buffer,
			m_pFlexBuffer->m_RigidLocalPositions.buffer,
			m_pFlexBuffer->m_RigidLocalNormals.buffer,
			m_pFlexBuffer->m_RigidCoefficients.buffer,
			m_pFlexBuffer->m_RigidRotations.buffer,
			m_pFlexBuffer->m_RigidTranslations.buffer,
			m_pFlexBuffer->m_RigidOffsets.size() - 1,
			m_pFlexBuffer->m_RigidIndices.size());
	}

	if(m_pFlexBuffer->m_ShapeFlags.size())
	{
		NvFlexSetShapes(m_pSolver,
			m_pFlexBuffer->m_ShapeGeometry.buffer,
			m_pFlexBuffer->m_ShapePositions.buffer,
			m_pFlexBuffer->m_ShapeRotations.buffer,
			m_pFlexBuffer->m_ShapePrevPositions.buffer,
			m_pFlexBuffer->m_ShapePrevRotations.buffer,
			m_pFlexBuffer->m_ShapeFlags.buffer,
			(int)m_pFlexBuffer->m_ShapeFlags.size());
	}

#pragma endregion
	//todo: warmup?
}

void FleXProxy::Tick(const MainManager& gameManager)
{
	MapBuffers();

	//TODO: Check what is most performant

	ApplyChanges();

	UnmapBuffers();

	//write to device
	NvFlexSetParticles(m_pSolver, m_pFlexBuffer->m_Positions.buffer, m_pFlexBuffer->m_Positions.size());
	NvFlexSetVelocities(m_pSolver, m_pFlexBuffer->m_Velocities.buffer, m_pFlexBuffer->m_Velocities.size());
	NvFlexSetPhases(m_pSolver, m_pFlexBuffer->m_Phases.buffer, m_pFlexBuffer->m_Phases.size());
	NvFlexSetActive(m_pSolver, m_pFlexBuffer->m_ActiveIndices.buffer, m_pFlexBuffer->m_ActiveIndices.size());

	if(m_ShapesChanged)
	{
		NvFlexSetShapes(m_pSolver, m_pFlexBuffer->m_ShapeGeometry.buffer,
			m_pFlexBuffer->m_ShapePositions.buffer,
			m_pFlexBuffer->m_ShapeRotations.buffer,
			m_pFlexBuffer->m_ShapePrevPositions.buffer,
			m_pFlexBuffer->m_ShapePrevRotations.buffer,
			m_pFlexBuffer->m_ShapeFlags.buffer,		
			m_pFlexBuffer->m_ShapeFlags.size());
	}

	//Tick
	NvFlexSetParams(m_pSolver, &m_FlexParams);
	NvFlexUpdateSolver(m_pSolver, gameManager.GetDeltaTime(), 3, false);
	
	//Read back
	NvFlexGetParticles(m_pSolver, m_pFlexBuffer->m_Positions.buffer, m_pFlexBuffer->m_Positions.size());
	NvFlexGetVelocities(m_pSolver, m_pFlexBuffer->m_Velocities.buffer, m_pFlexBuffer->m_Velocities.size());
	NvFlexGetNormals(m_pSolver, m_pFlexBuffer->m_Normals.buffer, m_pFlexBuffer->m_Normals.size());

	if (m_pFlexBuffer->m_RigidOffsets.size() > 0)
		NvFlexGetRigidTransforms(m_pSolver, m_pFlexBuffer->m_RigidRotations.buffer, m_pFlexBuffer->m_RigidTranslations.buffer);

	///	NvFlexGetSmoothParticles(m_pSolver, m_pFlexBuffer->m_SmoothPositions.buffer, m_pFlexBuffer->m_SmoothPositions.size());
	//NvFlexGetAnisotropy(m_pSolver, m_pFlexBuffer->m_Anisotropy1.buffer, m_pFlexBuffer->m_Anisotropy2.buffer, m_pFlexBuffer->m_Anisotropy3.buffer);
	NvFlexGetDensities(m_pSolver, m_pFlexBuffer->m_Densities.buffer, m_pFlexBuffer->m_Densities.size());
	NvFlexGetActive(m_pSolver, m_pFlexBuffer->m_ActiveIndices.buffer);
}

void FleXProxy::Draw(const MainManager& gameManager)
{
	MapBuffers();
	auto rad = m_FlexParams.radius;
	m_pFluidDepthMat->SetRadius(rad);
	m_pFluidThicknessMat->SetRadius(rad);

	//1. Set RenderTarget to custom depth buffer
	m_pFluidDepthMat->ClearRTV(gameManager);
	m_pFluidDepthMat->SetRenderTarget(gameManager);

	//2. Render to this buffer
	for(size_t i = 0; i != m_pFlexBuffer->m_ActiveIndices.size(); ++i)
	{
		size_t index = m_pFlexBuffer->m_ActiveIndices[i];
		auto pos = m_pFlexBuffer->m_Positions[index];

		m_pFluidDepthMat->SetPosition(pos);
		m_pFluidDepthMat->RootUpdateShaderVariables(gameManager);
		m_pFluidDepthMat->Draw(gameManager);
	}

	//3. Set RenderTarget to custom Thickness buffer
	m_pFluidThicknessMat->ClearRTV(gameManager);
	m_pFluidThicknessMat->SetRenderTarget(gameManager);

	//4. Render to this buffer
	for (size_t i = 0; i != m_pFlexBuffer->m_ActiveIndices.size(); ++i)
	{
		size_t index = m_pFlexBuffer->m_ActiveIndices[i];
		auto pos = m_pFlexBuffer->m_Positions[index];

		m_pFluidThicknessMat->SetPosition(pos);
		m_pFluidThicknessMat->RootUpdateShaderVariables(gameManager);
		m_pFluidThicknessMat->Draw(gameManager);
	}

	//5. Blur both buffers (like a post processing effect)
	m_pFluidBlurMat->ClearRTV(gameManager);
	m_pFluidBlurMat->SetRenderTarget(gameManager);

	m_pFluidBlurMat->SetDepthTexture(m_pFluidDepthMat->GetSRV());
	m_pFluidBlurMat->SetThicknessTexture(m_pFluidThicknessMat->GetSRV());

	m_pFluidBlurMat->Draw(gameManager);

	//6. Reset Render Target
	gameManager.BindDefaultRenderTarget();

	//7. Combine information to visual output
	m_pFluidCombineMat->UpdateShaderVariables(gameManager);
	m_pFluidCombineMat->SetTextureInfo(m_pFluidBlurMat->GetSRV());
//	m_pFluidCombineMat->SetTexture(gameManager.GetSRV());
	m_pFluidCombineMat->Draw(gameManager);
	
	
	UnmapBuffers();

}

void FleXProxy::DrawDebug(const MainManager& gameManager)
{
	MapBuffers();
	auto rad = m_FlexParams.radius;
	m_pFluidDebugMat->SetRadius(rad);
	for (size_t i = 0; i != m_pFlexBuffer->m_ActiveIndices.size(); ++i)
	{
		size_t index = m_pFlexBuffer->m_ActiveIndices[i];
		auto pos = m_pFlexBuffer->m_Positions[index];

		m_pFluidDebugMat->SetPosition(pos);
		m_pFluidDebugMat->RootUpdateShaderVariables(gameManager);
		m_pFluidDebugMat->Draw(gameManager);
	}
	
	UnmapBuffers();
}

void FleXProxy::Clean()
{
	delete m_pFlexBuffer;
	m_pFlexBuffer = nullptr;

	//m_pFluidDepthMat->Clean();
	//m_pFluidThicknessMat->Clean();
	//m_pFluidBlurMat->Clean();

	//delete m_pFluidDepthMat;
	m_pFluidDepthMat = nullptr;

	//delete m_pFluidThicknessMat;
	m_pFluidThicknessMat = nullptr;

	//delete m_pFluidBlurMat;
	m_pFluidBlurMat = nullptr;

	m_pFluidDebugMat = nullptr;

	NvFlexDestroySolver(m_pSolver);
}

void FleXProxy::SetDebugRendering(bool state)
{
	m_EnableDebugRendering = state; 
	//m_pFluidMaterial->SetDebugMode(state);
}

void FleXProxy::AddRigidByMesh(Mesh* pMesh)
{
	m_UserChangesMade = true;

	//TODO!
	const float spacing = 0.05f;
	
	//auto asset = NvFlexExtCreateRigidFromMesh(
	//	(float*)pMesh->m_VertexList[0],
	//	pMesh->m_VertexList.size(), 
	//	(int*)pMesh->m_IndexList[0],
	//	pMesh->m_VertexList.size(),
	//	spacing,
	//	-spacing *0.5f);
}

void FleXProxy::AddFluidBox(PxVec3 pos, int dimX, int dimY, int dimZ, float density)
{
	m_FluidBoxesToAdd.push_back(FluidBox(pos, dimX, dimY, dimZ, density));
	m_UserChangesMade = true;
}

void FleXProxy::SetGravity(float3 gravity)
{
	m_FlexParams.gravity[0] = gravity.x;
	m_FlexParams.gravity[1] = gravity.y; 
	m_FlexParams.gravity[2] = gravity.z;

	NvFlexSetParams(m_pSolver, &m_FlexParams);
}

void FleXProxy::ApplyChanges()
{
	if (!m_UserChangesMade)
		return;

	m_UserChangesMade = false;

	for(auto it= m_FluidBoxesToAdd.begin(); it != m_FluidBoxesToAdd.end(); ++it)
	{
		int index = NvFlexGetActiveCount(m_pSolver) + 1;

		for (int x = 0; x < (*it).dimX; ++x)
		{
			for (int y = 0; y < (*it).dimY; ++y)
			{
				for (int z = 0; z < (*it).dimZ; ++z)
				{
					//auto index = m_pFlexBuffer->m_ActiveIndices[m_pFlexBuffer->m_ActiveIndices.size() - 1];
					//++index;
					if (m_ElementCount + 1 > m_MaxParticles)
						return;


					++m_ElementCount;
					PxVec3 corner = (*it).pos - (PxVec3((*it).dimX / 2, (*it).dimY / 2, (*it).dimZ / 2) * (*it).density);
					PxVec3 localPos = corner + (PxVec3(x, y, z) * (*it).density);

					m_pFlexBuffer->m_Positions[index] = (float4(localPos.x, localPos.y, localPos.z, 1.0f));
					m_pFlexBuffer->m_Velocities[index] = (float3(0.0f, 0.0f, 0.0f));
					m_pFlexBuffer->m_Phases[index] = (m_FluidPhase);
					m_pFlexBuffer->m_ActiveIndices.push_back(index);
					++index;
				}
			}
		}

	}
}

#pragma region MapFunctions
void FleXProxy::MapBuffers()
{
	//General & Fluids
	m_pFlexBuffer->m_Positions.map();
	m_pFlexBuffer->m_RestPositions.map();
	m_pFlexBuffer->m_Velocities.map();
	m_pFlexBuffer->m_Phases.map();
	m_pFlexBuffer->m_Densities.map();
	m_pFlexBuffer->m_Anisotropy1.map();
	m_pFlexBuffer->m_Anisotropy2.map();
	m_pFlexBuffer->m_Anisotropy3.map();
	m_pFlexBuffer->m_Normals.map();
//	m_pFlexBuffer->m_SmoothPositions.map();
	m_pFlexBuffer->m_DiffusePositions.map();
	m_pFlexBuffer->m_DiffuseVelocities.map();
	m_pFlexBuffer->m_DiffuseIndices.map();
	m_pFlexBuffer->m_ActiveIndices.map();

	//Convexes
	m_pFlexBuffer->m_ShapeGeometry.map();
	m_pFlexBuffer->m_ShapePositions.map();
	m_pFlexBuffer->m_ShapeRotations.map();
	m_pFlexBuffer->m_ShapePrevPositions.map();
	m_pFlexBuffer->m_ShapePrevRotations.map();
	m_pFlexBuffer->m_ShapeFlags.map();

	//Rigids
	m_pFlexBuffer->m_RigidOffsets.map();
	m_pFlexBuffer->m_RigidIndices.map();
	m_pFlexBuffer->m_RigidMeshSize.map();
	m_pFlexBuffer->m_RigidCoefficients.map();
	m_pFlexBuffer->m_RigidRotations.map();
	m_pFlexBuffer->m_RigidTranslations.map();
	m_pFlexBuffer->m_RigidLocalPositions.map();
	m_pFlexBuffer->m_RigidLocalNormals.map();
}

void FleXProxy::UnmapBuffers()
{

	//General & Fluids
	m_pFlexBuffer->m_Positions.unmap();
	m_pFlexBuffer->m_RestPositions.unmap();
	m_pFlexBuffer->m_Velocities.unmap();
	m_pFlexBuffer->m_Phases.unmap();
	m_pFlexBuffer->m_Densities.unmap();
	m_pFlexBuffer->m_Anisotropy1.unmap();
	m_pFlexBuffer->m_Anisotropy2.unmap();
	m_pFlexBuffer->m_Anisotropy3.unmap();
	m_pFlexBuffer->m_Normals.unmap();
	//m_pFlexBuffer->m_SmoothPositions.unmap();
	m_pFlexBuffer->m_DiffusePositions.unmap();
	m_pFlexBuffer->m_DiffuseVelocities.unmap();
	m_pFlexBuffer->m_DiffuseIndices.unmap();
	m_pFlexBuffer->m_ActiveIndices.unmap();

	//Convexes
	m_pFlexBuffer->m_ShapeGeometry.unmap();
	m_pFlexBuffer->m_ShapePositions.unmap();
	m_pFlexBuffer->m_ShapeRotations.unmap();
	m_pFlexBuffer->m_ShapePrevPositions.unmap();
	m_pFlexBuffer->m_ShapePrevRotations.unmap();
	m_pFlexBuffer->m_ShapeFlags.unmap();

	//Rigids
	m_pFlexBuffer->m_RigidOffsets.unmap();
	m_pFlexBuffer->m_RigidIndices.unmap();
	m_pFlexBuffer->m_RigidMeshSize.unmap();
	m_pFlexBuffer->m_RigidCoefficients.unmap();
	m_pFlexBuffer->m_RigidRotations.unmap();
	m_pFlexBuffer->m_RigidTranslations.unmap();
	m_pFlexBuffer->m_RigidLocalPositions.unmap();
	m_pFlexBuffer->m_RigidLocalNormals.unmap();

}

#pragma endregion