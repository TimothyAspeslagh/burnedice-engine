#pragma once
class Texture final
{
public:
	Texture(DesignString assetFile, DesignString textureID);
	~Texture();

	void Initialise(const MainManager& gameManager);
	void Clean();

	ID3D11ShaderResourceView* GetSRV() { return m_pSRV; }
	ID3D11Resource* GetResource() { return m_pResource; }
	DesignString GetTextureID() { return m_TextureID; }

private:
	ID3D11ShaderResourceView * m_pSRV = nullptr;
	ID3D11Resource* m_pResource = nullptr;
	DesignString m_AssetFile;
	DesignString m_TextureID;
};

