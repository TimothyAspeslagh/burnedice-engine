﻿#include "stdafx.h"
#include "Mesh.h"
#include "Material.h"
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

Mesh::Mesh(DesignString assetFile, DesignString meshID): m_AssetFile(assetFile), m_MeshID(meshID)
{
}

Mesh::~Mesh()
{
}

void Mesh::Initialise(const MainManager& gameManager)
{
	Assimp::Importer importer;
	const aiScene * object = importer.ReadFile(m_AssetFile.GetText(), aiProcess_Triangulate | aiProcess_JoinIdenticalVertices); //| aiProcess_MakeLeftHanded

	if (object == nullptr)
	{
		wcout << L"Failed to read asset: " << m_AssetFile << L" , with tag: " << m_MeshID << endl;
		return;
	}

	for (size_t meshID = 0; meshID != object->mNumMeshes; ++meshID)
	{
		//Vertex info
		for (size_t i = 0; i != object->mMeshes[meshID]->mNumVertices; ++i)
		{
			auto aiVertex = object->mMeshes[meshID]->mVertices[i];
			auto aiNormal = object->mMeshes[meshID]->mNormals[i];
			auto aiTex = object->mMeshes[meshID]->mTextureCoords[0][i];


			iVertex vertex;

			vertex.Pos = XMFLOAT3(aiVertex.x, aiVertex.y, aiVertex.z);
			vertex.Tex = XMFLOAT2(aiTex.x, aiTex.y);
			vertex.Normal = XMFLOAT3(aiNormal.x, aiNormal.y, aiNormal.z);

			m_VertexList.push_back(vertex);
		}
		//Index info
		for(size_t i = 0; i != object->mMeshes[meshID]->mNumFaces; ++i)
		{
			auto aiFace = object->mMeshes[meshID]->mFaces[i];
			iIndex index;
			//I used the max-quality flag in the aiImportFile function
			//this automatically triangulates all faces

			index.i = aiFace.mIndices[0];
			index.j = aiFace.mIndices[1];
			index.k = aiFace.mIndices[2];
			m_IndexList.push_back(index);
		}
	}

	//Create vertex buffer
	D3D11_BUFFER_DESC vbd;
	vbd.ByteWidth = sizeof(iPrimitiveVertex) * m_VertexList.size();
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vsrd = { 0 };
	vsrd.pSysMem = m_VertexList.data();

	auto result = gameManager.m_pDevice->CreateBuffer(&vbd, &vsrd, &m_pVertexBuffer);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create vertex buffer of mesh: " << m_AssetFile << " , with tag: " << m_MeshID << endl;
	}

	D3D11_BUFFER_DESC ibd;
	ibd.ByteWidth = sizeof(iIndex) * m_IndexList.size();
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	//ibd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA isrd = { 0 };
	isrd.pSysMem = m_IndexList.data();

	result = gameManager.m_pDevice->CreateBuffer(&ibd, &isrd, &m_pIndexBuffer);

	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create index buffer of mesh: " << m_AssetFile << " , with tag: " << m_MeshID << endl;
	}


}

void Mesh::Tick()
{
}

void Mesh::Draw()
{
}

void Mesh::Clean()
{
}
