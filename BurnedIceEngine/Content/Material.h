﻿#pragma once

class Texture;
struct DesignString;

enum TextureUsage
{
	DIFFUSE,
	NORMAL,
	AO,		   //Currently unused
	ROUGHNESS, //Currently unused
	METALNESS, //Currently unused
	CUSTOM	   //Currently unused
};

class Material
{
public:
	Material(DesignString assetFile, DesignString materialID);

	virtual ~Material() {};

	virtual void Initialise(const MainManager &gameManager);
	void RootUpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp , const vector<pair<DesignString, TextureUsage>> &textures);
	void RootUpdateShaderVariables(const MainManager& gameManager);
	virtual void UpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>> &textures) {};
	virtual void UpdateShaderVariables(const MainManager& gameManager) {};
	void RootClean();
	virtual void Clean() {}
	virtual void Draw(const MainManager &gameManager, const DesignString meshID, TransformComponent* transformCmp);
	virtual void Draw(const MainManager &gameManager, const DesignString meshID);
	virtual void Draw(const MainManager &gameManager) {};
	virtual void SetDiffuseTexture(Texture* diffuseTex) {}; //todo: Make abstract class

	friend class Mesh;

protected:
	DesignString m_AssetFile;
	DesignString m_MaterialID;

	ID3DX11Effect *m_pEffect;
	ID3DX11EffectTechnique *m_pTechnique;
	ID3D11InputLayout *m_pInputLayout;

	ID3DX11EffectMatrixVariable *m_pWorldViewProjMat;
	ID3DX11EffectMatrixVariable *m_pWorldMat;
	ID3DX11EffectShaderResourceVariable * m_pTexDiffuse;
	
};
