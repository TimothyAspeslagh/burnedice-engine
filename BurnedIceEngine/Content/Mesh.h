﻿#pragma once

class Material;

class Mesh final
{
public:
	Mesh(DesignString assetFile, DesignString meshID);
	~Mesh();

	void Initialise(const MainManager& gameManager);
	void Tick();
	void Draw();
	void Clean();
	DesignString GetMeshID() const { return m_MeshID; }
	DesignString GetAssetPath() const { return m_AssetFile; }

	ID3D11Buffer* const GetVertexBuffer() const
	{ return m_pVertexBuffer; }
	ID3D11Buffer* GetIndexBuffer() const { return m_pIndexBuffer; }
	size_t GetIndexListSize() const { return m_IndexList.size(); }

	friend class Material;
	friend class FleXProxy;
	friend class FluidMaterial; //TODO: Fix setvertexbuffers
private:
	DesignString m_AssetFile;
	DesignString m_MeshID;

	ID3D11Buffer *m_pVertexBuffer;
	ID3D11Buffer *m_pIndexBuffer;
	vector<iVertex> m_VertexList;
	vector <iIndex> m_IndexList;
	
};
