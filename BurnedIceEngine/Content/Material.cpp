﻿#include "stdafx.h"
#include "Material.h"
#include "../Scene Classes/Components/TransformComponent.h"

Material::Material(DesignString assetFile, DesignString materialID): m_MaterialID(materialID), m_AssetFile(assetFile)
{
}

void Material::Initialise(const MainManager &gameManager)
{
	DWORD shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif
	ID3D10Blob* compilationMsgs = 0;
	HRESULT result = D3DX11CompileEffectFromFile(L"./Resources/CubeMeshEffect.fx", 0, 0, shaderFlags, 0, gameManager.m_pDevice, &m_pEffect, &compilationMsgs);
	if (FAILED(result))
	{
		if (compilationMsgs != nullptr)
		{
			char *errors = (char*)compilationMsgs->GetBufferPointer();

			wstringstream ss;
			for (unsigned int i = 0; i < compilationMsgs->GetBufferSize(); i++)
				ss << errors[i];

			OutputDebugStringW(ss.str().c_str());
			compilationMsgs->Release();
			compilationMsgs = nullptr;

			wcout << ss.str() << endl;
		}

		wcout << L"Warning! Failed to create Effect from file: CubeMeshEffect.fx" << endl;
	}

	compilationMsgs->Release();

	m_pTechnique = m_pEffect->GetTechniqueByName("DefaultTechnique");
	m_pWorldViewProjMat = m_pEffect->GetVariableByName("gWorldViewProj")->AsMatrix();
	m_pWorldMat = m_pEffect->GetVariableByName("gWorld")->AsMatrix();
	m_pTexDiffuse = m_pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();

	//define & create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",0, DXGI_FORMAT_R32G32B32_FLOAT, 0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "NORMAL" ,0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,24,D3D11_INPUT_PER_VERTEX_DATA,0 }

	};
	UINT numElem = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC pD;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&pD);
	result = gameManager.m_pDevice->CreateInputLayout(
		layout,
		numElem,
		pD.pIAInputSignature,
		pD.IAInputSignatureSize,
		&m_pInputLayout);
	if (FAILED(result))
	{
		wcout << L"Warning! Failed to create input layout!: CubeMeshComponent" << endl;
	}

}

void Material::RootUpdateShaderVariables(const MainManager& gameManager, TransformComponent* transformCmp, const vector<pair<DesignString, TextureUsage>> &textures)
{
	auto world = transformCmp->GetWorldMatrix();
	auto worldMat = XMLoadFloat4x4(&world);
	auto viewProj = gameManager.GetActiveCamera()->GetViewProjection();
	auto viewProjMat = XMLoadFloat4x4(&viewProj);
	auto wvp = worldMat * viewProjMat;

	if(m_pWorldMat != nullptr)
		m_pWorldMat->SetMatrix(reinterpret_cast<float*>(&worldMat));

	if(m_pWorldViewProjMat != nullptr)
		m_pWorldViewProjMat->SetMatrix(reinterpret_cast<float*>(&wvp));

	if(m_pTexDiffuse != nullptr)
	{
		for(auto it = textures.begin(); it != textures.end(); ++it)
		{
			if(it->second == TextureUsage::DIFFUSE)
			{
				m_pTexDiffuse->SetResource(gameManager.Content->GetTexture(it->first)->GetSRV());
				break;
			}
		}
	}
	UpdateShaderVariables(gameManager, transformCmp, textures);
}

void Material::RootUpdateShaderVariables(const MainManager& gameManager)
{
	UpdateShaderVariables(gameManager);
}

void Material::RootClean()
{
	m_pEffect->Release();

	if(m_pInputLayout != nullptr)
	{
		m_pInputLayout->Release();
	}
	Clean();
}

void Material::Draw(const MainManager &gameManager, const DesignString meshID, TransformComponent* transformCmp)
{
	auto mesh = gameManager.Content->GetMesh(meshID);
	size_t stride = sizeof(iVertex);
	size_t offset = 0;
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &mesh->m_pVertexBuffer, &stride, &offset);
	gameManager.m_pDeviceContext->IASetIndexBuffer(mesh->m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3DX11_TECHNIQUE_DESC tD;
	m_pTechnique->GetDesc(&tD);
	for (size_t i = 0; i < tD.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->DrawIndexed(mesh->m_IndexList.size() * 3, 0, 0);
	}
}

void Material::Draw(const MainManager& gameManager, const DesignString meshID)
{
	auto mesh = gameManager.Content->GetMesh(meshID);
	size_t stride = sizeof(iVertex);
	size_t offset = 0;
	gameManager.m_pDeviceContext->IASetVertexBuffers(0, 1, &mesh->m_pVertexBuffer, &stride, &offset);
	gameManager.m_pDeviceContext->IASetIndexBuffer(mesh->m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	gameManager.m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	gameManager.m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3DX11_TECHNIQUE_DESC tD;
	m_pTechnique->GetDesc(&tD);
	for (size_t i = 0; i < tD.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameManager.m_pDeviceContext);
		gameManager.m_pDeviceContext->DrawIndexed(mesh->m_IndexList.size() * 3, 0, 0);
	}

}
