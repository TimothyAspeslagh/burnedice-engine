#include "stdafx.h"
#include "Texture.h"


Texture::Texture(DesignString assetFile, DesignString textureID): m_AssetFile(assetFile), m_TextureID(textureID)
{
}


Texture::~Texture()
{
}

void Texture::Initialise(const MainManager& gameManager)
{
	auto fileExtention = m_AssetFile.GetText().substr(m_AssetFile.GetText().find_last_of(".") + 1);
	wstring assetFile = wstring(m_AssetFile.GetText().begin(), m_AssetFile.GetText().end());
	ScratchImage * image = new ScratchImage();
	HRESULT hr;

	if (fileExtention.c_str() == "dds") 	//Extention is DDS
	{
		hr = LoadFromDDSFile(assetFile.c_str(), DDS_FLAGS_NONE, NULL, *image);
		if(FAILED(hr))
		{
			wcout << L"Failed to load texture from file: " << m_AssetFile << " , with tag: " << m_TextureID << endl;
			
			delete image;
			image = nullptr;
			return;
		}
	}
	else //Generic load
	{
		hr = LoadFromWICFile(assetFile.c_str(), WIC_FLAGS_NONE, NULL, *image);
		if(FAILED(hr))
		{
			wcout << L"Failed to load texture from file: " << m_AssetFile << " , with tag: " << m_TextureID << endl;
			
			delete image;
			image = nullptr;
			return;
		}
	}

	hr = CreateTexture(gameManager.m_pDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &m_pResource);
	if(FAILED(hr))
	{
		wcout << L"Failed to create texture from file: " << m_AssetFile << " , with tag: " << m_TextureID << endl; 

		delete image;
		image = nullptr;
		m_pResource = nullptr;
		return;
	}

	hr = CreateShaderResourceView(gameManager.m_pDevice, image->GetImages(), image->GetImageCount(), image->GetMetadata(), &m_pSRV);
	if(FAILED(hr))
	{
		wcout << L"Failed to create ShaderResourceView from file: " << m_AssetFile << " , with tag: " << m_TextureID << endl;

		delete image;
		image = nullptr;
		m_pResource = nullptr;
		m_pSRV = nullptr;
		return;
	}

	delete image;
	image = nullptr;
}

void Texture::Clean()
{
	m_pSRV->Release();
	m_pResource->Release();
}
